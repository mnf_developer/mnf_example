﻿using System;
using System.IO;
using System.Windows.Forms;

namespace LogServer.Forms
{
    public partial class Form_Set_DB : Form
    {
        public Form_Set_DB()
        {
            InitializeComponent();
            if (MNF_Library.DBDirector.Instance.readDBConnInfo() == true)
                setTextBox_DBInfo(MNF_Library.DBDirector.Instance.DBConnInfo);
        }

        private void setTextBox_DBInfo(MNF_Library.DBConnectionInfo dbConnInfo)
        {
            TextBox_DBServer.Text = dbConnInfo.server;
            TextBox_DBName.Text = dbConnInfo.database;
            TextBox_DBUser.Text = dbConnInfo.uid;
            TextBox_DBPasswd.Text = dbConnInfo.pwd;
        }

        private void Button_DBTest_Click(object sender, EventArgs e)
        {
            if (TextBox_DBServer.Text.Length == 0
            || TextBox_DBName.Text.Length == 0
            || TextBox_DBUser.Text.Length == 0
            || TextBox_DBPasswd.Text.Length == 0)
                return;

            MNF_Library.DBDirector.Instance.DBConnInfo.server = TextBox_DBServer.Text;
            MNF_Library.DBDirector.Instance.DBConnInfo.database = TextBox_DBName.Text;
            MNF_Library.DBDirector.Instance.DBConnInfo.uid = TextBox_DBUser.Text;
            MNF_Library.DBDirector.Instance.DBConnInfo.pwd = TextBox_DBPasswd.Text;

            if (MNF_Library.DBDirector.Instance.connectToDB() == true)
                MessageBox.Show("Connect successed!");
            else
                MessageBox.Show("Connect failed!");
        }

        private void Button_DBDone_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
