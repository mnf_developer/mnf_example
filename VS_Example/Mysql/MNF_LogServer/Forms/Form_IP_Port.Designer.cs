﻿namespace LogServer.Forms
{
    partial class Form_IP_Port
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox_IP = new System.Windows.Forms.TextBox();
            this.TextBox_Port = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Button_IP_Port_Done = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBox_IP
            // 
            this.TextBox_IP.Location = new System.Drawing.Point(56, 16);
            this.TextBox_IP.Name = "TextBox_IP";
            this.TextBox_IP.Size = new System.Drawing.Size(100, 21);
            this.TextBox_IP.TabIndex = 0;
            this.TextBox_IP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TextBox_Port
            // 
            this.TextBox_Port.Location = new System.Drawing.Point(56, 43);
            this.TextBox_Port.Name = "TextBox_Port";
            this.TextBox_Port.Size = new System.Drawing.Size(100, 21);
            this.TextBox_Port.TabIndex = 1;
            this.TextBox_Port.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            // 
            // Button_IP_Port_Done
            // 
            this.Button_IP_Port_Done.Location = new System.Drawing.Point(56, 70);
            this.Button_IP_Port_Done.Name = "Button_IP_Port_Done";
            this.Button_IP_Port_Done.Size = new System.Drawing.Size(75, 23);
            this.Button_IP_Port_Done.TabIndex = 4;
            this.Button_IP_Port_Done.Text = "Done";
            this.Button_IP_Port_Done.UseVisualStyleBackColor = true;
            this.Button_IP_Port_Done.Click += new System.EventHandler(this.Button_IP_Port_Done_Click);
            // 
            // Form_IP_Port
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(184, 112);
            this.Controls.Add(this.Button_IP_Port_Done);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBox_Port);
            this.Controls.Add(this.TextBox_IP);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_IP_Port";
            this.Text = "Form_IP_Port";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox_IP;
        private System.Windows.Forms.TextBox TextBox_Port;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Button_IP_Port_Done;
    }
}