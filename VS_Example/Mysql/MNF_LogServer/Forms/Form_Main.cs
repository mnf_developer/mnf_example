﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace LogServer.Forms
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
            setLogServerIP("127.0.0.1", "5000");
        }

        ~Form_Main()
        {
        }

        private void ClosedOtherForms(object sender, FormClosedEventArgs e)
        {
            Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            MNF_Library.MNF_Manager.Instance.stop();
        }

        private void setIPPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();

            var form_IP_Port = new Form_IP_Port();
            form_IP_Port.Owner = this;
            form_IP_Port.FormClosed += ClosedOtherForms;
            form_IP_Port.Show();
        }

        private void setDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();

            var form_Set_DB = new Form_Set_DB();
            form_Set_DB.Owner = this;
            form_Set_DB.FormClosed += ClosedOtherForms;
            form_Set_DB.Show();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            initMNF();
        }

        private void Button_Clear_LogServerOutput_Click(object sender, EventArgs e)
        {
            ListBox_LogOutput.Items.Clear();
        }

        private void Button_Clear_ClientLog_Click(object sender, EventArgs e)
        {
            ListBox_ClientLog.Items.Clear();
        }

        private void ListBox_ClientLog_MouseDoubleClick(object sender, EventArgs e)
        {
            if (ListBox_ClientLog.SelectedItem != null)
                MessageBox.Show(ListBox_ClientLog.SelectedItem.ToString());
        }

        private void ListBox_LogOutput_MouseDoubleClick(object sender, EventArgs e)
        {
            if (ListBox_LogOutput.SelectedItem != null)
                MessageBox.Show(ListBox_LogOutput.SelectedItem.ToString());
        }

        private void dBClearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MNF_Library.DBDirector.Instance.connectToDB() == false)
                throw new Exception("DB Connect failed");

            var cmd = MNF_Library.DBDirector.Instance.createMySqlCommand("truncate client_log;");
            cmd.ExecuteNonQuery();

            MNF_Library.DBDirector.Instance.Connection.Close();
        }

        private void showDBLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();

            var form_ShowDB = new Form_ShowDB();
            form_ShowDB.Owner = this;
            form_ShowDB.FormClosed += ClosedOtherForms;
            form_ShowDB.Show();
        }

        private void Form_SizeChanged(Object sender, EventArgs e)
        {
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
        }
    }
}
