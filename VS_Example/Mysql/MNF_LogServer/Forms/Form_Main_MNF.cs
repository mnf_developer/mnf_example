﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogServer.Forms
{
    public partial class Form_Main : Form
    {
        #region MNF
        public void updateConnectedSession()
        {
            ListView_Mobiles.BeginUpdate();
            ListView_Mobiles.Items.Clear();

            if (MNF_Library.LogSessionManager.Instance.LogSessionCollection == null)
                return;

            foreach (var logSession in MNF_Library.LogSessionManager.Instance.LogSessionCollection)
            {
                ListViewItem lvi = new ListViewItem(logSession.IP);
                lvi.SubItems.Add(logSession.Info);
                ListView_Mobiles.Items.Add(lvi);
            }

            ListView_Mobiles.EndUpdate();
        }

        public ListBox getLogServerOutputTextBox()
        {
            return ListBox_LogOutput;
        }

        public ListView getMobilesListView()
        {
            return ListView_Mobiles;
        }

        public void writeListBoxLogOutput(string outputLog)
        {
            ListBox_LogOutput.Items.Add(outputLog);
        }

        public void writeListBoxClientLog(string clientLog)
        {
            if (ListBox_ClientLog.Items.Count > 100)
                ListBox_ClientLog.Items.RemoveAt(0);
            ListBox_ClientLog.Items.Add(clientLog);
        }

        public void setLogServerIP(string ip, string port)
        {
            Text = "LogServer Info [" + ip + " : " + port + "]";
            MNF_Library.MNF_Manager.Instance.ServerIP = ip;
            MNF_Library.MNF_Manager.Instance.ServerPort = port;
        }

        public void initMNF()
        {
            try
            {
                if (MNF_Library.DBDirector.Instance.connectToDB() == false)
                    throw new Exception("DBDirector connect to DB failed");

                if (MNF_Library.MNF_Manager.Instance.init() == false)
                    throw new Exception("MNF_Manager init failed.");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        #endregion MNF
    }
}
