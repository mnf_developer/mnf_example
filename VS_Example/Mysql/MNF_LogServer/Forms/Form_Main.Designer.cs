﻿namespace LogServer.Forms
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.serverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setIPPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dBClearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDBLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ListView_Mobiles = new System.Windows.Forms.ListView();
            this.IP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Info = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ListBox_LogOutput = new System.Windows.Forms.ListBox();
            this.TableLayoutPanel_ClientLog = new System.Windows.Forms.TableLayoutPanel();
            this.ListBox_ClientLog = new System.Windows.Forms.ListBox();
            this.Button_Clear_LogServerOutput = new System.Windows.Forms.Button();
            this.Button_Clear_ClientLog = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.TableLayoutPanel_ClientLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serverToolStripMenuItem,
            this.dBToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(964, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // serverToolStripMenuItem
            // 
            this.serverToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setIPPortToolStripMenuItem,
            this.startToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.serverToolStripMenuItem.Name = "serverToolStripMenuItem";
            this.serverToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.serverToolStripMenuItem.Text = "Server";
            // 
            // setIPPortToolStripMenuItem
            // 
            this.setIPPortToolStripMenuItem.Name = "setIPPortToolStripMenuItem";
            this.setIPPortToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.setIPPortToolStripMenuItem.Text = "Set IP/Port";
            this.setIPPortToolStripMenuItem.Click += new System.EventHandler(this.setIPPortToolStripMenuItem_Click);
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.startToolStripMenuItem.Text = "Start";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // dBToolStripMenuItem
            // 
            this.dBToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setDBToolStripMenuItem,
            this.dBClearToolStripMenuItem,
            this.showDBLogToolStripMenuItem});
            this.dBToolStripMenuItem.Name = "dBToolStripMenuItem";
            this.dBToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.dBToolStripMenuItem.Text = "DB";
            // 
            // setDBToolStripMenuItem
            // 
            this.setDBToolStripMenuItem.Name = "setDBToolStripMenuItem";
            this.setDBToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.setDBToolStripMenuItem.Text = "Set DB";
            this.setDBToolStripMenuItem.Click += new System.EventHandler(this.setDBToolStripMenuItem_Click);
            // 
            // dBClearToolStripMenuItem
            // 
            this.dBClearToolStripMenuItem.Name = "dBClearToolStripMenuItem";
            this.dBClearToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.dBClearToolStripMenuItem.Text = "DB Clear";
            this.dBClearToolStripMenuItem.Click += new System.EventHandler(this.dBClearToolStripMenuItem_Click);
            // 
            // showDBLogToolStripMenuItem
            // 
            this.showDBLogToolStripMenuItem.Name = "showDBLogToolStripMenuItem";
            this.showDBLogToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.showDBLogToolStripMenuItem.Text = "Show DB Log";
            this.showDBLogToolStripMenuItem.Click += new System.EventHandler(this.showDBLogToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.ListView_Mobiles, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ListBox_LogOutput, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 27);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 227F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(453, 506);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // ListView_Mobiles
            // 
            this.ListView_Mobiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IP,
            this.Info});
            this.ListView_Mobiles.Location = new System.Drawing.Point(3, 3);
            this.ListView_Mobiles.Name = "ListView_Mobiles";
            this.ListView_Mobiles.Size = new System.Drawing.Size(447, 273);
            this.ListView_Mobiles.TabIndex = 1;
            this.ListView_Mobiles.UseCompatibleStateImageBehavior = false;
            this.ListView_Mobiles.View = System.Windows.Forms.View.Details;
            // 
            // IP
            // 
            this.IP.Text = "IP";
            this.IP.Width = 151;
            // 
            // Info
            // 
            this.Info.Text = "Info";
            this.Info.Width = 137;
            // 
            // ListBox_LogOutput
            // 
            this.ListBox_LogOutput.FormattingEnabled = true;
            this.ListBox_LogOutput.HorizontalScrollbar = true;
            this.ListBox_LogOutput.ItemHeight = 12;
            this.ListBox_LogOutput.Location = new System.Drawing.Point(3, 282);
            this.ListBox_LogOutput.Name = "ListBox_LogOutput";
            this.ListBox_LogOutput.Size = new System.Drawing.Size(447, 220);
            this.ListBox_LogOutput.TabIndex = 2;
            this.ListBox_LogOutput.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListBox_LogOutput_MouseDoubleClick);
            // 
            // TableLayoutPanel_ClientLog
            // 
            this.TableLayoutPanel_ClientLog.ColumnCount = 1;
            this.TableLayoutPanel_ClientLog.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel_ClientLog.Controls.Add(this.ListBox_ClientLog, 0, 0);
            this.TableLayoutPanel_ClientLog.Location = new System.Drawing.Point(468, 27);
            this.TableLayoutPanel_ClientLog.Name = "TableLayoutPanel_ClientLog";
            this.TableLayoutPanel_ClientLog.RowCount = 1;
            this.TableLayoutPanel_ClientLog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel_ClientLog.Size = new System.Drawing.Size(496, 506);
            this.TableLayoutPanel_ClientLog.TabIndex = 2;
            // 
            // ListBox_ClientLog
            // 
            this.ListBox_ClientLog.FormattingEnabled = true;
            this.ListBox_ClientLog.HorizontalScrollbar = true;
            this.ListBox_ClientLog.ItemHeight = 12;
            this.ListBox_ClientLog.Location = new System.Drawing.Point(3, 3);
            this.ListBox_ClientLog.Name = "ListBox_ClientLog";
            this.ListBox_ClientLog.Size = new System.Drawing.Size(490, 496);
            this.ListBox_ClientLog.TabIndex = 0;
            this.ListBox_ClientLog.SizeChanged += new System.EventHandler(this.Form_SizeChanged);
            this.ListBox_ClientLog.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListBox_ClientLog_MouseDoubleClick);
            // 
            // Button_Clear_LogServerOutput
            // 
            this.Button_Clear_LogServerOutput.Location = new System.Drawing.Point(12, 539);
            this.Button_Clear_LogServerOutput.Name = "Button_Clear_LogServerOutput";
            this.Button_Clear_LogServerOutput.Size = new System.Drawing.Size(450, 23);
            this.Button_Clear_LogServerOutput.TabIndex = 3;
            this.Button_Clear_LogServerOutput.Text = "Clear LogServer Output";
            this.Button_Clear_LogServerOutput.UseVisualStyleBackColor = true;
            this.Button_Clear_LogServerOutput.Click += new System.EventHandler(this.Button_Clear_LogServerOutput_Click);
            // 
            // Button_Clear_ClientLog
            // 
            this.Button_Clear_ClientLog.Location = new System.Drawing.Point(471, 539);
            this.Button_Clear_ClientLog.Name = "Button_Clear_ClientLog";
            this.Button_Clear_ClientLog.Size = new System.Drawing.Size(490, 23);
            this.Button_Clear_ClientLog.TabIndex = 4;
            this.Button_Clear_ClientLog.Text = "Clear Client Log";
            this.Button_Clear_ClientLog.UseVisualStyleBackColor = true;
            this.Button_Clear_ClientLog.Click += new System.EventHandler(this.Button_Clear_ClientLog_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(964, 577);
            this.Controls.Add(this.Button_Clear_ClientLog);
            this.Controls.Add(this.Button_Clear_LogServerOutput);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.TableLayoutPanel_ClientLog);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Main";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.SizeChanged += new System.EventHandler(this.Form_SizeChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel_ClientLog.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serverToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem setIPPortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDBToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListView ListView_Mobiles;
        private System.Windows.Forms.ColumnHeader IP;
        private System.Windows.Forms.ColumnHeader Info;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel_ClientLog;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ListBox ListBox_LogOutput;
        private System.Windows.Forms.ListBox ListBox_ClientLog;
        private System.Windows.Forms.Button Button_Clear_LogServerOutput;
        private System.Windows.Forms.Button Button_Clear_ClientLog;
        private System.Windows.Forms.ToolStripMenuItem dBClearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showDBLogToolStripMenuItem;
    }
}

