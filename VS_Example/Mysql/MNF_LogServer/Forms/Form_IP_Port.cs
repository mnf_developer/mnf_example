﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogServer.Forms
{
    public partial class Form_IP_Port : Form
    {
        public Form_IP_Port()
        {
            InitializeComponent();

            TextBox_IP.Text = MNF_Library.MNF_Manager.Instance.ServerIP;
            TextBox_Port.Text = MNF_Library.MNF_Manager.Instance.ServerPort;
        }

        private void Button_IP_Port_Done_Click(object sender, EventArgs e)
        {
            Owner.Text = "LogServer Info [" + TextBox_IP.Text + " : " + TextBox_Port.Text + "]";
            Close();
        }
    }
}
