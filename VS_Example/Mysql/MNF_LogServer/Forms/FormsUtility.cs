﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogServer.Forms
{
    static class FormsUtility
    {
        public static Forms.Form_Main GetFormMain()
        {
            var formCollection = Application.OpenForms;
            foreach (var form in formCollection)
            {
                var formMain = form as Forms.Form_Main;
                if (formMain != null)
                    return formMain;
            }
            return null;
        }

        public static Forms.Form_Set_DB GetFormSetDB()
        {
            var formCollection = Application.OpenForms;
            foreach (var form in formCollection)
            {
                var formSetDB = form as Forms.Form_Set_DB;
                if (formSetDB != null)
                    return formSetDB;
            }
            return null;
        }
    }
}
