﻿namespace LogServer.Forms
{
    partial class Form_Set_DB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox_DBServer = new System.Windows.Forms.TextBox();
            this.TextBox_DBName = new System.Windows.Forms.TextBox();
            this.TextBox_DBUser = new System.Windows.Forms.TextBox();
            this.TextBox_DBPasswd = new System.Windows.Forms.TextBox();
            this.Label_DBServer = new System.Windows.Forms.Label();
            this.Label_DBUser = new System.Windows.Forms.Label();
            this.Label_DBName = new System.Windows.Forms.Label();
            this.Label_DBPwd = new System.Windows.Forms.Label();
            this.Button_DBTest = new System.Windows.Forms.Button();
            this.Button_DBDone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBox_DBServer
            // 
            this.TextBox_DBServer.Location = new System.Drawing.Point(108, 20);
            this.TextBox_DBServer.Name = "TextBox_DBServer";
            this.TextBox_DBServer.Size = new System.Drawing.Size(100, 21);
            this.TextBox_DBServer.TabIndex = 0;
            // 
            // TextBox_DBName
            // 
            this.TextBox_DBName.Location = new System.Drawing.Point(108, 47);
            this.TextBox_DBName.Name = "TextBox_DBName";
            this.TextBox_DBName.Size = new System.Drawing.Size(100, 21);
            this.TextBox_DBName.TabIndex = 1;
            // 
            // TextBox_DBUser
            // 
            this.TextBox_DBUser.Location = new System.Drawing.Point(108, 74);
            this.TextBox_DBUser.Name = "TextBox_DBUser";
            this.TextBox_DBUser.Size = new System.Drawing.Size(100, 21);
            this.TextBox_DBUser.TabIndex = 2;
            // 
            // TextBox_DBPasswd
            // 
            this.TextBox_DBPasswd.Location = new System.Drawing.Point(108, 101);
            this.TextBox_DBPasswd.Name = "TextBox_DBPasswd";
            this.TextBox_DBPasswd.Size = new System.Drawing.Size(100, 21);
            this.TextBox_DBPasswd.TabIndex = 3;
            // 
            // Label_DBServer
            // 
            this.Label_DBServer.AutoSize = true;
            this.Label_DBServer.Location = new System.Drawing.Point(31, 23);
            this.Label_DBServer.Name = "Label_DBServer";
            this.Label_DBServer.Size = new System.Drawing.Size(61, 12);
            this.Label_DBServer.TabIndex = 4;
            this.Label_DBServer.Text = "DB Server";
            // 
            // Label_DBUser
            // 
            this.Label_DBUser.AutoSize = true;
            this.Label_DBUser.Location = new System.Drawing.Point(31, 77);
            this.Label_DBUser.Name = "Label_DBUser";
            this.Label_DBUser.Size = new System.Drawing.Size(51, 12);
            this.Label_DBUser.TabIndex = 5;
            this.Label_DBUser.Text = "DB User";
            // 
            // Label_DBName
            // 
            this.Label_DBName.AutoSize = true;
            this.Label_DBName.Location = new System.Drawing.Point(31, 50);
            this.Label_DBName.Name = "Label_DBName";
            this.Label_DBName.Size = new System.Drawing.Size(59, 12);
            this.Label_DBName.TabIndex = 5;
            this.Label_DBName.Text = "DB Name";
            // 
            // Label_DBPwd
            // 
            this.Label_DBPwd.AutoSize = true;
            this.Label_DBPwd.Location = new System.Drawing.Point(31, 104);
            this.Label_DBPwd.Name = "Label_DBPwd";
            this.Label_DBPwd.Size = new System.Drawing.Size(50, 12);
            this.Label_DBPwd.TabIndex = 6;
            this.Label_DBPwd.Text = "DB Pwd";
            // 
            // Button_DBTest
            // 
            this.Button_DBTest.Location = new System.Drawing.Point(33, 137);
            this.Button_DBTest.Name = "Button_DBTest";
            this.Button_DBTest.Size = new System.Drawing.Size(75, 43);
            this.Button_DBTest.TabIndex = 7;
            this.Button_DBTest.Text = "Connect Test";
            this.Button_DBTest.UseVisualStyleBackColor = true;
            this.Button_DBTest.Click += new System.EventHandler(this.Button_DBTest_Click);
            // 
            // Button_DBDone
            // 
            this.Button_DBDone.Location = new System.Drawing.Point(133, 137);
            this.Button_DBDone.Name = "Button_DBDone";
            this.Button_DBDone.Size = new System.Drawing.Size(75, 43);
            this.Button_DBDone.TabIndex = 8;
            this.Button_DBDone.Text = "Done";
            this.Button_DBDone.UseVisualStyleBackColor = true;
            this.Button_DBDone.Click += new System.EventHandler(this.Button_DBDone_Click);
            // 
            // Form_Set_DB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 192);
            this.Controls.Add(this.Button_DBDone);
            this.Controls.Add(this.Button_DBTest);
            this.Controls.Add(this.Label_DBPwd);
            this.Controls.Add(this.Label_DBName);
            this.Controls.Add(this.Label_DBUser);
            this.Controls.Add(this.Label_DBServer);
            this.Controls.Add(this.TextBox_DBPasswd);
            this.Controls.Add(this.TextBox_DBUser);
            this.Controls.Add(this.TextBox_DBName);
            this.Controls.Add(this.TextBox_DBServer);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Set_DB";
            this.Text = "Form_Set_DB";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox_DBServer;
        private System.Windows.Forms.TextBox TextBox_DBName;
        private System.Windows.Forms.TextBox TextBox_DBUser;
        private System.Windows.Forms.TextBox TextBox_DBPasswd;
        private System.Windows.Forms.Label Label_DBServer;
        private System.Windows.Forms.Label Label_DBName;
        private System.Windows.Forms.Label Label_DBPwd;
        private System.Windows.Forms.Label Label_DBUser;
        private System.Windows.Forms.Button Button_DBTest;
        private System.Windows.Forms.Button Button_DBDone;
    }
}