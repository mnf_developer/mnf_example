﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MNF;

namespace LogServer.Forms
{
    public partial class Form_ShowDB : Form
    {
        private int prevHeight;
        private int prevWidth;

        public Form_ShowDB()
        {
            InitializeComponent();
        }

        private void Form_ShowDB_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTimePicker_Start.Value.Date;
            DateTimePicker_Start.Text = dt.AddDays(-2).ToString();
            prevHeight = Height;
            prevWidth = Width;
        }

        private void Button_Reload_Click(object sender, EventArgs e)
        {
            ReloadFromDB();
        }

        private void ReloadFromDB()
        {
            try
            {
                if (MNF_Library.DBDirector.Instance.connectToDB() == false)
                    throw new Exception("DB Connect failed");

                var cmd = MNF_Library.DBDirector.Instance.createMySqlCommand(
                    "select ip, session, logType, log, date from client_log where date between @start and @end");
                cmd.Parameters.AddWithValue("@start", DateTimePicker_Start.Value.ToString("yyyy-MM-dd hh:mm:ss"));
                cmd.Parameters.AddWithValue("@end", DateTimePicker_End.Value.ToString("yyyy-MM-dd hh:mm:ss"));

                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(cmd);

                DataSet DS = new DataSet();

                mySqlDataAdapter.Fill(DS);
                DataGridView_ClientLog.DataSource = DS.Tables[0];

                MNF_Library.DBDirector.Instance.Connection.Close();
            }
            catch (Exception exception)
            {
                LogManager.Instance.WriteException(exception, "Form_ShowDB load failed");
                MessageBox.Show(exception.ToString());
                Close();
            }
        }

        private void Form_SizeChanged(Object sender, EventArgs e)
        {
            int changedHeightSize = Height - prevHeight;
            int changedWidthSize = Width - prevWidth;

            prevWidth = Width;
            prevHeight = Height;

            DataGridView_ClientLog.Height += changedHeightSize;
            DataGridView_ClientLog.Width += changedWidthSize;
        }

        private void DataGridView_ClientLog_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            if (DataGridView_ClientLog.Rows.Count == 0)
                return;

            if (DataGridView_ClientLog.SelectedCells.Count == 0)
                return;

            var selectedCell = DataGridView_ClientLog.SelectedCells[0];
            if (selectedCell.RowIndex < 0)
                return;

            var selectedRow = DataGridView_ClientLog.Rows[selectedCell.RowIndex];

            string selectedRowString = "";
            for (int i = 0; i < selectedRow.Cells.Count; ++i)
            {
                string columnName = DataGridView_ClientLog.Columns[i].HeaderText;
                selectedRowString += string.Format("{0} : {1}", columnName, selectedRow.Cells[i].Value.ToString());
                selectedRowString += "\n";
            }
            if (selectedRowString.Length > 0)
            {
                MessageBox.Show(selectedRowString);
                return;
            }
        }
    }
}
