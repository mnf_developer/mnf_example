﻿namespace LogServer.Forms
{
    partial class Form_ShowDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DateTimePicker_Start = new System.Windows.Forms.DateTimePicker();
            this.DateTimePicker_End = new System.Windows.Forms.DateTimePicker();
            this.DataGridView_ClientLog = new System.Windows.Forms.DataGridView();
            this.Button_Reload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_ClientLog)).BeginInit();
            this.SuspendLayout();
            // 
            // DateTimePicker_Start
            // 
            this.DateTimePicker_Start.Location = new System.Drawing.Point(12, 12);
            this.DateTimePicker_Start.Name = "DateTimePicker_Start";
            this.DateTimePicker_Start.Size = new System.Drawing.Size(200, 21);
            this.DateTimePicker_Start.TabIndex = 2;
            // 
            // DateTimePicker_End
            // 
            this.DateTimePicker_End.Location = new System.Drawing.Point(218, 12);
            this.DateTimePicker_End.Name = "DateTimePicker_End";
            this.DateTimePicker_End.Size = new System.Drawing.Size(200, 21);
            this.DateTimePicker_End.TabIndex = 3;
            // 
            // DataGridView_ClientLog
            // 
            this.DataGridView_ClientLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_ClientLog.Location = new System.Drawing.Point(12, 39);
            this.DataGridView_ClientLog.Name = "DataGridView_ClientLog";
            this.DataGridView_ClientLog.ReadOnly = true;
            this.DataGridView_ClientLog.RowTemplate.Height = 23;
            this.DataGridView_ClientLog.Size = new System.Drawing.Size(758, 412);
            this.DataGridView_ClientLog.TabIndex = 4;
            this.DataGridView_ClientLog.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridView_ClientLog_CellMouseDoubleClick);
            // 
            // Button_Reload
            // 
            this.Button_Reload.Location = new System.Drawing.Point(424, 13);
            this.Button_Reload.Name = "Button_Reload";
            this.Button_Reload.Size = new System.Drawing.Size(75, 23);
            this.Button_Reload.TabIndex = 5;
            this.Button_Reload.Text = "Reload";
            this.Button_Reload.UseVisualStyleBackColor = true;
            this.Button_Reload.Click += new System.EventHandler(this.Button_Reload_Click);
            // 
            // Form_ShowDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(782, 463);
            this.Controls.Add(this.Button_Reload);
            this.Controls.Add(this.DataGridView_ClientLog);
            this.Controls.Add(this.DateTimePicker_End);
            this.Controls.Add(this.DateTimePicker_Start);
            this.Name = "Form_ShowDB";
            this.Text = "Form_ShowDB";
            this.Load += new System.EventHandler(this.Form_ShowDB_Load);
            this.SizeChanged += new System.EventHandler(this.Form_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_ClientLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DateTimePicker_Start;
        private System.Windows.Forms.DateTimePicker DateTimePicker_End;
        private System.Windows.Forms.DataGridView DataGridView_ClientLog;
        private System.Windows.Forms.Button Button_Reload;
    }
}