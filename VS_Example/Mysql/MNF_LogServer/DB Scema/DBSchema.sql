﻿DROP DATABASE mnf_db;
CREATE DATABASE mnf_db;

CREATE TABLE `client_log` (
  `ip` varchar(50) NOT NULL,
  `session` varchar(50) DEFAULT NULL,
  `logType` varchar(50) DEFAULT NULL,
  `log` varchar(1024) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
