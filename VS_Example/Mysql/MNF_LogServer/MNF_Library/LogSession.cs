﻿using System.Collections.Generic;
using MNF;

namespace LogServer.MNF_Library
{
    public class LogSession : JsonSession
    {
        public string IP { get; set; }
        public string ID { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }
        public bool ShowCheckBox { get; set; }

        public LogSession()
        {
            LogManager.Instance.Write("LogSession Create");
        }

        ~LogSession()
        {
            LogManager.Instance.Write("LogSession Destroy");
        }

        public override string ToString()
        {
            return string.Format("[{0}:{1}:{2}:{3}]", base.ToString(), IP, ID, Info);
        }
    }

    public class LogSessionManager : Singleton<LogSessionManager>
    {
        private List<LogSession> logSessionList = new List<LogSession>();
        private object lockObject = new object();

        public List<LogSession> LogSessionCollection
        {
            get { return logSessionList; }
        }

        public bool addSession(LogSession logSession)
        {
            bool addResult = false;
            lock (lockObject)
            {
                var result = logSessionList.Find(x => x.IP == logSession.IP);
                if (result == null)
                {
                    logSessionList.Add(logSession);
                    addResult = true;
                }
            }
            return addResult;
        }

        public void removeSession(LogSession logSession)
        {
            lock (lockObject)
            {
                logSessionList.Remove(logSession);
            }
        }
    }
}
