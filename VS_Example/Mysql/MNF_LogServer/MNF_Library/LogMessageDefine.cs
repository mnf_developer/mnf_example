﻿using MNF;

namespace LogServer.MNF_Library
{
	public partial class LogServerMessageDefine
	{
		public enum ENUM_LOG_SERVER
		{
			LOG_SERVER_CONNECT,
			LOG_SERVER_LOG_INFO,
		}

		[System.Serializable]
		public class PACK_LOG_SERVER_CONNECT// : JsonMessageHeader
		{
			public string fromID;
			public string fromInfo;
        }

		[System.Serializable]
		public class PACK_LOG_SERVER_LOG_INFO// : JsonMessageHeader
		{
            public ENUM_LOG_TYPE logType;
			public string logInfo;
        }
	}
}
