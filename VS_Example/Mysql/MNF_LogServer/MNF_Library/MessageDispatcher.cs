﻿using System.Windows.Forms;
using System;
using MNF;

namespace LogServer.MNF_Library
{
    public class LogServerMessageDispatcher : DefaultDispatchHelper<LogSession, LogServerMessageDefine, LogServerMessageDefine.ENUM_LOG_SERVER>
    {
        Forms.Form_Main formMain = null;

        protected override bool OnInit()
        {
            if (base.OnInit() == false)
                return false;

            formMain = Forms.FormsUtility.GetFormMain();

            return true;
        }

        int onLOG_SERVER_CONNECT(LogSession session, object message)
        {
            var connect = (LogServerMessageDefine.PACK_LOG_SERVER_CONNECT)message;
            session.IP = session.Socket.RemoteEndPoint.ToString();
            session.ID = connect.fromID;
            session.Info = connect.fromInfo;

            string connectString = string.Format("Session:{0} - Connect IP:{1} - ID:{2} - Info:{3}",
                session, session.IP, connect.fromID, connect.fromInfo);

            LogSessionManager.Instance.addSession(session);

            var clientLogOutput = formMain.getLogServerOutputTextBox();
            clientLogOutput.InvokeIfNeeded(formMain.writeListBoxClientLog, connectString);

            var mobileListView = formMain.getMobilesListView();
            mobileListView.InvokeIfNeeded(formMain.updateConnectedSession);

            return 0;
        }

        private int onLOG_SERVER_LOG_INFO(LogSession session, object message)
        {
            if (session.IsConnected == false)
                return -1;

            try
            {
                var logInfo = (LogServerMessageDefine.PACK_LOG_SERVER_LOG_INFO)message;

                // write to form
                var clientLogOutput = formMain.getLogServerOutputTextBox();
                clientLogOutput.InvokeIfNeeded(formMain.writeListBoxClientLog, logInfo.logInfo);
                
                // write to db
                var dbLogInfo = new DBMessageDispatcher.DBLogInfo();
                dbLogInfo.IP = session.Socket.RemoteEndPoint.ToString();
                dbLogInfo.Session = session.ToString();
                dbLogInfo.LogType = logInfo.logType.ToString();
                dbLogInfo.Log = logInfo.logInfo;

                if (TcpHelper.Instance.RequestDBMessage((int)DBMessageDispatcher.DB_MESSAGE_TYPE.DB_MESSAGE_WRITE_LOG, dbLogInfo) == false)
                {
                    LogManager.Instance.WriteError("DB Message push failed");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
            return 0;
        }
    }
}
