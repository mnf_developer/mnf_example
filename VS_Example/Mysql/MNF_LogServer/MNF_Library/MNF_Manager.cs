﻿using System;
using MNF_Common;
using MNF;

namespace LogServer.MNF_Library
{
    public class MNF_Manager : Singleton<MNF_Manager>
    {
        public enum ENUM_PACKET_TYPE
        {
            PACKET_TYPE_BINARY,
            PACKET_TYPE_JSON,
        }

        public bool IsInit { get; set; }
        public string ServerIP { get; set; }
        public string ServerPort { get; set; }

        public bool init()
        {
            if (IsInit == true)
                return true;

            LogManager.Instance.SetLogWriter(new ConsoleLogWriter());
            if (LogManager.Instance.Init() == false)
                Console.WriteLine("LogWriter init failed");

            try
            {
                if (TcpHelper.Instance.Start(false) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
                    return false;
                }

                if (TcpHelper.Instance.RegistDBMsgDispatcher<DBMessageDispatcher>() == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.registDBMsgDispatcher<DBMessageDispatcher>(true) failed");
                    return false;
                }

                if (TcpHelper.Instance.StartAccept<LogSession, LogServerMessageDispatcher>(ServerIP, ServerPort, 500) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.StartAccept<BinaryClientSession, BinaryMessageDispatcher>() failed");
                    return false;
                }

                IsInit = true;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "MNF init failed");
                IsInit = false;
            }

            LogManager.Instance.Write("init MNF_Manager result: " + IsInit);

            return IsInit;
        }

        public void stop()
        {
            TcpHelper.Instance.Stop();
            LogManager.Instance.Release();
        }
    }
}
