﻿using System;
using System.IO;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MNF;

namespace LogServer.MNF_Library
{
    class DBConnectionInfo
    {
        public string server;
        public string database;
        public string uid;
        public string pwd;

        public override string ToString()
        {
            return string.Format(
                "server={0};database={1};uid={2};pwd={3};",
                server, database, uid, pwd);
        }
    }

    class DBDirector : Singleton<DBDirector>
    {
        string fileName = Application.StartupPath + "..\\DBInfo.txt";

        public DBConnectionInfo DBConnInfo { get; set; }
        public string ErrorString { get; set; }
        public MySqlConnection Connection
        {
            get;
            private set;
        }

        #region DBConnectionInfo
        public bool readDBConnInfo()
        {
            try
            {
                DBConnInfo = new DBConnectionInfo();
                using (StreamReader sr = new StreamReader(fileName))
                {
                    DBConnInfo.server = sr.ReadLine();
                    DBConnInfo.database = sr.ReadLine();
                    DBConnInfo.uid = sr.ReadLine();
                    DBConnInfo.pwd = sr.ReadLine();
                }
                return true;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "DB Connection info read failed");
                return false;
            }
        }

        public bool writeDBConnInfo()
        {
            try
            {
                // save option string
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    sw.WriteLine(DBConnInfo.server);
                    sw.WriteLine(DBConnInfo.database);
                    sw.WriteLine(DBConnInfo.uid);
                    sw.WriteLine(DBConnInfo.pwd);
                }
                return true;
            }
            catch(Exception e)
            {
                LogManager.Instance.WriteException(e, "DB Connection info write failed");
                return false;
            }
        }
        #endregion DBConnectionInfo

        #region DBSetting
        public bool connectToDB()
        {
            if (DBConnInfo == null)
            {
                if (readDBConnInfo() == false)
                    return false;
            }

            try
            {
                Connection = new MySqlConnection();
                Connection.ConnectionString = DBConnInfo.ToString();
                Connection.Open();

                writeDBConnInfo();
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 0:
                        LogManager.Instance.WriteException(
                            e, "Cannot connect to server. Contact administrator : %s, %d", DBConnInfo.ToString(), e.Number);
                        break;

                    case 1045:
                        LogManager.Instance.WriteException(
                            e, "Invalid username/password, please try again : %s, %d", DBConnInfo.ToString(), e.Number);
                        break;

                    default:
                        LogManager.Instance.WriteException(
                            e, "Cannot connect to server : %s, %d", DBConnInfo.ToString(), e.Number);
                        break;
                }
                return false;
            }
            return true;
        }

        public MySqlCommand createMySqlCommand(string query)
        {
            try
            {
                var cmd = new MySqlCommand();
                cmd.Connection = Connection;
                cmd.CommandText = query;
                cmd.Prepare();
                return cmd;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "DB mysql command create failed : %s", query);
                return null;
            }
        }
        #endregion DBSetting
    }
}
