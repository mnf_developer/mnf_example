﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MNF;

namespace LogServer.MNF_Library
{
    public class CLogWriter : ILogWriter
    {
        private BinaryFormatter binaryFormatter = new BinaryFormatter();
        private FileStream file = null;
        private Forms.Form_Main formMain = null;

        public override bool OnInit()
        {
            formMain = Forms.FormsUtility.GetFormMain();
            file = File.Create("MNFLog.log");
            return true;
        }

        public override void OnRelease()
        {
            binaryFormatter = null;
            formMain = null;
            file.Close();
        }

        public override bool OnWrite(ENUM_LOG_TYPE logType, string logString)
        {
            binaryFormatter.Serialize(file, logString);
            Console.WriteLine(logString);

            var logOutput = formMain.getLogServerOutputTextBox();
            logOutput.InvokeIfNeeded(
                formMain.writeListBoxLogOutput, 
                string.Format("{0} : {1}", logType.ToString(), logString));

            return true;
        }
    }
}
