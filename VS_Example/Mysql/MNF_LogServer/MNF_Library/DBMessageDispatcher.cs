﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using MNF;

namespace LogServer.MNF_Library
{
    public class CustomMessage
    {
        public SessionBase session;
        public object messageData;
    }

    public class DBMessageDispatcher : CustomDispatchHelper<CustomMessage>
    {
        Dictionary<DB_MESSAGE_TYPE, MySqlCommand> Commands { get; set; }

        public enum DB_MESSAGE_TYPE
        {
            DB_MESSAGE_WRITE_LOG,
        }

        public class DBLogInfo
        {
            [System.ComponentModel.DefaultValue("")]
            public string IP { get; set; }

            public string Session { get; set; }

            [System.ComponentModel.DefaultValue("")]
            public string LogType { get; set; }

            [System.ComponentModel.DefaultValue("")]
            public string Log { get; set; }
        }

        protected override bool OnInit()
        {
            if (ExportFunctionFromEnum<DB_MESSAGE_TYPE>() == false)
                return false;

            Commands = new Dictionary<DB_MESSAGE_TYPE, MySqlCommand>();

            var cmd = DBDirector.Instance.createMySqlCommand(
                    "insert into client_log (ip, session, logType, log) values (@ip, @session, @logType, @log)");
            if (cmd == null)
                return false;

            Commands.Add(DB_MESSAGE_TYPE.DB_MESSAGE_WRITE_LOG, cmd);

            return true;
        }

        int onDB_MESSAGE_WRITE_LOG(CustomMessage dbMessage)
        {
            var dbLogInfo = dbMessage.messageData as DBLogInfo;
            if (dbLogInfo == null)
                return -1;

            var cmd = Commands[DB_MESSAGE_TYPE.DB_MESSAGE_WRITE_LOG];
            try
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@ip", dbLogInfo.IP);
                cmd.Parameters.AddWithValue("@session", dbLogInfo.Session);
                cmd.Parameters.AddWithValue("@logType", dbLogInfo.LogType);
                cmd.Parameters.AddWithValue("@log", dbLogInfo.Log);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "%s query failed", cmd.CommandText);
                return -2;
            }
            return 0;
        }
    }
}
