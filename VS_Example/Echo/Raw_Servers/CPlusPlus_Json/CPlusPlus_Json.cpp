// CPlusPlus_Binary.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"

#define DEFAULT_BUFLEN 10240
#define DEFAULT_PORT "20000"

enum ENUM_CS_
{
	CS_JSON_ECHO = 345,
};
enum ENUM_SC_
{
	SC_JSON_ECHO = 234,
};

void procPacketParsing(SOCKET ClientSocket, int iResult, char recvbuf[DEFAULT_BUFLEN], int recvbuflen, int& jsonBufferIndex, char jsonPacketBuffer[DEFAULT_BUFLEN])
{
	CopyMemory(&jsonPacketBuffer[jsonBufferIndex], recvbuf, iResult);
	jsonBufferIndex += iResult;

	const int jsonPacketHeaderSize = sizeof(short) + sizeof(short);
	
	if (jsonBufferIndex < jsonPacketHeaderSize)
		return;

	short packetSize = 0;
	CopyMemory(&packetSize, &jsonPacketBuffer[0], sizeof(packetSize));
	
	if (jsonBufferIndex < packetSize)
		return;

	short packetId = 0;
	CopyMemory(&packetId, &jsonPacketBuffer[sizeof(packetSize)], sizeof(packetId));

	printf("Echo Size : %d, Id : %d\n", jsonPacketHeaderSize, packetId);

	jsonBufferIndex -= (jsonPacketHeaderSize + packetSize);

	char jsonString[10240] = { '\0', };
	CopyMemory(jsonString, &jsonPacketBuffer[jsonPacketHeaderSize], packetSize);
	
	rapidjson::Document doc;
	if (doc.Parse(jsonString).HasParseError() == true)
	{
		printf("json parsing error(offset %u): %s\n",
			(unsigned)doc.GetErrorOffset(), 
			rapidjson::GetParseError_En(doc.GetParseError()));
	}

	packetId = SC_JSON_ECHO;
	CopyMemory(&jsonPacketBuffer[sizeof(packetSize)], &packetId, sizeof(packetId));
	for (int i = 0; i < jsonPacketHeaderSize + packetSize; ++i)
	{
		if (send(ClientSocket, &jsonPacketBuffer[i], 1, 0) == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return;
		}
	}
}

int main()
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	SOCKET ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// No longer need server socket
	closesocket(ListenSocket);

	// Receive until the peer shuts down the connection
	char recvbuf[15];
	int recvbuflen = sizeof(recvbuf);

	int totalRecvSize = 0;
	int structBufferIndex = 0;
	char structBuffer[DEFAULT_BUFLEN];
	ZeroMemory(structBuffer, sizeof(structBuffer));
	do {
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			procPacketParsing(ClientSocket, iResult, recvbuf, recvbuflen, structBufferIndex, structBuffer);
			totalRecvSize += iResult;
			printf("Total Recv Size %d\n", totalRecvSize);
		}
		else if (iResult == 0)
			printf("Connection closing...\n");
		else {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

	} while (iResult > 0);

	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}

