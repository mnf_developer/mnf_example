// CPlusPlus_Binary.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"

#define DEFAULT_BUFLEN 10240
#define DEFAULT_PORT "10000"

enum ENUM_CS_
{
	CS_STRUCT_ECHO = 123,
};
enum ENUM_SC_
{
	SC_CLASS_ECHO = 234,
};

#pragma pack(push, 1)
struct stBinaryPacketHeader
{
	short size_;
	unsigned short id_;

	stBinaryPacketHeader()
		: size_(0), id_(0)
	{
	}
};

struct stPOINT
{
	int x;
	int y;
};

struct stCSEchoPacket : public stBinaryPacketHeader
{
	BOOL boolField;
	int intField;
	INT64 int64Field;
	int intArrayField[10];
	char stringField[100];
	stPOINT structField;
	stPOINT structArrayField[10];

	stCSEchoPacket()
	{
		ZeroMemory(this, sizeof(stCSEchoPacket));
	}
};

struct stSCEchoPacket : public stBinaryPacketHeader
{
	stPOINT structArrayField[10];
	stPOINT structField;
	char stringField[100];
	int intArrayField[10];
	INT64 int64Field;
	int intField;
	BOOL boolField;

	stSCEchoPacket()
	{
		ZeroMemory(this, sizeof(stSCEchoPacket));
	}
};
#pragma pack(pop)

void procPacketParsing(SOCKET ClientSocket, int iResult, char recvbuf[DEFAULT_BUFLEN], int recvbuflen, int& structBufferIndex, char structBuffer[DEFAULT_BUFLEN])
{
	CopyMemory(&structBuffer[structBufferIndex], recvbuf, iResult);
	structBufferIndex += iResult;

	stCSEchoPacket csEchoPacket;
	stSCEchoPacket scEchoPacket;

	if (structBufferIndex >= sizeof(csEchoPacket))
	{
		structBufferIndex -= sizeof(csEchoPacket);
		
		// recv
		CopyMemory(&csEchoPacket, structBuffer, sizeof(stCSEchoPacket));
		scEchoPacket.size_ = sizeof(scEchoPacket) - sizeof(stBinaryPacketHeader);
		scEchoPacket.id_ = ENUM_SC_::SC_CLASS_ECHO;

		// send
		CopyMemory(scEchoPacket.structArrayField, csEchoPacket.structArrayField, sizeof(scEchoPacket.structArrayField));
		scEchoPacket.structField = csEchoPacket.structField;
		CopyMemory(scEchoPacket.stringField, csEchoPacket.stringField, sizeof(scEchoPacket.stringField));
		CopyMemory(scEchoPacket.intArrayField, csEchoPacket.intArrayField, sizeof(scEchoPacket.intArrayField));
		scEchoPacket.int64Field = csEchoPacket.int64Field;
		scEchoPacket.intField = csEchoPacket.intField;
		scEchoPacket.boolField = csEchoPacket.boolField;
		
		for (size_t i = 0; i < sizeof(scEchoPacket); ++i)
		{
			char* slicedBuffer = (char*)&scEchoPacket;
			if (send(ClientSocket, &slicedBuffer[i], 1, 0) == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(ClientSocket);
				WSACleanup();
				return;
			}
		}

		printf("Echo %llu\n", sizeof(scEchoPacket));
	}
}

int main()
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	SOCKET ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// No longer need server socket
	closesocket(ListenSocket);

	// Receive until the peer shuts down the connection
	char recvbuf[15];
	int recvbuflen = sizeof(recvbuf);

	int totalRecvSize = 0;
	int structBufferIndex = 0;
	char structBuffer[DEFAULT_BUFLEN];
	ZeroMemory(structBuffer, sizeof(structBuffer));
	do {
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			procPacketParsing(ClientSocket, iResult, recvbuf, recvbuflen, structBufferIndex, structBuffer);

			totalRecvSize += iResult;
			printf("Total Recv Size : %d\n", totalRecvSize);
		}
		else if (iResult == 0)
			printf("Connection closing...\n");
		else {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

	} while (iResult > 0);

	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}

