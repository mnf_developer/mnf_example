﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using MNF_Common;

namespace CSharp_Binary
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class BinaryMessageHeader
    {
        [MarshalAs(UnmanagedType.I2)]
        public short size;
        [MarshalAs(UnmanagedType.U2)]
        public ushort packetId;
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpListener tcpListener = new TcpListener(IPAddress.Any, 10000);
                tcpListener.Start();

                Socket clientsoket = tcpListener.AcceptSocket();
                Console.WriteLine("Client {0} accepted", clientsoket.RemoteEndPoint.ToString());

                NetworkStream stream = new NetworkStream(clientsoket);
                byte[] readBuffer = new byte[15];
                byte[] echoPacketBuffer = new byte[10240];

	            int structBufferIndex = 0;
                int totalRecvSize = 0;
                while (true)
                {
                    int readSize = stream.Read(readBuffer, 0, readBuffer.Length);
                    totalRecvSize += readSize;
                    Console.WriteLine("Total recv {0} bytes", totalRecvSize);

                    structBufferIndex = procPacketParsing(stream, readBuffer, echoPacketBuffer, structBufferIndex, readSize);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static int procPacketParsing(NetworkStream stream, byte[] readBuffer, byte[] echoPacketBuffer, int structBufferIndex, int readSize)
        {
            Array.Copy(readBuffer, 0, echoPacketBuffer, structBufferIndex, readSize);
            structBufferIndex += readSize;

            int headerSize = Marshal.SizeOf(typeof(BinaryMessageHeader));
            int csStructEchoSize = Marshal.SizeOf(typeof(BinaryMessageDefine.PACK_CS_ECHO));
            if (structBufferIndex >= (headerSize + csStructEchoSize))
            {
                structBufferIndex -= (headerSize + csStructEchoSize);

                BinaryMessageDefine.PACK_CS_ECHO csEchoPacket;
                {
                    IntPtr marshalBuffer = Marshal.AllocHGlobal(csStructEchoSize);
                    Marshal.Copy(echoPacketBuffer, headerSize, marshalBuffer, csStructEchoSize);
                    csEchoPacket = (BinaryMessageDefine.PACK_CS_ECHO)Marshal.PtrToStructure(
                        marshalBuffer, typeof(BinaryMessageDefine.PACK_CS_ECHO));
                    Marshal.FreeHGlobal(marshalBuffer);
                }

                // send
                var scEchoPacket = new BinaryMessageDefine.PACK_SC_ECHO();
                csEchoPacket.structArrayField.CopyTo(scEchoPacket.structArrayField, 0);
                scEchoPacket.structField = csEchoPacket.structField;
                scEchoPacket.stringField = csEchoPacket.stringField;
                csEchoPacket.intArrayField.CopyTo(scEchoPacket.intArrayField, 0);
                scEchoPacket.int64Field = csEchoPacket.int64Field;
                scEchoPacket.intField = csEchoPacket.intField;
                scEchoPacket.boolField = csEchoPacket.boolField;
                {
                    // setting header
                    var header = new BinaryMessageHeader();
                    header.size = (short)Marshal.SizeOf(typeof(BinaryMessageDefine.PACK_SC_ECHO));
                    header.packetId = (int)BinaryMessageDefine.ENUM_SC_.SC_ECHO;

                    var rawHeader = new byte[Marshal.SizeOf(typeof(BinaryMessageHeader))];
                    IntPtr headerBuffer = Marshal.UnsafeAddrOfPinnedArrayElement(rawHeader, 0);
                    Marshal.StructureToPtr(header, headerBuffer, false);
                    for (int i = 0; i < rawHeader.Length; ++i)
                    {
                        stream.Write(rawHeader, i, 1);
                    }

                    // setting data
                    var RawData = new byte[Marshal.SizeOf(typeof(BinaryMessageDefine.PACK_SC_ECHO))];
                    IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(RawData, 0);
                    Marshal.StructureToPtr(scEchoPacket, buffer, false);
                    for (int i = 0; i < RawData.Length; ++i)
                    {
                        stream.Write(RawData, i, 1);
                    }
                    Console.WriteLine("Echo {0} bytes", RawData.Length);
                }
            }

            Console.WriteLine("Struct Buffer Index {0}", structBufferIndex);

            return structBufferIndex;
        }
    }
}
