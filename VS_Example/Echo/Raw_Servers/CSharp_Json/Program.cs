﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using MNF_Common;

namespace CSharp_Binary
{
    public enum ENUM_CS_
    {
        CS_JSON_ECHO = 345,
    }
    public enum ENUM_SC_
    {
        SC_JSON_ECHO = 234,
    }

    [System.Serializable]
    public class JsonMessageHeader
    {
        public short messageSize;
        public short messageID;

        static public int getHeaderSize()
        {
            return getMessageSizeFieldSize() + getMessageIdFieldSize();
        }

        static public int getMessageSizeFieldSize()
        {
            return sizeof(short);
        }

        static public int getMessageIdFieldSize()
        {
            return sizeof(short);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpListener tcpListener = new TcpListener(IPAddress.Any, 20000);
                tcpListener.Start();

                Socket clientsoket = tcpListener.AcceptSocket();
                NetworkStream stream = new NetworkStream(clientsoket);
                byte[] readBuffer = new byte[15];
                byte[] echoPacketBuffer = new byte[10240];

	            int echoPacketBufferIndex = 0;
                while (true)
                {
                    // recv
                    int readSize = stream.Read(readBuffer, 0, readBuffer.Length);
                    procPacketParsing(stream, readBuffer, echoPacketBuffer, readSize, ref echoPacketBufferIndex);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        static void procPacketParsing(NetworkStream stream, byte[] readBuffer, byte[] echoPacketBuffer, int readSize, ref int echoPacketBufferIndex)
        {
            Array.Copy(readBuffer, 0, echoPacketBuffer, echoPacketBufferIndex, readSize);
            echoPacketBufferIndex += readSize;

            if (readSize < JsonMessageHeader.getHeaderSize())
                return;

            byte[] packetSizeBuffer = new byte[2];
            Array.Copy(echoPacketBuffer, 0, packetSizeBuffer, 0, JsonMessageHeader.getMessageSizeFieldSize());
            byte[] packetIdBuffer = new byte[2];
            Array.Copy(echoPacketBuffer, JsonMessageHeader.getMessageSizeFieldSize(), packetIdBuffer, 0, JsonMessageHeader.getMessageIdFieldSize());

            short packetSize = BitConverter.ToInt16(packetSizeBuffer, 0);
            short packetId = BitConverter.ToInt16(packetIdBuffer, 0);

            if (echoPacketBufferIndex < (JsonMessageHeader.getHeaderSize() + packetSize))
                return;

            echoPacketBufferIndex -= (JsonMessageHeader.getHeaderSize() + packetSize);

            string jsonString = Encoding.Default.GetString(
                echoPacketBuffer,
                JsonMessageHeader.getHeaderSize(),
                packetSize);
            Console.WriteLine(jsonString);

			var deserializedJson = (JsonMessageDefine.PACK_CS_JSON_ECHO)JsonFx.Json.JsonReader.Deserialize(
                jsonString, typeof(JsonMessageDefine.PACK_CS_JSON_ECHO));
            Console.WriteLine(deserializedJson);

            short ackMessageID = (short)ENUM_SC_.SC_JSON_ECHO;
            Array.Copy(BitConverter.GetBytes(ackMessageID), 0, echoPacketBuffer, JsonMessageHeader.getMessageSizeFieldSize(), 2);

            for (int i = 0; i < +(JsonMessageHeader.getHeaderSize() + packetSize); ++i)
                stream.Write(echoPacketBuffer, i, 1);
        }
    }
}
