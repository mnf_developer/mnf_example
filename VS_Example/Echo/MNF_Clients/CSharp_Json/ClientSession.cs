﻿using MNF_Common;
using MNF;

public class ClientSession : JsonSession
{
    public override int OnConnectSuccess()
    {
        LogManager.Instance.Write("OnConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var jsonEcho = new JsonMessageDefine.PACK_CS_JSON_ECHO();
        jsonEcho.sandwiches = Sandwich.createSandwichList(10);
        //syncSend((int)JsonMessageDefine.ENUM_CS_.CS_JSON_ECHO, ref jsonEcho);
        AsyncSend((int)JsonMessageDefine.ENUM_CS_.CS_JSON_ECHO, jsonEcho);

        return 0;
    }

    public override int OnConnectFail()
    {
        LogManager.Instance.Write("OnConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int OnDisconnect()
    {
        LogManager.Instance.Write("OnDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}