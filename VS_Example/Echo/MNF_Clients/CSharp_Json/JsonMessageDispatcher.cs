﻿using MNF_Common;
using MNF;

public class JsonMessageDispatcher : DefaultDispatchHelper<ClientSession, JsonMessageDefine, JsonMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_JSON_ECHO(ClientSession session, object message)
    {
        var echo = (JsonMessageDefine.PACK_SC_JSON_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var jsonEcho = new JsonMessageDefine.PACK_CS_JSON_ECHO();
        jsonEcho.sandwiches = echo.sandwiches;
        session.AsyncSend((int)JsonMessageDefine.ENUM_CS_.CS_JSON_ECHO, jsonEcho);

        return 0;
    }
}
