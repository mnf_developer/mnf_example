﻿using System;
using System.Threading;
using MNF;
using MNF_Common;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            LogManager.Instance.SetLogWriter(new ConsoleLogWriter());
            if (LogManager.Instance.Init() == false)
                Console.WriteLine("LogWriter init failed");

            try
            {
                string serverIP = Utility.getLocalAddress().ToString();
                string serverPort = "10000";

                if (TcpHelper.Instance.Start(false) == false)
                {
                    Console.WriteLine("TcpHelper.Instance.Start() failed");
                    return;
                }

                if (TcpHelper.Instance.AsyncConnect<ClientSession, BinaryMessageDispatcher>(serverIP, serverPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}, {1}) failed", serverIP, serverPort);
                    return;
                }
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "run failed");
            }

            while (true)
            {
                TcpHelper.Instance.DipatchNetworkInterMessage();
                Thread.Sleep(10);
            }
        }
    }
}
