﻿using MNF_Common;
using MNF;

public class BinaryMessageDispatcher : DefaultDispatchHelper<ClientSession, BinaryMessageDefine, BinaryMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_ECHO(ClientSession session, object message)
    {
        var echo = (BinaryMessageDefine.PACK_SC_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var ecshoPacket = new BinaryMessageDefine.PACK_CS_ECHO();
        session.AsyncSend((int)BinaryMessageDefine.ENUM_CS_.CS_ECHO, ecshoPacket);

        return 0;
    }
}
