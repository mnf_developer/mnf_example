﻿using MNF;

public class CustomMessage
{
    public SessionBase session;
    public object messageData;
}

public class InterMessageDispatcher : CustomDispatchHelper<CustomMessage>
{
    public class InterDummy
    {
        [System.ComponentModel.DefaultValue("")]
        public string Dummy { get; set; }
    }

    public enum INTER_MESSAGE_TYPE
    {
        INTER_MESSAGE_DUMMY,
    }

    protected override bool OnInit()
    {
        if (ExportFunctionFromEnum<INTER_MESSAGE_TYPE>() == false)
            return false;

        return true;
    }

    private int onINTER_MESSAGE_DUMMY(CustomMessage interMessage)
    {
        return 0;
    }
}