﻿using MNF;

public class DBMessageDispatcher : CustomDispatchHelper<CustomMessage>
{
    public class DBDummy
    {
        [System.ComponentModel.DefaultValue("")]
        public string Dummy { get; set; }
    }

    public enum DB_MESSAGE_TYPE
    {
        DB_MESSAGE_DUMMY,
    }

    protected override bool OnInit()
    {
        if (ExportFunctionFromEnum<DB_MESSAGE_TYPE>() == false)
            return false;

        return true;
    }

    int onDB_MESSAGE_DUMMY(CustomMessage dbMessage)
    {
        var interDummy = new InterMessageDispatcher.InterDummy();
        interDummy.Dummy = "DB Dummy Data From DB";

        CustomMessage customMessage = new CustomMessage();
        customMessage.session = dbMessage.session;
        customMessage.messageData = interDummy;

        if (TcpHelper.Instance.RequestInterMessage(
            (int)InterMessageDispatcher.INTER_MESSAGE_TYPE.INTER_MESSAGE_DUMMY
            , customMessage) == false)
            return -1;

        return 0;
    }
}