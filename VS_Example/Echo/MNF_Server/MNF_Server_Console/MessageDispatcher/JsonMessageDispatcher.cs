﻿using System;
using MNF_Common;
using MNF;

public class JsonMessageDispatcher : DefaultDispatchHelper<JsonServerSession, JsonMessageDefine, JsonMessageDefine.ENUM_CS_>
{
    int count = 0;

    int onCS_JSON_ECHO(JsonServerSession session, object message)
    {
        var echo = (JsonMessageDefine.PACK_CS_JSON_ECHO)message;
        if (++count % 100 == 0)
            Console.WriteLine("{0}, {1}, {2}", session, echo.GetType(), count);

        var jsonEcho = new JsonMessageDefine.PACK_SC_JSON_ECHO();
        jsonEcho.sandwiches = echo.sandwiches;
        session.AsyncSend((int)JsonMessageDefine.ENUM_SC_.SC_JSON_ECHO, jsonEcho);

        var dbDummy = new DBMessageDispatcher.DBDummy();
        dbDummy.Dummy = "DB Dummy Data From Json";

        CustomMessage customMessage = new CustomMessage();
        customMessage.session = session;
        customMessage.messageData = dbDummy;

        if (TcpHelper.Instance.RequestDBMessage(
            (int)DBMessageDispatcher.DB_MESSAGE_TYPE.DB_MESSAGE_DUMMY
            , customMessage) == false)
            return -1;

        return 0;
    }
}