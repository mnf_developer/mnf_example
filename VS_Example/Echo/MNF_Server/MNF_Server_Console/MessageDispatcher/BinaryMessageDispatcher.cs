﻿using System;
using MNF_Common;
using MNF;

public class BinaryMessageDispatcher : DefaultDispatchHelper<BinaryServerSession, BinaryMessageDefine, BinaryMessageDefine.ENUM_CS_>
{
    int count = 0;

    int onCS_ECHO(BinarySession session, object message)
    {
        var echo = (BinaryMessageDefine.PACK_CS_ECHO)message;
        if (++count % 100 == 0)
            Console.WriteLine("{0}, {1}, {2}", session, echo.GetType(), count);

        var echoPacket = new BinaryMessageDefine.PACK_SC_ECHO();
        session.AsyncSend((int)BinaryMessageDefine.ENUM_SC_.SC_ECHO, echoPacket);

        var dbDummy = new DBMessageDispatcher.DBDummy();
        dbDummy.Dummy = "DB Dummy Data From Binary";

        CustomMessage customMessage = new CustomMessage();
        customMessage.session = session;
        customMessage.messageData = dbDummy;
        
        if (TcpHelper.Instance.RequestDBMessage(
            (int)DBMessageDispatcher.DB_MESSAGE_TYPE.DB_MESSAGE_DUMMY
            , customMessage) == false)
            return -1;

        return 0;
    }
}