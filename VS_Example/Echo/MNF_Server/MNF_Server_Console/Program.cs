﻿using System;
using System.Threading;
using MNF_Common;
using MNF;

namespace MNF_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            LogManager.Instance.SetLogWriter(new ConsoleLogWriter());
            if (LogManager.Instance.Init() == false)
                Console.WriteLine("LogWriter init failed");

            try
            {
                string serverIP = Utility.getLocalAddress().ToString();
                string binaryServerPort = "10000";
                string jsonServerPort = "20000";

                if (TcpHelper.Instance.Start(false) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.Start() failed");
                    return;
                }

                if (TcpHelper.Instance.RegistDBMsgDispatcher<DBMessageDispatcher>() == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.registDBMsgDispatcher<DBMessageDispatcher>(true) failed");
                    return;
                }

                if (TcpHelper.Instance.RegistInterMsgDispatcher<InterMessageDispatcher>() == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.registInterMsgDispatcher<InterMessageDispatcher>() failed");
                    return;
                }

                if (TcpHelper.Instance.StartAccept<BinaryServerSession, BinaryMessageDispatcher>(serverIP, binaryServerPort, 500) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.StartAccept<BinaryClientSession, BinaryMessageDispatcher>() failed");
                    return;
                }

                if (TcpHelper.Instance.StartAccept<JsonServerSession, JsonMessageDispatcher>(serverIP, jsonServerPort, 500) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.StartAccept<JsonClientSession, JsonMessageDispatcher>() failed");
                    return;
                }

                LogManager.Instance.Write("run server, binary({0}), json({1})", binaryServerPort, jsonServerPort);

                while (true)
                {
                    TcpHelper.Instance.DipatchNetworkInterMessage();
                    Thread.Sleep(10);
                }
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "run failed");
            }
        }
    }
}
