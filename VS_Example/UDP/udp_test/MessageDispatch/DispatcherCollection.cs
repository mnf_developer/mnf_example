﻿using System.Collections.Generic;
using System.Threading;

namespace MNF
{
    internal enum DISPATCH_TYPE
    {
        DISPATCH_NONE,
        DISPATCH_NETWORK_INTERNAL,
        DISPATCH_DB,
    }

    internal class DispatcherCollection : Singleton<DispatcherCollection>
    {
        private class DispatcherThread
        {
            public DispatcherThread(DISPATCH_TYPE dispatchType, bool isRunThread)
            {
                MessageEvent = new AutoResetEvent(false);
                Dispatcher = new Dispatcher(new DoEventNotifier(MessageEvent), dispatchType);
                if (isRunThread == true)
                {
                    ThreadAdapter = new ThreadAdapter(MessageEvent);
                    ThreadAdapter.ThreadEvent += Dispatcher.dispatchMessage;
                }
            }

            public AutoResetEvent MessageEvent { get; set; }
            public Dispatcher Dispatcher { get; set; }
            public ThreadAdapter ThreadAdapter { get; set; }
        }

        private Dictionary<DISPATCH_TYPE, DispatcherThread> dispatcherThreads = null;

        public DispatcherCollection()
        {
            dispatcherThreads = new Dictionary<DISPATCH_TYPE, DispatcherThread>();
        }

        public Dispatcher initDispatcher(DISPATCH_TYPE dispatchType, bool isRunThread)
        {
            if (dispatcherThreads.ContainsKey(dispatchType) == true)
                return dispatcherThreads[dispatchType].Dispatcher;

            var dispatcherThread = new DispatcherThread(dispatchType, isRunThread);
            dispatcherThreads.Add(dispatchType, dispatcherThread);

            if (isRunThread == true)
                dispatcherThread.ThreadAdapter.start();

            return dispatcherThread.Dispatcher;
        }

        public bool pushMessage(DISPATCH_TYPE dispatchType, ref NF_Message message)
        {
            DispatcherThread dispatcherThread = null;
            if (dispatcherThreads.TryGetValue(dispatchType, out dispatcherThread) == false)
                return false;
            return dispatcherThread.Dispatcher.pushMessage(ref message);
        }

        public Dispatcher getDispatcher(DISPATCH_TYPE dispatchType)
        {
            DispatcherThread dispatcherThread = null;
            if (dispatcherThreads.TryGetValue(dispatchType, out dispatcherThread) == false)
                return null;
            return dispatcherThread.Dispatcher;
        }

        public void stop()
        {
            foreach (var dispatcherThread in dispatcherThreads)
            {
                if (dispatcherThread.Value.ThreadAdapter != null)
                    dispatcherThread.Value.ThreadAdapter.stop();
            }
        }
    }
}
