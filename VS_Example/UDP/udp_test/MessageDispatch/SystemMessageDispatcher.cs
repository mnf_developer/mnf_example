﻿using System;
using System.Diagnostics;

namespace MNF
{
    public abstract class SystemMessageDispatcher
    {
        public delegate bool onSystemMessageDispatch(ref NF_Message message);
        public event onSystemMessageDispatch SystemMessageEvent;
        private bool isInit = false;

        [System.ComponentModel.DefaultValue(null)]
        public onDispatch AcceptDispatcher { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public onDispatch ConnectSuccessDispatcher { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public onDispatch ConnectFailDispatcher { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public onDispatch DisconnectDispatcher { get; set; }

        internal bool init()
        {
            if (isInit == true)
                return true;

            try
            {
                var dispatcher = DispatcherCollection.Instance.getDispatcher(DISPATCH_TYPE.DISPATCH_NETWORK_INTERNAL);
                if (dispatcher == null)
                    throw new Exception("Network/Internal dispatch didn't create");

                SystemMessageEvent +=
                    (SystemMessageDispatcher.onSystemMessageDispatch)
                    Utility.loadDelegate(dispatcher, "pushMessage");

                AcceptDispatcher = onAccept;
                ConnectSuccessDispatcher = onConnectSuccess;
                ConnectFailDispatcher = onConnectFail;
                DisconnectDispatcher = onDisconnect;

                isInit = onInit();

                return isInit;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "SystemMessageDispatcher object failed to create");
                return false;
            }
        }

        public void pushAcceptMessage(Session session)
        {
            Debug.Assert(AcceptDispatcher != null);
            NF_Message message = new NF_Message(session, AcceptDispatcher, null);
            SystemMessageEvent(ref message);
        }

        public void pushConnectSuccessMessage(Session session)
        {
            Debug.Assert(ConnectSuccessDispatcher != null);
            NF_Message message = new NF_Message(session, ConnectSuccessDispatcher, null);
            SystemMessageEvent(ref message);
        }

        public void pushConnectFailMessage(Session session)
        {
            Debug.Assert(ConnectFailDispatcher != null);
            NF_Message message = new NF_Message(session, ConnectFailDispatcher, null);
            SystemMessageEvent(ref message);
        }

        public void pushDisconnectMessage(Session session)
        {
            Debug.Assert(DisconnectDispatcher != null);
            NF_Message message = new NF_Message(session, DisconnectDispatcher, null);
            SystemMessageEvent(ref message);
        }

        public abstract bool onInit();
        public abstract int onAccept(Session session, object packet);
        public abstract int onConnectSuccess(Session session, object packet);
        public abstract int onConnectFail(Session session, object packet);
        public abstract int onDisconnect(Session session, object packet);

        private bool onEmptySystemMessageDispatch(ref NF_Message message)
        {
            return true;
        }
    }
}
