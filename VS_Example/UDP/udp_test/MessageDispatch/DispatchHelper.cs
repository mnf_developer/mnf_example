﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace MNF
{
    public abstract class DispatchHelper
    {
        private Dictionary<int/*message id*/, DispatchInfo> dispatchList = null;
        private bool isInit = false;

        public DispatchHelper()
        {
            dispatchList = new Dictionary<int, DispatchInfo>();
        }

        internal bool init()
        {
            if (isInit == true)
                return true;

            isInit = onInit();

            return isInit;
        }

        public abstract bool onInit();

        // function
        public bool exportFunctionFromEnum<T>(object targetObject)
        {
            var dict = Utility.EnumDictionary<T>();
            foreach (var enumMessage in dict)
            {
                string functionName = "on" + enumMessage.Value;
                try
                {
                    MethodInfo methodInfo = targetObject.GetType().GetMethod(functionName,
                                   BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
                    if (methodInfo == null)
                        throw new Exception(string.Format("{0} Function not Found", functionName));

                    Delegate dispatch = Delegate.CreateDelegate(typeof(onDispatch), targetObject, methodInfo);
                    if (dispatch == null)
                        throw new Exception(string.Format("{0} Dispatch Create Failed", functionName));

                    DispatchInfo dispatchInfo = tryGetMessageDispatch(enumMessage.Key);
                    if (dispatchInfo == null)
                        dispatchList.Add(enumMessage.Key, new DispatchInfo((onDispatch)dispatch));
                    else
                        dispatchInfo.dispatcher = (onDispatch)dispatch;
                }
                catch (Exception e)
                {
                    LogManager.Instance.WriteException(e, "Enum({0}) EnumValue({1}) Function({2}) Exception({3}) from {4}",
                            typeof(T).ToString(), enumMessage.Value, functionName, e.ToString(), targetObject.ToString());
                    return false;
                }
            }
            return true;
        }

        // class
        public bool exportClassFromEnum<TClass, TEnum>()
        {
            Assembly mscorlib = typeof(TClass).Assembly;
            var dict = Utility.EnumDictionary<TEnum>();
            foreach (var enumMessage in dict)
            {
                string packetName = typeof(TClass).ToString() + "+PACK_" + enumMessage.Value;
                try
                {
                    Type packetType = null;
                    foreach (Type type in mscorlib.GetTypes())
                    {
                        if (type.ToString() == packetName)
                        {
                            packetType = type;
                            break;
                        }
                    }
                    if (packetType == null)
                        throw new Exception(string.Format("{0} Packet Invalid", packetName));

                    DispatchInfo dispatchInfo = tryGetMessageDispatch(enumMessage.Key);
                    if (dispatchInfo == null)
                    {
                        dispatchInfo = new DispatchInfo(null);
                        dispatchInfo.packetType = packetType;
                        dispatchList.Add(enumMessage.Key, dispatchInfo);
                    }
                    else
                    {
                        dispatchInfo.packetType = packetType;
                    }
                }
                catch (Exception e)
                {
                    LogManager.Instance.WriteException(e, "Enum({0}) EnumValue({1}) PacketName({2}) Exception({3})",
                            typeof(TClass).ToString(), enumMessage.Value, packetName, e.ToString());
                    return false;
                }
            }
            return true;
        }

        public DispatchInfo tryGetMessageDispatch(int messageType)
        {
            if (dispatchList.ContainsKey(messageType) == false)
                return null;

            DispatchInfo dispatchInfo = null;
            if (dispatchList.TryGetValue(messageType, out dispatchInfo) == false)
                return null;

            return dispatchInfo;
        }

        public bool tryCreateMessage(int messageType, Session session, object data, ref NF_Message outMessage)
        {
            DispatchInfo dispatchInfo = tryGetMessageDispatch(messageType);
            if (dispatchInfo == null)
                return false;

            outMessage = new NF_Message(session, dispatchInfo.dispatcher, data);
            return true;
        }
    }
}