﻿using System;

namespace MNF
{
    public delegate int onDispatch(Session session, object packet);

    public class DispatchInfo
    {
        public onDispatch dispatcher;
        public Type packetType;

        public DispatchInfo(onDispatch packetDispatch, Type packetType)
        {
            this.dispatcher = packetDispatch;
            this.packetType = packetType;
        }

        public DispatchInfo(onDispatch packetDispatch)
        {
            this.dispatcher = packetDispatch;
            this.packetType = null;
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", dispatcher.ToString(), packetType.ToString());
        }
    }
}
