﻿using System;
using System.Collections.Generic;

namespace MNF
{
    internal class DispatchExporterCollection : Singleton<DispatchExporterCollection>
    {
        private Dictionary<Type, DispatchHelper> messageDispatchExporters = null;

        public DispatchExporterCollection()
        {
            messageDispatchExporters = new Dictionary<Type, DispatchHelper>();
        }

        public bool add(Type type)
        {
            DispatchHelper dispatchExporter = null;
            if (messageDispatchExporters.TryGetValue(type, out dispatchExporter) == true)
                return true;

            dispatchExporter = Utility.GetInstance(type) as DispatchHelper;
            if (dispatchExporter == null)
                return false;

            messageDispatchExporters.Add(type, dispatchExporter);
            return true;
        }

        public DispatchHelper get(Type type)
        {
            DispatchHelper messageDipatchExporter = null;
            if (messageDispatchExporters.TryGetValue(type, out messageDipatchExporter) == false)
            {
                if (add(type) == false)
                    return null;
                messageDispatchExporters.TryGetValue(type, out messageDipatchExporter);
            }

            return messageDipatchExporter;
        }
    }
}
