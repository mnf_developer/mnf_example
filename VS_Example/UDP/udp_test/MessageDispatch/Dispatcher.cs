﻿using System;
using System.Diagnostics;

namespace MNF
{
    internal class Dispatcher
    {
        private DISPATCH_TYPE dispatchType = DISPATCH_TYPE.DISPATCH_NONE;
        private EventNofier eventNotifier = null;
        private Object lockQueue = new Object();
        private SwapableMessgeQueue<NF_Message> dispatchMessageQueue = new SwapableMessgeQueue<NF_Message>();

        public Dispatcher(EventNofier eventNotifier, DISPATCH_TYPE dispatchType)
        {
            Debug.Assert(dispatchType != DISPATCH_TYPE.DISPATCH_NONE);
            this.eventNotifier = eventNotifier;
            this.dispatchType = dispatchType;
        }

        public bool pushMessage(ref NF_Message message)
        {
            lock (lockQueue)
            {
                dispatchMessageQueue.getWritableQueue().Enqueue(message);
            }
            eventNotifier.notify();
            return true;
        }

        public void dispatchMessage()
        {
            lock (lockQueue)
            {
                if (dispatchMessageQueue.getWritableQueue().Count > 0)
                {
                    dispatchMessageQueue.swap();
                }
            }

            int dispatchCount = 0;
            while (dispatchMessageQueue.getReadableQueue().Count > 0)
            {
                NF_Message message = dispatchMessageQueue.getReadableQueue().Peek();
                int returnValue = message.execute();
                if (returnValue != 0)
                {
                    if (LogManager.Instance.isWriteLog(ENUM_LOG_TYPE.LOG_TYPE_SYSTEM_DEBUG) == true)
                    {
                        LogManager.Instance.WriteDebug("Dispatcher({0}) Message({1}) return value({2})",
                        dispatchType.ToString(), message.ToString(), returnValue);
                    }
                }

                dispatchMessageQueue.getReadableQueue().Dequeue();

                ++dispatchCount;
            }

            if (dispatchCount > 0)
            {
                if (LogManager.Instance.isWriteLog(ENUM_LOG_TYPE.LOG_TYPE_SYSTEM_DEBUG) == true)
                {
                    LogManager.Instance.WriteSystemDebug("Dispatcher({0}) dispatch count({1})", dispatchType.ToString(), dispatchCount);
                }
            }
        }
    }
}
