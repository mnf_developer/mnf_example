﻿using System;

namespace MNF
{
    public class BinarySerializeMessageFormatterData : ISerializeMessageFormatterData
    {
        public byte[] serializedMessage;

        public BinarySerializeMessageFormatterData()
        {
            serializedMessage = new byte[1024];
        }

        ~BinarySerializeMessageFormatterData()
        {
        }
    }

    public class BinaryDeserializeMessageFormatterData : IDeserializeMessageFormatterData
    {
        public byte[] messageSize;
        public byte[] deSerializedMessage;
        public IntPtr headerMarshalAllocatedBuffer;
        public int headerMarshalAllocatedBufferSize;
        public IntPtr marshalAllocatedBuffer;
        public int marshalAllocatedBufferSize;

        public BinaryDeserializeMessageFormatterData()
        {
            messageSize = new byte[2];
            deSerializedMessage = new byte[1024];
            headerMarshalAllocatedBufferSize = MarshalRef.getManagedDataSize(typeof(BinaryMessageHeader));
            headerMarshalAllocatedBuffer = MarshalRef.allocGlobalHeap(headerMarshalAllocatedBufferSize);
            marshalAllocatedBufferSize = 1024;
            marshalAllocatedBuffer = MarshalRef.allocGlobalHeap(marshalAllocatedBufferSize);
        }

        ~BinaryDeserializeMessageFormatterData()
        {
            MarshalRef.deAllocGlobalHeap(headerMarshalAllocatedBuffer);
            MarshalRef.deAllocGlobalHeap(marshalAllocatedBuffer);
        }
    }

    public class BinaryMessageFormatter : IMessageFormatter
    {
        internal override ISerializeMessageFormatterData allocSerializeMessageFormatterData()
        {
            return new BinarySerializeMessageFormatterData();
        }

        internal override IDeserializeMessageFormatterData allocDeserializeMessageFormatterData()
        {
            return new BinaryDeserializeMessageFormatterData();
        }

        public short _readMessageSize(CircularBuffer circularBuffer, BinaryDeserializeMessageFormatterData messageFormatterData)
        {
            try
            {
                if (circularBuffer.read(messageFormatterData.messageSize, messageFormatterData.messageSize.Length) == false)
                    throw new Exception("CircularBuffer read failed");

                return BitConverter.ToInt16(messageFormatterData.messageSize, 0);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Read packet size failed");
                return 0;
            }
        }

        public byte[] _readMessage(CircularBuffer circularBuffer, short packetSize, BinaryDeserializeMessageFormatterData messageFormatterData)
        {
            if (messageFormatterData.deSerializedMessage.Length < packetSize)
                messageFormatterData.deSerializedMessage = new byte[packetSize];

            if (circularBuffer.read(messageFormatterData.deSerializedMessage, packetSize) == true)
                return messageFormatterData.deSerializedMessage;

            return null;
        }

        public bool _deSerializeMessageHeader(byte[] serializedPacket, ref BinaryMessageHeader packetHeader, BinaryDeserializeMessageFormatterData messageFormatterData)
        {
            try
            {
                packetHeader = (BinaryMessageHeader)MarshalRef.rawDeSerialize(
                    serializedPacket
                    , BinaryMessageHeader.getHeaderType()
                    , ref messageFormatterData.headerMarshalAllocatedBuffer
                    , ref messageFormatterData.headerMarshalAllocatedBufferSize);
                return true;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "BinaryPacketHeader deSerialize Failed");
                return false;
            }
        }

        public void _deSerializeMessage(
            DispatchHelper dispatchExporter
            , BinaryDeserializeMessageFormatterData messageFormatterData
            , byte[] serializedPacket
            , ref BinaryMessageHeader messageHeader
            , out object message
            , out onDispatch dispatcher)
        {
            message = null;
            dispatcher = null;
            try
            {
                var dispatchInfo = dispatchExporter.tryGetMessageDispatch(messageHeader.messageID);
                message = MarshalRef.rawDeSerialize(
                    serializedPacket
                    , dispatchInfo.packetType
                    , ref messageFormatterData.marshalAllocatedBuffer
                    , ref messageFormatterData.marshalAllocatedBufferSize);
                if (message == null)
                    throw new Exception(string.Format("Dispatcher({0}), Expect packet size({1}), real packet size({2})",
                        dispatchInfo, MarshalRef.getManagedDataSize(dispatchInfo.packetType), messageHeader.messageSize));
                dispatcher = dispatchInfo.dispatcher;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Deserialize and dispatch failed, ID({0}) Length({1})",
                    messageHeader.messageID, messageHeader.messageSize);
            }
        }

        internal override byte[] serialize(object managedData, ref int serializedSize, ISerializeMessageFormatterData messageFormatterData)
        {
            var binaryMessageFormatterData = (BinarySerializeMessageFormatterData)messageFormatterData;
            MarshalRef.rawSerialize(managedData, binaryMessageFormatterData.serializedMessage, ref serializedSize);
            return binaryMessageFormatterData.serializedMessage;
        }

        internal override ParsingResult deSerialize(Session session)
        {
            try
            {
                // check message formatter data
                var binaryMessageFormatterData = session.DeserializeMessageFormatterData as BinaryDeserializeMessageFormatterData;
                if (binaryMessageFormatterData == null)
                    return ParsingResult_Error;

                var circularBuffer = session.CircularBuffer;
                if (BinaryMessageHeader.getHeaderSize() > circularBuffer.getReadableSize())
                    return ParsingResult_Incomplete;

                // check size
                short packetSize = _readMessageSize(circularBuffer, binaryMessageFormatterData);
                if (packetSize > circularBuffer.capacity())
                    return ParsingResult_Error;

                if (packetSize > circularBuffer.getReadableSize())
                    return ParsingResult_Incomplete;

                // read messageData
                byte[] serializedPacket = _readMessage(circularBuffer, packetSize, binaryMessageFormatterData);
                if (serializedPacket == null)
                    return ParsingResult_Error;

                circularBuffer.pop(packetSize);

                // deserialize message header
                BinaryMessageHeader packetHeader = new BinaryMessageHeader();
                if (_deSerializeMessageHeader(serializedPacket, ref packetHeader, binaryMessageFormatterData) == false)
                    return ParsingResult_Error;

                // deserialize message
                object message = null;
                onDispatch dispatcher = null;
                _deSerializeMessage(
                    session.DispatchHelper
                    , binaryMessageFormatterData
                    , serializedPacket
                    , ref packetHeader
                    , out message
                    , out dispatcher);
                if (message == null || dispatcher == null)
                    return ParsingResult_Error;

                ParsingResult parsingResult = new ParsingResult(
                    ParsingResult.ParsingResultEnum.PARSING_COMPLETE
                    , dispatcher
                    , message
                    , packetHeader.messageSize);
                return parsingResult;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Deserialize failed");
                return ParsingResult_Error;
            }
        }
    }
}