﻿using System;
using System.Runtime.InteropServices;

namespace MNF
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BinaryMessageHeader
    {
        public static BinaryMessageHeader emptyPacketHeader = new BinaryMessageHeader();

        [MarshalAs(UnmanagedType.I2)]
        public short messageSize;
        [MarshalAs(UnmanagedType.U2)]
        public ushort messageID;
        [MarshalAs(UnmanagedType.I4)]
        public int sequence;
        [MarshalAs(UnmanagedType.I4)]
        public int checkSum;

        public static int getHeaderSize()
        {
            return MarshalRef.getManagedDataSize(emptyPacketHeader);
        }

        public static Type getHeaderType()
        {
            return emptyPacketHeader.GetType();
        }
    }
}
