﻿using System;

namespace MNF
{
    public class UdpDeserializeMessageFormatterData : UdpDeserializeMessageFormatterData<UdpUnreliableHeader>
    {
    }

    public class UdpMessageFormatter : IMessageFormatter
    {
        internal override ISerializeMessageFormatterData allocSerializeMessageFormatterData()
        {
            return new UdpSerializeMessageFormatterData();
        }

        internal override IDeserializeMessageFormatterData allocDeserializeMessageFormatterData()
        {
            return new UdpDeserializeMessageFormatterData();
        }

        public short _readMessageSize(CircularBuffer circularBuffer, UdpDeserializeMessageFormatterData messageFormatterData)
        {
            try
            {
                if (circularBuffer.read(messageFormatterData.messageSize, messageFormatterData.messageSize.Length) == false)
                    throw new Exception("CircularBuffer read failed");

                return BitConverter.ToInt16(messageFormatterData.messageSize, 0);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Read packet size failed");
                return 0;
            }
        }

        public byte[] _readMessage(CircularBuffer circularBuffer, short packetSize, UdpDeserializeMessageFormatterData messageFormatterData)
        {
            if (messageFormatterData.deSerializedMessage.Length < packetSize)
                messageFormatterData.deSerializedMessage = new byte[packetSize];

            if (circularBuffer.read(messageFormatterData.deSerializedMessage, packetSize) == true)
                return messageFormatterData.deSerializedMessage;

            return null;
        }

        public bool _deSerializeMessageHeader(byte[] serializedPacket, ref UdpUnreliableHeader packetHeader, UdpDeserializeMessageFormatterData messageFormatterData)
        {
            try
            {
                packetHeader = (UdpUnreliableHeader)MarshalRef.rawDeSerialize(
                    serializedPacket
                    , UdpUnreliableHeader.getHeaderType()
                    , ref messageFormatterData.headerMarshalAllocatedBuffer
                    , ref messageFormatterData.headerMarshalAllocatedBufferSize);
                return true;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "UdpUnreliableHeader deSerialize Failed");
                return false;
            }
        }

        public void _deSerializeMessage(
            DispatchHelper dispatchExporter
            , UdpDeserializeMessageFormatterData messageFormatterData
            , byte[] serializedPacket
            , ref UdpUnreliableHeader messageHeader
            , out object message
            , out onDispatch dispatcher)
        {
            message = null;
            dispatcher = null;
            try
            {
                var dispatchInfo = dispatchExporter.tryGetMessageDispatch(messageHeader.udpDatagramHeader.messageID);
                message = MarshalRef.rawDeSerialize(
                    serializedPacket
                    , dispatchInfo.packetType
                    , ref messageFormatterData.marshalAllocatedBuffer
                    , ref messageFormatterData.marshalAllocatedBufferSize);
                if (message == null)
                    throw new Exception(string.Format("Dispatcher({0}), Expect packet size({1})",
                        dispatchInfo, MarshalRef.getManagedDataSize(dispatchInfo.packetType)));
                dispatcher = dispatchInfo.dispatcher;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Deserialize and dispatch failed",
                    messageHeader.udpDatagramHeader.messageID);
            }
        }

        internal override byte[] serialize(object managedData, ref int serializedSize, ISerializeMessageFormatterData messageFormatterData)
        {
            var udpMessageFormatterData = (UdpSerializeMessageFormatterData)messageFormatterData;
            MarshalRef.rawSerialize(managedData, udpMessageFormatterData.serializedMessage, ref serializedSize);
            return udpMessageFormatterData.serializedMessage;
        }

        internal override ParsingResult deSerialize(Session session)
        {
            try
            {
                // check message formatter data
                var messageFormatterData = session.DeserializeMessageFormatterData as UdpDeserializeMessageFormatterData;
                if (messageFormatterData == null)
                    return ParsingResult_Error;

                var circularBuffer = session.CircularBuffer;
                if (UdpUnreliableHeader.getHeaderSize() > circularBuffer.getReadableSize())
                    return ParsingResult_Incomplete;

                short messageSize = (short)circularBuffer.getReadableSize();

                // read messageData
                byte[] serializedPacket = _readMessage(circularBuffer, messageSize, messageFormatterData);
                if (serializedPacket == null)
                    return ParsingResult_Error;

                circularBuffer.pop(messageSize);

                // deserialize message header
                UdpUnreliableHeader messageHeader = new UdpUnreliableHeader();
                if (_deSerializeMessageHeader(serializedPacket, ref messageHeader, messageFormatterData) == false)
                    return ParsingResult_Error;

                // deserialize message
                object message = null;
                onDispatch dispatcher = null;
                _deSerializeMessage(
                    session.DispatchHelper
                    , messageFormatterData
                    , serializedPacket
                    , ref messageHeader
                    , out message
                    , out dispatcher);
                if (message == null || dispatcher == null)
                    return ParsingResult_Error;

                UdpMessageInfo udpMessageInfo = new UdpMessageInfo();
                udpMessageInfo.UdpMessage = message;
                udpMessageInfo.RemoteIP = null;

                ParsingResult parsingResult = new ParsingResult(
                    ParsingResult.ParsingResultEnum.PARSING_COMPLETE
                    , dispatcher
                    , udpMessageInfo
                    , messageSize);
                return parsingResult;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Deserialize failed");
                return ParsingResult_Error;
            }
        }
    }
}
