﻿using System;
using System.Collections.Generic;

namespace MNF
{
    public class MessageFormatterCollection : Singleton<MessageFormatterCollection>
    {
        private Dictionary<Type, IMessageFormatter> messages = null;

        public MessageFormatterCollection()
        {
            messages = new Dictionary<Type, IMessageFormatter>();
        }

        public bool add(Type messageFormatterType)
        {
            IMessageFormatter messageFormatter = null;
            if (messages.TryGetValue(messageFormatterType, out messageFormatter) == true)
                return true;

            messageFormatter = Utility.GetInstance(messageFormatterType) as IMessageFormatter;
            if (messageFormatter == null)
                return false;

            messages.Add(messageFormatterType, messageFormatter);
            return true;
        }

        public IMessageFormatter get(Type messageFormatterType)
        {
            IMessageFormatter messageFormatter = null;
            messages.TryGetValue(messageFormatterType, out messageFormatter);
            return messageFormatter;
        }
    }
}
