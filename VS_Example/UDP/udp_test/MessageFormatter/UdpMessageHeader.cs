﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace MNF
{
    enum ENUM_UDP_DATAGRAM_TYPE
    {
        UDP_DATAGRAM_TYPE_UNKNOWN       = 0,
        UDP_DATAGRAM_TYPE_UNRELIABLE    = 1,
        UDP_DATAGRAM_TYPE_RELIABLE      = 2,
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class UdpDatagramHeader
    {
        public static UdpDatagramHeader emptyMessageHeader = new UdpDatagramHeader();

        [MarshalAs(UnmanagedType.U1)]
        public byte datagramType;
        [MarshalAs(UnmanagedType.U2)]
        public ushort messageID;
        [MarshalAs(UnmanagedType.U4)]
        public uint checkSum;
        
        public UdpDatagramHeader()
        {
            datagramType = (byte)ENUM_UDP_DATAGRAM_TYPE.UDP_DATAGRAM_TYPE_UNKNOWN;
            messageID = 0;
            checkSum = 0;
        }

        public static int getHeaderSize()
        {
            return MarshalRef.getManagedDataSize(emptyMessageHeader);
        }

        public static Type getHeaderType()
        {
            return emptyMessageHeader.GetType();
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class UdpReliableHeader
    {
        public static UdpReliableHeader emptyMessageHeader = new UdpReliableHeader();

        public UdpDatagramHeader udpDatagramHeader;

        [MarshalAs(UnmanagedType.U4)]
        public uint sendWindowFrom;
        [MarshalAs(UnmanagedType.U4)]
        public uint sendWindowTo;
        [MarshalAs(UnmanagedType.U4)]
        public uint receiveWindow;

        public UdpReliableHeader()
        {
            udpDatagramHeader = new UdpDatagramHeader();
            udpDatagramHeader.datagramType = (byte)ENUM_UDP_DATAGRAM_TYPE.UDP_DATAGRAM_TYPE_RELIABLE;
        }

        public static int getHeaderSize()
        {
            return MarshalRef.getManagedDataSize(emptyMessageHeader);
        }

        public static Type getHeaderType()
        {
            return emptyMessageHeader.GetType();
        }

        public UdpDatagramHeader getUdpDatagramHeader()
        {
            return udpDatagramHeader;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class UdpUnreliableHeader
    {
        public static UdpUnreliableHeader emptyMessageHeader = new UdpUnreliableHeader();

        public UdpDatagramHeader udpDatagramHeader;

        [MarshalAs(UnmanagedType.U4)]
        public uint sequenceNumber; // for order

        public UdpUnreliableHeader()
        {
            udpDatagramHeader = new UdpDatagramHeader();
            udpDatagramHeader.datagramType = (byte)ENUM_UDP_DATAGRAM_TYPE.UDP_DATAGRAM_TYPE_UNRELIABLE;
        }

        public static int getHeaderSize()
        {
            return MarshalRef.getManagedDataSize(emptyMessageHeader);
        }

        public static Type getHeaderType()
        {
            return emptyMessageHeader.GetType();
        }

        public UdpDatagramHeader getUdpDatagramHeader()
        {
            return udpDatagramHeader;
        }
    }

    public class UdpSerializeMessageFormatterData : ISerializeMessageFormatterData
    {
        public byte[] serializedMessage;

        public UdpSerializeMessageFormatterData()
        {
            serializedMessage = new byte[1024];
        }

        ~UdpSerializeMessageFormatterData()
        {
        }
    }

    public class UdpDeserializeMessageFormatterData<T> : IDeserializeMessageFormatterData
    {
        public byte[] messageSize;
        public byte[] deSerializedMessage;
        public IntPtr headerMarshalAllocatedBuffer;
        public int headerMarshalAllocatedBufferSize;
        public IntPtr marshalAllocatedBuffer;
        public int marshalAllocatedBufferSize;

        public UdpDeserializeMessageFormatterData()
        {
            messageSize = new byte[2];
            deSerializedMessage = new byte[1024];
            headerMarshalAllocatedBufferSize = MarshalRef.getManagedDataSize(typeof(T));
            headerMarshalAllocatedBuffer = MarshalRef.allocGlobalHeap(headerMarshalAllocatedBufferSize);
            marshalAllocatedBufferSize = 1024;
            marshalAllocatedBuffer = MarshalRef.allocGlobalHeap(marshalAllocatedBufferSize);
        }

        ~UdpDeserializeMessageFormatterData()
        {
            MarshalRef.deAllocGlobalHeap(headerMarshalAllocatedBuffer);
            MarshalRef.deAllocGlobalHeap(marshalAllocatedBuffer);
        }
    }

    public class UdpMessageInfo
    {
        public object UdpMessage { get; set; }
        public EndPoint RemoteIP { get; set; }

        public UdpMessageInfo()
        {
            UdpMessage = null;
            RemoteIP = null;
        }
    }
}
