﻿using System;
namespace MNF
{
    public abstract class ISerializeMessageFormatterData
    {
    }

    public abstract class IDeserializeMessageFormatterData
    {
    }

    public abstract class IMessageFormatter
    {
        protected ParsingResult ParsingResult_Error;
        protected ParsingResult ParsingResult_Incomplete;

        public IMessageFormatter()
        {
            ParsingResult_Error = new ParsingResult(
                ParsingResult.ParsingResultEnum.PARSING_ERROR, null, null, 0);
            ParsingResult_Incomplete = new ParsingResult(
                ParsingResult.ParsingResultEnum.PARSING_INCOMPLETE, null, null, 0);
        }

        internal abstract byte[] serialize(object managedData, ref int serializedSize, ISerializeMessageFormatterData messageFormatterData);

        internal abstract ParsingResult deSerialize(Session session);

        internal abstract ISerializeMessageFormatterData allocSerializeMessageFormatterData();
        internal abstract IDeserializeMessageFormatterData allocDeserializeMessageFormatterData();
    }

    public class EmptyMessagFormatter : IMessageFormatter
    {
        internal override byte[] serialize(object managedData, ref int serializedSize, ISerializeMessageFormatterData messageFormatterData)
        {
            return null;
        }

        internal override ParsingResult deSerialize(Session session)
        {
            return new ParsingResult();
        }

        internal override ISerializeMessageFormatterData allocSerializeMessageFormatterData()
        {
            return null;
        }

        internal override IDeserializeMessageFormatterData allocDeserializeMessageFormatterData()
        {
            return null;
        }
    }
}
