﻿using System;

namespace MNF
{
    public interface IJsonMessageHeader
    {
        short getMessageID();
    }

    [System.Serializable]
    public struct JsonMessageHeader
    {
        public short messageSize;
        public short messageID;

        public JsonMessageHeader(short packetSize, short packetId)
        {
            this.messageSize = packetSize;
            this.messageID = packetId;
        }

        public byte[] convertMessageSizeToByte()
        {
            return BitConverter.GetBytes(messageSize);
        }

        public byte[] convertMessageIdToByte()
        {
            return BitConverter.GetBytes(messageID);
        }

        public byte[] convertToByte()
        {
            return BitConverter.GetBytes(convertToInt());
        }

        public int convertToInt()
        {
            int byteArray = 0;
            byteArray = messageID;
            byteArray <<= 16;
            byteArray |= (int)messageSize;
            return byteArray;
        }

        static public int getHeaderSize()
        {
            return getMessageSizeFieldSize() + getMessageIdFieldSize();
        }

        static public int getMessageSizeFieldSize()
        {
            return sizeof(short);
        }

        static public int getMessageIdFieldSize()
        {
            return sizeof(short);
        }
    }
}
