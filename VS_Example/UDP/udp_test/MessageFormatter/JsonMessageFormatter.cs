﻿using System;
using System.Text;
using JsonFx.Json;

namespace MNF
{
    public class JsonSerializeMessageFormatterData : ISerializeMessageFormatterData
    {
        public byte[] serializedMessage;

        public JsonSerializeMessageFormatterData()
        {
            serializedMessage = new byte[1024];
        }
    }

    public class JsonDeserializeMessageFormatterData : IDeserializeMessageFormatterData
    {
        public byte[] shortData;
        public byte[] deSerializedMessage;

        public JsonDeserializeMessageFormatterData()
        {
            shortData = new byte[2];
            deSerializedMessage = new byte[1024];
        }
    }

    public class JsonMessageFormatter : IMessageFormatter
    {
        public JsonMessageFormatter()
        {
        }

        internal override ISerializeMessageFormatterData allocSerializeMessageFormatterData()
        {
            return new JsonSerializeMessageFormatterData();
        }

        internal override IDeserializeMessageFormatterData allocDeserializeMessageFormatterData()
        {
            return new JsonDeserializeMessageFormatterData();
        }

        public short _convertToShort(CircularBuffer circularBuffer, int readSize, JsonDeserializeMessageFormatterData messageFormatterData)
        {
            try
            {
                if (circularBuffer.read(messageFormatterData.shortData, messageFormatterData.shortData.Length) == false)
                    throw new Exception("CircularBuffer read failed");

                return BitConverter.ToInt16(messageFormatterData.shortData, 0);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Read({0}) failed", readSize);
                return 0;
            }
        }

        public byte[] _readMessage(CircularBuffer circularBuffer, short packetSize, JsonDeserializeMessageFormatterData messageFormatterData)
        {
            short dataSize = (short)(packetSize - JsonMessageHeader.getHeaderSize());
            if (messageFormatterData.deSerializedMessage.Length < dataSize)
                messageFormatterData.deSerializedMessage = new byte[dataSize];

            if (circularBuffer.read(messageFormatterData.deSerializedMessage, dataSize) == false)
                return null;

            return messageFormatterData.deSerializedMessage;
        }

        public void _deSerializeMessage(short packetSize, CircularBuffer circularBuffer, JsonDeserializeMessageFormatterData jsonMessageFormatterData, out string jsonMessage, out short messageID)
        {
            // read packet id
            messageID = _convertToShort(circularBuffer, JsonMessageHeader.getMessageIdFieldSize(), jsonMessageFormatterData);
            circularBuffer.pop(JsonMessageHeader.getMessageIdFieldSize());

            // read packet
            byte[] serializedPacket = _readMessage(circularBuffer, packetSize, jsonMessageFormatterData);
            if (serializedPacket == null)
            {
                jsonMessage = null;
                messageID = 0;
                return;
            }

            circularBuffer.pop(packetSize - JsonMessageHeader.getHeaderSize());

            // byte -> string
            jsonMessage = Encoding.Default.GetString(serializedPacket);
        }

        public void _deSerializeMessage(
            DispatchHelper dispatchExporter
            , string jsonString
            , short messageID
            , out object message
            , out onDispatch dispatcher)
        {
            message = null;
            dispatcher = null;
            try
            {
                var dispatchInfo = dispatchExporter.tryGetMessageDispatch(messageID);
                if (dispatchInfo == null)
                    throw new Exception("Dispatcher found failed");

                message = JsonReader.Deserialize(jsonString, dispatchInfo.packetType);
                if (message == null)
                    throw new Exception(string.Format("dispatchInfo({0}) Json({1}) deserialize failed",
                        dispatchInfo, jsonString.Length));

                dispatcher = dispatchInfo.dispatcher;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Deserialize and dispatch failed, ID({0}) Length({1})",
                    messageID, jsonString.Length);
            }
        }

        internal override byte[] serialize(object managedData, ref int serializedSize, ISerializeMessageFormatterData messageFormatterData)
        {
            try
            {
                var jsonMessageFormatterData = (JsonSerializeMessageFormatterData)messageFormatterData;
                var jsonHeaderInterface = managedData as IJsonMessageHeader;
                if (jsonHeaderInterface == null)
                    return null;

                var jsonData = JsonWriter.Serialize(managedData);
                var convertedData = Encoding.UTF8.GetBytes(jsonData);
                serializedSize = JsonMessageHeader.getHeaderSize() + convertedData.Length;
                var jsonHeader = new JsonMessageHeader((short)serializedSize, jsonHeaderInterface.getMessageID());

                if (jsonMessageFormatterData.serializedMessage.Length < jsonHeader.messageSize)
                    jsonMessageFormatterData.serializedMessage = new byte[jsonHeader.messageSize];

                var convertedHeader = jsonHeader.convertToByte();
                Buffer.BlockCopy(convertedHeader, 0, jsonMessageFormatterData.serializedMessage, 0, convertedHeader.Length);
                Buffer.BlockCopy(convertedData, 0, jsonMessageFormatterData.serializedMessage, convertedHeader.Length, convertedData.Length);
                return jsonMessageFormatterData.serializedMessage;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Object({0}) Serialize failed", managedData);
                return null;
            }
        }

        internal override ParsingResult deSerialize(Session session)
        {
            try
            {
                // check message formatter data
                var jsonMessageFormatterData = session.DeserializeMessageFormatterData as JsonDeserializeMessageFormatterData;
                if (jsonMessageFormatterData == null)
                    return ParsingResult_Error;

                var circularBuffer = session.CircularBuffer;
                if (JsonMessageHeader.getHeaderSize() > circularBuffer.getReadableSize())
                    return ParsingResult_Incomplete;

                // read size
                short packetSize = _convertToShort(circularBuffer, JsonMessageHeader.getMessageSizeFieldSize(), jsonMessageFormatterData);
                if (packetSize > circularBuffer.capacity())
                    return ParsingResult_Error;

                if (packetSize > circularBuffer.getReadableSize())
                    return ParsingResult_Incomplete;

                circularBuffer.pop(JsonMessageHeader.getMessageSizeFieldSize());

                // deserialize packet
                string jsonMessage = null;
                short messageID = 0;
                _deSerializeMessage(packetSize, circularBuffer, jsonMessageFormatterData, out jsonMessage, out messageID);
                if (jsonMessage.Length == 0)
                    return ParsingResult_Error;

                object message = null;
                onDispatch dispatcher = null;
                _deSerializeMessage(session.DispatchHelper, jsonMessage, messageID, out message, out dispatcher);

                if (message == null || dispatcher == null)
                    return ParsingResult_Error;

                ParsingResult parsingResult = new ParsingResult(
                    ParsingResult.ParsingResultEnum.PARSING_COMPLETE
                    , dispatcher
                    , message
                    , packetSize);
                return parsingResult;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Deserialize failed");
                return ParsingResult_Error;
            }
        }
    }
}
