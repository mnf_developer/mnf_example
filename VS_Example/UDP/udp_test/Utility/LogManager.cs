﻿using System;

namespace MNF
{
    public enum ENUM_LOG_TYPE
    {
        LOG_TYPE_SYSTEM         = 1,
        LOG_TYPE_SYSTEM_DEBUG   = 2,
        LOG_TYPE_NORMAL         = 4,
        LOG_TYPE_NORMAL_DEBUG   = 8,
        LOG_TYPE_ERROR          = 16,
        LOG_TYPE_EXCEPTION      = 32,
    }

    public abstract class ILogWriter
    {
        public abstract bool OnInit();

        public abstract bool OnWrite(ENUM_LOG_TYPE logType, string logString);

        public abstract void OnRelease();
    }

    public class LogManager : Singleton<LogManager>
    {
        private ILogWriter logWriter_ = null;
        private int writeLogInfo_ = 0;

        public bool isWriteLog(ENUM_LOG_TYPE logType)
        {
            if ((writeLogInfo_ & (int)logType) == 0)
                return false;
            return true;
        }

        public void SetLogWriter(ILogWriter logWriter, int writeLogInfo)
        {
            logWriter_ = logWriter;
            writeLogInfo_ = writeLogInfo;
        }

        public bool init()
        {
            if (logWriter_ == null)
                return false;

            return logWriter_.OnInit();
        }

        public void release()
        {
            if (logWriter_ == null)
                return;

            logWriter_.OnRelease();
            logWriter_ = null;
        }

        public void Write(string formatString, params object[] values)
        {
            if (logWriter_ == null)
                return;

            if ((writeLogInfo_ & (int)ENUM_LOG_TYPE.LOG_TYPE_NORMAL) == 0)
                return;

            logWriter_.OnWrite(ENUM_LOG_TYPE.LOG_TYPE_NORMAL, string.Format(formatString, values));
        }

        public void WriteDebug(string formatString, params object[] values)
        {
            if (logWriter_ == null)
                return;

            if ((writeLogInfo_ & (int)ENUM_LOG_TYPE.LOG_TYPE_NORMAL_DEBUG) == 0)
                return;

            logWriter_.OnWrite(ENUM_LOG_TYPE.LOG_TYPE_NORMAL_DEBUG, string.Format(formatString, values));
        }

        public void WriteSystem(string formatString, params object[] values)
        {
            if (logWriter_ == null)
                return;

            if ((writeLogInfo_ & (int)ENUM_LOG_TYPE.LOG_TYPE_SYSTEM) == 0)
                return;

            logWriter_.OnWrite(ENUM_LOG_TYPE.LOG_TYPE_SYSTEM, string.Format(formatString, values));
        }

        public void WriteSystemDebug(string formatString, params object[] values)
        {
            if (logWriter_ == null)
                return;

            if ((writeLogInfo_ & (int)ENUM_LOG_TYPE.LOG_TYPE_SYSTEM_DEBUG) == 0)
                return;

            logWriter_.OnWrite(ENUM_LOG_TYPE.LOG_TYPE_SYSTEM_DEBUG, string.Format(formatString, values));
        }

        public void WriteError(string formatString, params object[] values)
        {
            if (logWriter_ == null)
                return;

            logWriter_.OnWrite(ENUM_LOG_TYPE.LOG_TYPE_ERROR, string.Format(formatString, values));
        }

        public void WriteException(Exception e, string formatString, params object[] values)
        {
            if (logWriter_ == null)
                return;

            logWriter_.OnWrite(ENUM_LOG_TYPE.LOG_TYPE_EXCEPTION, string.Format("{0}\n **[Exception]**{1}", 
                string.Format(formatString, values), e.ToString()));
        }
    }
}
