﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace MNF
{
    public static class Utility
    {
        // for thread
        public static int getCurrentThreadID()
        {
            return Thread.CurrentThread.ManagedThreadId;
        }

        // for network
        public static IPEndPoint getIPEndPoint(string ipString, string portString)
        {
            try
            {
                int portNum = Convert.ToInt32(portString);
                IPAddress ipAddress = IPAddress.Parse(ipString);
                return new IPEndPoint(ipAddress, portNum);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "IP({0}) Port({1}) Error", ipString, portString);
                return null;
            }
        }

        // for instance
        public static T GetInstance<T>(string type)
        {
            return (T)Activator.CreateInstance(Type.GetType(type));
        }

        public static object GetInstance(string type)
        {
            return Activator.CreateInstance(Type.GetType(type));
        }

        public static T GetInstance<T>(Type type)
        {
            return (T)Activator.CreateInstance(type);
        }

        public static object GetInstance(Type type)
        {
            return Activator.CreateInstance(type);
        }

        // for enum
        public static Dictionary<int, string> EnumDictionary<T>()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            foreach (var foo in Enum.GetValues(typeof(T)))
            {
                dict.Add((int)foo, foo.ToString());
            }
            return dict;
        }

        // for delegate
        public static Delegate loadDelegate(object targetObject, string methodName)
        {
            MethodInfo methodInfo = targetObject.GetType().GetMethod(methodName,
                BindingFlags.Public | BindingFlags.NonPublic |
                BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            Delegate loadedDelegate = Delegate.CreateDelegate(
                typeof(SystemMessageDispatcher.onSystemMessageDispatch), targetObject, methodInfo);
            return loadedDelegate;
        }

        // for datetime
        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }

        public static DateTime UnixTimeToDateTime(long unixtime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return sTime.AddSeconds(unixtime);
        }

        public static bool ExportServerInfo(string[] args, out string serverIP, out string serverPort)
        {
            serverIP = "";
            serverPort = "";

            // get server ip, port
            string hostNameORaddress = "";
            foreach (string arg in args)
            {
                char[] delimiterChars = { '=' };
                string[] words = arg.Split(delimiterChars);
                if (words.Length != 2)
                    continue;

                switch (words[0])
                {
                    case "dns":
                        hostNameORaddress = words[1];
                        break;
                    case "port":
                        serverPort = words[1];
                        break;
                }
            }
            IPHostEntry host = Dns.GetHostEntry(hostNameORaddress);
            if (host.AddressList.Length == 0)
                return false;

            // find vaild ip
            if (hostNameORaddress.Length == 0)
            {
                // get local ip
                foreach (IPAddress ipAddress in host.AddressList)
                {
                    if (ipAddress.ToString().StartsWith("192.168.") == true)
                    {
                        serverIP = ipAddress.ToString();
                        break;
                    }
                }
                if (serverIP.Length == 0)
                {
                    serverIP = host.AddressList[0].ToString();
                }
            }
            else
            {
                // get remote ip
                serverIP = host.AddressList[0].ToString();
            }

            return true;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }

    //static T GetByName<T>(object target, string methodName)
    //{
    //    MethodInfo method = target.GetType().GetMethod(methodName,
    //                   BindingFlags.Public
    //                   | BindingFlags.Instance
    //                   | BindingFlags.FlattenHierarchy);

    //    // Insert appropriate check for method == null here

    //    return (T)Delegate.CreateDelegate(typeof(T), target, method);
    //}

    //static T GetByName(object target, string methodName)
    //{
    //    return (T)Delegate.CreateDelegate
    //        (typeof(T), target, methodName);
    //}
    //public class TestLoadEnumsInClass
    //{
    //    public class TestingEnums
    //    {
    //        public enum Color { Red, Blue, Yellow, Pink }

    //        public enum Styles { Plaid = 0, Striped = 23, Tartan = 65, Corduroy = 78 }

    //        public string TestingProperty { get; set; }

    //        public string TestingMethod()
    //        {
    //            return null;
    //        }
    //    }

    //    public void btnTest_Click(object sender, EventArgs e)
    //    {
    //        var t = typeof(TestingEnums);
    //        var nestedTypes = t.GetMembers().Where(item => item.MemberType == MemberTypes.NestedType);
    //        foreach (var item in nestedTypes)
    //        {
    //            var type = Type.GetType(item.ToString());
    //            if (type == null || type.IsEnum == false)
    //                continue;

    //            string items = " ";
    //            foreach (MemberInfo x in type.GetMembers())
    //            {
    //                if (x.MemberType != MemberTypes.Field)
    //                    continue;

    //                if (x.Name.Equals("value__") == true)
    //                    continue;

    //                items = items + (" " + Enum.Parse(type, x.Name));
    //                items = items + (" " + (int)Enum.Parse(type, x.Name));
    //            }
    //            Console.WriteLine(items);
    //        }
    //    }
    //}

}
