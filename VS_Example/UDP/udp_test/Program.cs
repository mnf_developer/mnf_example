﻿using System;
using System.Collections.Generic;
using System.Net;
using MNF;

public class UdpTargetLinker
{
    public Dictionary<EndPoint, ulong> Sequences { get; private set; }

    public UdpTargetLinker()
    {
        Sequences = new Dictionary<EndPoint, ulong>();
    }
}

enum ENUM_UDP_TYPE
{
    UDP_TYPE_NONE,
    UDP_TYPE_UNRELIABLE,
    UDP_TYPE_RELIABLE,
}

enum ENUM_COMMUNITY_TYPE
{
    COMMUNITY_TYPE_NONE,
    COMMUNITY_TYPE_ECHO,
    COMMUNITY_TYPE_SEND,
}

class Program
{
    public static ENUM_UDP_TYPE udpType = ENUM_UDP_TYPE.UDP_TYPE_NONE;
    public static ENUM_COMMUNITY_TYPE communityType = ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_NONE;

    static ENUM_UDP_TYPE selectUdpType()
    {
        Console.WriteLine("Select ['U(u)'nreliable / 'R(r)'eliable] UDP [Reliable : Just Enter]");
        string format = Console.ReadLine();
        udpType = ENUM_UDP_TYPE.UDP_TYPE_RELIABLE;
        switch (format)
        {
            case "U":
            case "u":
                udpType = ENUM_UDP_TYPE.UDP_TYPE_UNRELIABLE;
                break;

            case "R":
            case "r":
                udpType = ENUM_UDP_TYPE.UDP_TYPE_RELIABLE;
                break;

            default:
                break;
        }
        return udpType;
    }

    static ENUM_COMMUNITY_TYPE selectCommunityType()
    {
        Console.WriteLine("Select ['E(e)'cho / 'S(s)'end] UDP [Send : Just Enter]");
        string format = Console.ReadLine();
        communityType = ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_SEND;
        switch (format)
        {
            case "E":
            case "e":
                communityType = ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_ECHO;
                break;

            case "S":
            case "s":
                communityType = ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_SEND;
                break;

            default:
                break;
        }
        return communityType;
    }

    static void Main(string[] args)
    {
        if (selectUdpType() == ENUM_UDP_TYPE.UDP_TYPE_NONE)
            return;

        if (selectCommunityType() == ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_NONE)
            return;

        Console.WriteLine("Input My Port : [10000 : Just Enter]");
        string myPort = "10000";
        string tmpMyPort = Console.ReadLine();
        if (tmpMyPort != "")
            myPort = tmpMyPort;

        string targetIP = Utility.getLocalAddress().ToString();
        Console.WriteLine("Input Target IP : [{0}:Just Enter]", targetIP);
        string tmpServerIP = Console.ReadLine();
        if (tmpServerIP != "")
            targetIP = tmpServerIP;

        Console.WriteLine("Input Target Port : [20000 : Just Enter]");
        string targetPort = "20000";
        string tmpTargetPort = Console.ReadLine();
        if (tmpTargetPort != "")
            targetPort = tmpTargetPort;

        LookAround.Instance.Start(myPort, false);
        var targetEndPoint = Utility.GetIPEndPoint(targetIP, targetPort);

        int logType = (int)ENUM_LOG_TYPE.LOG_TYPE_NORMAL;
        logType |= (int)ENUM_LOG_TYPE.LOG_TYPE_DEFAULT;
        logType |= (int)ENUM_LOG_TYPE.LOG_TYPE_SYSTEM;
        LogManager.Instance.SetLogWriter(new CLogWriter(), logType);
        LogManager.Instance.Init();

        var udpSession1 = UdpHelper.Instance.Create(typeof(MessageDispatcher), "", myPort);
        udpSession1.TargetLink = new UdpTargetLinker();

        UdpControllHelper.Instance.SetDropRate(sendDropRate: 0, recvDropRate: 0, ackDropRate: 0);
        UdpControllHelper.Instance.SetSendLoopCount(1);
        UdpHelper.Instance.Run(isRunThread: true);

        if (udpType == ENUM_UDP_TYPE.UDP_TYPE_UNRELIABLE)
        {
            sendUnreliableMessage(targetEndPoint, udpSession1);
        }
        else
        {
            sendReliableMessage(targetEndPoint, udpSession1);
        }
    }

    private static void sendReliableMessage(IPEndPoint targetEndPoint, UdpSession udpSession1)
    {
        DateTime lastSendTime = DateTime.Now;
        if (communityType == ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_ECHO)
        {
            var rTest = new UdpMessageDefine.PACK_UDP_RELIABLE_TEST();
            udpSession1.reliableSendTo(targetEndPoint, ref rTest);
        }
        else
        {
            ulong sequenceField = 0;
            while (true)
            {
                var rTest = new UdpMessageDefine.PACK_UDP_RELIABLE_TEST();
                rTest.sequenceField_ = sequenceField;
                bool sendResult = udpSession1.reliableSendTo(targetEndPoint, ref rTest);
                if (sendResult == true)
                    ++sequenceField;

                System.Threading.Thread.Sleep(0);

                if (sendResult == false)
                {
                    System.Threading.Thread.Sleep(10);

                    if (lastSendTime.Second != DateTime.Now.Second)
                    {
                        LogManager.Instance.Write("time({0}) [SEND] stop send, seq({1})", DateTime.Now, rTest.sequenceField_);
                        lastSendTime = DateTime.Now;
                    }
                }
            }
        }
    }

    private static void sendUnreliableMessage(IPEndPoint targetEndPoint, UdpSession udpSession1)
    {
        var uTest = new UdpMessageDefine.PACK_UDP_UNRELIABLE_TEST();
        while (true)
        {
            udpSession1.unreliableSendTo(targetEndPoint, ref uTest);
            ++uTest.sequence;
        }
    }
}
