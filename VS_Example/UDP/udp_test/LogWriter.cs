﻿using System;
using System.IO;
using System.Diagnostics;
using MNF;

public class CLogWriter : ILogWriter
{
    StreamWriter file = null;
    object lockObject = null;

    public override bool OnInit()
    {
        lockObject = new object();
        file = new StreamWriter(string.Format("MNFLog_{0}.log", Process.GetCurrentProcess().Id));
        return true;
    }

    public override void OnRelease()
    {
        file.Close();
    }

    public override bool OnWrite(ENUM_LOG_TYPE logType, string logString)
    {
        lock (lockObject)
        {
            file.WriteLine(logString);
            Console.WriteLine(logString);
        }
        return true;
    }
}