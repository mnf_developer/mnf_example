﻿using System;
using System.Diagnostics;
using MNF;

public class MessageDispatcher : DefaultDispatchHelper<UdpSession, UdpMessageDefine, UdpMessageDefine.ENUM_UDP_TEST_>
{
    DateTime lastRecvTime = DateTime.Now;
    int prossedCount = 0;

    int onUDP_UNRELIABLE_CHAT_MESSAGE(UdpSession session, object message)
    {
        var udpMessageInfo = (UdpMessageInfo)message;
        var unreliableChatMessage = (UdpMessageDefine.PACK_UDP_UNRELIABLE_CHAT_MESSAGE)udpMessageInfo.UdpMessage;
        LogManager.Instance.Write("{0} onUDP_UNRELIABLE_CHAT_MESSAGE : {1}", udpMessageInfo.RemoteIP, unreliableChatMessage.stringField);

        return 0;
    }

    int onUDP_RELIABLE_CHAT_MESSAGE(UdpSession session, object message)
    {
        var udpMessageInfo = (UdpMessageInfo)message;
        var reliableChatMessage = (UdpMessageDefine.PACK_UDP_RELIABLE_CHAT_MESSAGE)udpMessageInfo.UdpMessage;
        LogManager.Instance.Write("{0} onUDP_RELIABLE_CHAT_MESSAGE : {1}", udpMessageInfo.RemoteIP, reliableChatMessage.stringField);

        return 0;
    }

    int onUDP_UNRELIABLE_TEST(UdpSession session, object message)
    {
        var udpMessageInfo = (UdpMessageInfo)message;
        var unreliableTestMessage = (UdpMessageDefine.PACK_UDP_UNRELIABLE_TEST)udpMessageInfo.UdpMessage;

        unreliableTestMessage.compare(new UdpMessageDefine.PACK_UDP_UNRELIABLE_TEST());

        //session.unreliableSendTo(udpMessageInfo.RemoteIP, ref unreliableTestMessage);

        ++prossedCount;

        if (lastRecvTime.Second != DateTime.Now.Second)
        {
            LogManager.Instance.Write("time({0}) [RECV] count({1}) seq({2}) - onUDP_UNRELIABLE_TEST", DateTime.Now, prossedCount, unreliableTestMessage.sequence);

            lastRecvTime = DateTime.Now;
            prossedCount = 0;
        }

        return 0;
    }

    int onUDP_RELIABLE_TEST(UdpSession session, object message)
    {
        var udpMessageInfo = (UdpMessageInfo)message;
        var reliableTestMessage = (UdpMessageDefine.PACK_UDP_RELIABLE_TEST)udpMessageInfo.UdpMessage;
        var udpTargetLinker = (UdpTargetLinker)session.TargetLink;

        if (udpTargetLinker.Sequences.ContainsKey(udpMessageInfo.RemoteIP) == false)
            udpTargetLinker.Sequences.Add(udpMessageInfo.RemoteIP, 0);

        reliableTestMessage.compare(new UdpMessageDefine.PACK_UDP_RELIABLE_TEST());
        Debug.Assert(udpTargetLinker.Sequences[udpMessageInfo.RemoteIP] == reliableTestMessage.sequenceField_);

        ++prossedCount;
        ++udpTargetLinker.Sequences[udpMessageInfo.RemoteIP];

        if (lastRecvTime.Second != DateTime.Now.Second)
        {
            LogManager.Instance.Write("time({0}) [RECV] count({1}) seq({2})", DateTime.Now, prossedCount, udpTargetLinker.Sequences[udpMessageInfo.RemoteIP]);

            lastRecvTime = DateTime.Now;
            prossedCount = 0;
        }

        if (Program.communityType == ENUM_COMMUNITY_TYPE.COMMUNITY_TYPE_ECHO)
        {
            ++reliableTestMessage.sequenceField_;
            session.reliableSendTo(udpMessageInfo.RemoteIP, ref reliableTestMessage);
        }

        return 0;
    }
}