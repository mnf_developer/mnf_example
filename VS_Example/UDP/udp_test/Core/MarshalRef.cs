﻿using System;
using System.Runtime.InteropServices;

namespace MNF
{
    public class MarshalRef
    {
        public static IntPtr allocGlobalHeap(int size)
        {
            return Marshal.AllocHGlobal(size);
        }

        public static void deAllocGlobalHeap(IntPtr buffer)
        {
            Marshal.FreeHGlobal(buffer);
        }

        public static byte[] rawSerialize(object managedData)
        {
            byte[] RawData = null;
            try
            {
                int objectSize = getManagedDataSize(managedData);
                RawData = new byte[objectSize];
                IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(RawData, 0);
                Marshal.StructureToPtr(managedData, buffer, false);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Object({0}) Marshal serialize failed", managedData.GetType().Name);
            }
            return RawData;
        }

        public static void rawSerialize(object managedData, byte[] rawData, ref int serializedSize)
        {
            try
            {
                serializedSize = getManagedDataSize(managedData);
                if (rawData.Length < serializedSize)
                    rawData = new byte[serializedSize];

                IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(rawData, 0);
                Marshal.StructureToPtr(managedData, buffer, false);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Object({0}) Marshal serialize failed", managedData.GetType().Name);
            }
        }

        public static object rawDeSerialize(byte[] RawData, Type managedData)
        {
            int RawSize = getManagedDataSize(managedData);

            //Size check
            if (RawSize > RawData.Length)
                return null;

            IntPtr buffer = Marshal.AllocHGlobal(RawSize);
            Marshal.Copy(RawData, 0, buffer, RawSize);
            object retobj = Marshal.PtrToStructure(buffer, managedData);
            Marshal.FreeHGlobal(buffer);
            return retobj;
        }

        public static object rawDeSerialize(byte[] RawData, Type managedData, ref IntPtr allocatedBuffer, ref int allocatedBufferSize)
        {
            int RawSize = getManagedDataSize(managedData);

            //Size check
            if (RawSize > RawData.Length)
                return null;

            if (allocatedBufferSize < RawSize)
            {
                if (allocatedBufferSize > 0)
                    Marshal.FreeHGlobal(allocatedBuffer);

                allocatedBuffer = Marshal.AllocHGlobal(RawSize);
                allocatedBufferSize = RawSize;
            }

            Marshal.Copy(RawData, 0, allocatedBuffer, RawSize);
            object retobj = Marshal.PtrToStructure(allocatedBuffer, managedData);
            return retobj;
        }

        public static int getManagedDataSize(object managedData)
        {
            return Marshal.SizeOf(managedData);
        }

        public static int getManagedDataSize(Type managedData)
        {
            return Marshal.SizeOf(managedData);
        }
    }
}
