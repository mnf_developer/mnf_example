﻿namespace MNF
{
    public struct NF_Message
    {
        [System.ComponentModel.DefaultValue(null)]
        public Session Session { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public onDispatch Dispatcher { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public object MessageData { get; set; }

        public NF_Message(Session session, onDispatch dispatcher, object messageData)
        {
            Session = session;
            Dispatcher = dispatcher;
            MessageData = messageData;
        }

        public int execute()
        {
            return Dispatcher(Session, MessageData);
        }

        public override string ToString()
        {
            if (Session == null)
                return string.Format("session(null):{0}", Dispatcher.ToString());

            if (Dispatcher == null)
                return Session.ToString();

            if (MessageData == null)
                return string.Format("{0}:{1}", Session.ToString(), Dispatcher.ToString());

            return string.Format("{0}:{1}:{2}", Session.ToString(), Dispatcher.ToString(), MessageData.ToString());
        }
    }
}
