﻿using System;
using System.Threading;
using System.Diagnostics;

namespace MNF
{
    public class ThreadAdapter : IDisposable
    {
        public delegate void ThreadEventHandler();
        public event ThreadEventHandler ThreadEvent;

        private AutoResetEvent messageEvent = null;
        private Thread messageDispatchThread = null;
        private bool isStop = false;
        private int waitTime = 100;

        public int WaitTime
        {
            set { waitTime = value; messageEvent.Set(); }
        }

        public ThreadAdapter(AutoResetEvent messageEvent)
        {
            this.messageEvent = messageEvent;
        }

        public bool start()
        {
            Debug.Assert(messageDispatchThread == null);
            Debug.Assert(isStop == false);

            if (messageDispatchThread != null)
                return false;

            if (messageEvent == null)
                messageEvent = new AutoResetEvent(false);

            messageDispatchThread = new Thread(runThread);
            isStop = false;
            messageDispatchThread.Start();

            return true;
        }

        public void stop()
        {
            isStop = true;
            messageEvent.Set();
            messageDispatchThread.Join();
        }

        public bool isRunning()
        {
            if (messageDispatchThread == null)
                return false;
            return messageDispatchThread.IsAlive;
        }

        private void runThread()
        {
            while (isStop == false)
            {
                messageEvent.WaitOne(waitTime);
                ThreadEvent();
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    messageEvent.Close();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ThreadAdapter() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
