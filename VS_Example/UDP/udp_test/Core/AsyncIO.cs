﻿using System;
using System.Net;
using System.Net.Sockets;

namespace MNF
{
    public struct ParsingResult
    {
        public enum ParsingResultEnum
        {
            PARSING_NONE,
            PARSING_INCOMPLETE,
            PARSING_COMPLETE,
            PARSING_ERROR,
        };

        public ParsingResultEnum parsingResultEnum;
        public onDispatch dispatcher;
        public object message;
        public int messageSize;

        public ParsingResult(ParsingResultEnum parsingResultEnum, onDispatch dispatcher, object message, int messageSize)
        {
            this.parsingResultEnum = parsingResultEnum;
            this.dispatcher = dispatcher;
            this.message = message;
            this.messageSize = messageSize;
        }
    }

    internal class AsyncIOResult
    {
        public Session session;
        public ISerializeMessageFormatterData serializeMessageFormatterData;
    }

    // Singleton class
    internal class AsyncIO
    {
        private static AsyncCallback delegateUnreliableRecvCallBack = new AsyncCallback(recvFromCallback);
        private static AsyncCallback delegateUnreliableSendCallBack = new AsyncCallback(sendToCallback);

        internal static bool sendTo(Session session, EndPoint endPoint, object managedData)
        {
            try
            {
                ISerializeMessageFormatterData messageFormatterData = null;
                int serializedSize = 0;
                byte[] serializedBuffer = null;

                lock (session.LockSend)
                {
                    if (session.FreeSerializeMessageFormatterDatas.Count > 0)
                    {
                        messageFormatterData = session.FreeSerializeMessageFormatterDatas.First.Value;
                        session.FreeSerializeMessageFormatterDatas.RemoveFirst();
                    }
                    else
                    {
                        messageFormatterData = session.MessageFormattor.allocSerializeMessageFormatterData();
                    }
                    session.SerializeMessageFormatterData = messageFormatterData;
                    session.AllocatedSerializeMessageFormatterDatas.AddLast(messageFormatterData);

                    serializedBuffer = session.serializePacket(managedData, ref serializedSize);
                    if (serializedBuffer == null)
                        throw new Exception("Send data serialize failed");
                }

                var asyncIOResult = new AsyncIOResult();
                asyncIOResult.session = session;
                asyncIOResult.serializeMessageFormatterData = messageFormatterData;
                session.Socket.BeginSendTo(serializedBuffer, 0, serializedSize, 0, endPoint, delegateUnreliableSendCallBack, asyncIOResult);
                return true;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Session({0}) packet({1}) unreliable send to ({2}) begin failed",
                    session.ToString(), managedData.ToString(), endPoint.ToString());
                return false;
            }
        }
        private static void sendToCallback(IAsyncResult asyncResult)
        {
            var asyncIOResult = asyncResult.AsyncState as AsyncIOResult;
            var session = asyncIOResult.session;
            try
            {
                if (session == null)
                    throw new Exception(string.Format("Send session({0}) is invalid", asyncResult.GetType().Name));

                lock (session.LockSend)
                {
                    session.AllocatedSerializeMessageFormatterDatas.Remove(asyncIOResult.serializeMessageFormatterData);
                    session.FreeSerializeMessageFormatterDatas.AddLast(asyncIOResult.serializeMessageFormatterData);
                }

                // Complete sending the readData to the remote device.
                int bytesSent = session.Socket.EndSendTo(asyncResult);

                if (LogManager.Instance.isWriteLog(ENUM_LOG_TYPE.LOG_TYPE_SYSTEM_DEBUG) == true)
                {
                    LogManager.Instance.WriteSystemDebug("Session({0}) sent({1}) bytes to server",
                        session.EndPoint.ToString(), bytesSent);
                }

                if (bytesSent == 0)
                {
                    LogManager.Instance.WriteError("Session({0}) send Zero byte",
                        session.EndPoint.ToString(), bytesSent);
                }
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Session({0}) send callback proc failed", asyncResult.GetType().Name);
            }
        }

        internal static bool recvFrom(Session session)
        {
            try
            {
                EndPoint newClientEP = new IPEndPoint(IPAddress.Any, 0);
                session.Socket.BeginReceiveFrom(session.AsyncRecvBuffer, 0, session.AsyncRecvBuffer.Length, 0, ref newClientEP, delegateUnreliableRecvCallBack, session);
                session.IsAsyncRecving = true;
                return true;
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Session({0}) receive failed", session);
                return false;
            }
        }
        private static void recvFromCallback(IAsyncResult asyncResult)
        {
            Session session = asyncResult.AsyncState as Session;
            try
            {
                if (session == null)
                    throw new Exception(string.Format("Recv session({0}) is invalid", asyncResult.GetType().Name));

                // Read readData from the remote device.
                EndPoint senderEndPoint = new IPEndPoint(IPAddress.Any, 0);
                int bytesRead = session.Socket.EndReceiveFrom(asyncResult, ref senderEndPoint);

                if (LogManager.Instance.isWriteLog(ENUM_LOG_TYPE.LOG_TYPE_SYSTEM_DEBUG) == true)
                {
                    LogManager.Instance.WriteSystemDebug("Session({0}) Recv({1}) bytes from server.",
                        session.EndPoint.ToString(), bytesRead);
                }

                // copy async buffer to circular buffer
                if (session.doCopyReceivedData(bytesRead) == false)
                    throw new Exception(string.Format("Session({0} copy received data({1}) failed", session, bytesRead));

                ParsingResult parsingResult = session.deserializePacket();
                var udpMessageInfo = (UdpMessageInfo)parsingResult.message;
                udpMessageInfo.RemoteIP = senderEndPoint;
                NF_Message nfMessage = new NF_Message(session, parsingResult.dispatcher, parsingResult.message);
                DispatcherCollection.Instance.pushMessage(DISPATCH_TYPE.DISPATCH_NETWORK_INTERNAL, ref nfMessage);

                // async recv again
                session.IsAsyncRecving = false;
                recvFrom(session);
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "Session({0}) recv callback proc failed", asyncResult.GetType().Name);
            }
        }
    }
}
