﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;

namespace MNF
{
    public class Session
    {
        // for messageData async receive
        private const int asyncRecvBufferSize = 4096;
        private byte[] asyncRecvBuffer = new byte[asyncRecvBufferSize];
        private int totalRecvSize = 0;

        // for messageData parsing
        public int TotalParingSize { get; set; }
        public CircularBuffer CircularBuffer { get; private set; }

        // for serialize/deserialize
        public object LockSend { get; private set; }
        public LinkedList<ISerializeMessageFormatterData> FreeSerializeMessageFormatterDatas { get; private set; }
        public LinkedList<ISerializeMessageFormatterData> AllocatedSerializeMessageFormatterDatas { get; private set; }
        public DispatchHelper DispatchHelper { get; set; }
        public ISerializeMessageFormatterData SerializeMessageFormatterData { get; set; }
        public IDeserializeMessageFormatterData DeserializeMessageFormatterData { get; set; }
        public IMessageFormatter MessageFormattor { get; set; }

        // for log
        private string logString = "";
        public int SendPacketSequence { get; set; }
        public int RecvPacketSequence { get; set; }
        public bool IsAsyncRecving { get; set; }

        // for network
        private Socket socket = null;
        private IPEndPoint endPoint = null;
        private bool isConnected = false;
        public bool IsConnected
        {
            get { return (this.isConnected && this.Socket.Connected); }
            set { this.isConnected = value; }
        }
        public bool IsNotifyDisconnect { get; set; }
        public byte[] AsyncRecvBuffer { get { return asyncRecvBuffer; } }
        public IPEndPoint EndPoint { get { return endPoint; } set { endPoint = value; } }
        public Socket Socket { get { return socket; } set { socket = value; } }

        public Session()
        {
            CircularBuffer = new CircularBuffer(asyncRecvBufferSize * 5);
            LockSend = new object();
            FreeSerializeMessageFormatterDatas = new LinkedList<ISerializeMessageFormatterData>();
            AllocatedSerializeMessageFormatterDatas = new LinkedList<ISerializeMessageFormatterData>();
            IsNotifyDisconnect = false;
        }

        public void setNetworkReady(Socket clientSocket, DispatchHelper dispatchExporter, IMessageFormatter messageFormatter)
        {
            Socket = clientSocket;
            DispatchHelper = dispatchExporter;
            MessageFormattor = messageFormatter;
            DeserializeMessageFormatterData = messageFormatter.allocDeserializeMessageFormatterData();
            FreeSerializeMessageFormatterDatas.AddLast(messageFormatter.allocSerializeMessageFormatterData());
        }

        public void disconnect()
        {
            if (Socket == null)
                return;

            if (IsConnected == true)
            {
                IsConnected = false;
                Socket.Shutdown(SocketShutdown.Both);
            }
            Socket.Close();
        }

        internal bool doCopyReceivedData(int receivedBufferSize)
        {
            // copy to circular buffer
            totalRecvSize += receivedBufferSize;
            return CircularBuffer.push(asyncRecvBuffer, receivedBufferSize);
        }

        public ParsingResult deserializePacket()
        {
            ParsingResult parsingResult = MessageFormattor.deSerialize(this);
            TotalParingSize += parsingResult.messageSize;
            return parsingResult;
        }

        public byte[] serializePacket(object managedData, ref int serializedSize)
        {
            return MessageFormattor.serialize(managedData, ref serializedSize, SerializeMessageFormatterData);
        }

        public bool sendTo<T>(EndPoint ipEndPoint, ref T message)
        {
            return AsyncIO.sendTo(this, ipEndPoint, message);
        }

        public override string ToString()
        {
            if (logString.Length == 0)
                logString = RuntimeHelpers.GetHashCode(this).ToString();
            return logString;
        }
    }
}
