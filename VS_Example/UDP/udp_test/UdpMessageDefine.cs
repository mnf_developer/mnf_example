﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using MNF;

public partial class UdpMessageDefine
{
    public enum ENUM_UDP_TEST_
    {
        UDP_UNRELIABLE_CHAT_MESSAGE,
        UDP_RELIABLE_CHAT_MESSAGE,
        UDP_UNRELIABLE_TEST,
        UDP_RELIABLE_TEST,
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public class PACK_UDP_UNRELIABLE_CHAT_MESSAGE : UdpMessageHeader
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string stringField;

        [MarshalAs(UnmanagedType.Bool)]
        public bool boolField_;

        [MarshalAs(UnmanagedType.I4)]
        public int intField_;

        [MarshalAs(UnmanagedType.I8)]
        public Int64 int64Field_;

        public PACK_UDP_UNRELIABLE_CHAT_MESSAGE()
        {
            messageSize = (short)MarshalHelper.GetManagedDataSize(this);
            messageID = (ushort)ENUM_UDP_TEST_.UDP_UNRELIABLE_CHAT_MESSAGE;
            stringField = "";
            boolField_ = true;
            intField_ = 3333;
            int64Field_ = 444444;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public class PACK_UDP_RELIABLE_CHAT_MESSAGE : UdpMessageHeader
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string stringField;

        [MarshalAs(UnmanagedType.Bool)]
        public bool boolField_;

        [MarshalAs(UnmanagedType.I4)]
        public int intField_;

        [MarshalAs(UnmanagedType.I8)]
        public Int64 int64Field_;

        public PACK_UDP_RELIABLE_CHAT_MESSAGE()
        {
            messageSize = (short)MarshalHelper.GetManagedDataSize(this);
            messageID = (ushort)ENUM_UDP_TEST_.UDP_RELIABLE_CHAT_MESSAGE;
            stringField = "";
            boolField_ = true;
            intField_ = 3333;
            int64Field_ = 444444;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public class PACK_UDP_UNRELIABLE_TEST : UdpMessageHeader
    {
        [MarshalAs(UnmanagedType.Bool)]
        public bool boolField_;

        [MarshalAs(UnmanagedType.I4)]
        public int intField_;

        [MarshalAs(UnmanagedType.I8)]
        public Int64 int64Field_;

        [MarshalAs(UnmanagedType.I4)]
        public int sequence;

        public PACK_UDP_UNRELIABLE_TEST()
        {
            messageSize = (short)MarshalHelper.GetManagedDataSize(this);
            messageID = (ushort)ENUM_UDP_TEST_.UDP_UNRELIABLE_TEST;
            boolField_ = true;
            intField_ = 3333;
            int64Field_ = 444444;
            sequence = 0;
        }

        public void compare(PACK_UDP_UNRELIABLE_TEST target)
        {
            Debug.Assert(boolField_ == target.boolField_);
            Debug.Assert(intField_ == target.intField_);
            Debug.Assert(int64Field_ == target.int64Field_);
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public class PACK_UDP_RELIABLE_TEST : UdpMessageHeader
    {
        [MarshalAs(UnmanagedType.Bool)]
        public bool boolField_;

        [MarshalAs(UnmanagedType.I4)]
        public int intField_;

        [MarshalAs(UnmanagedType.I8)]
        public Int64 int64Field_;

        [MarshalAs(UnmanagedType.U8)]
        public UInt64 sequenceField_;

        public PACK_UDP_RELIABLE_TEST()
        {
            messageSize = (short)MarshalHelper.GetManagedDataSize(this);
            messageID = (ushort)ENUM_UDP_TEST_.UDP_RELIABLE_TEST;
            boolField_ = true;
            intField_ = 3333;
            int64Field_ = 444444;
            sequenceField_ = 0;
        }

        public void compare(PACK_UDP_RELIABLE_TEST target)
        {
            Debug.Assert(boolField_ == target.boolField_);
            Debug.Assert(intField_ == target.intField_);
            Debug.Assert(int64Field_ == target.int64Field_);
        }
    }
}