﻿using System;
using System.Collections.Generic;
using System.Net;
using MNF;

namespace udp_test
{
    class Program
    {
        static Program staticProgram = new Program();

        ~Program()
        {
            LookAround.Instance.Stop();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Input My Port : ");
            LookAround.Instance.Start(Console.ReadLine(), false);
            for (int i = 0; i < 10; ++i)
            {
                System.Threading.Thread.Sleep(1000);
                List<ResponseEndPointInfo> responseEndPoints = LookAround.Instance.GetResponseEndPoint();
                foreach (var endPoint in responseEndPoints)
                {
                    Console.WriteLine("Response IPEndPoint : {0}", endPoint);
                }
            }
            Console.WriteLine("My IP:{0} Port:{1}", LookAround.Instance.MyIP, LookAround.Instance.MyPort);
            LookAround.Instance.Stop();
        }
    }
}
