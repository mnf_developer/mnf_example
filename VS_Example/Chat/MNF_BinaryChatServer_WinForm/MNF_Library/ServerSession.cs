﻿using System.Collections.Generic;
using MNF;

namespace MNF_Server_WinForm.MNF_Library
{
    public class ClientSession : BinarySession
    {
        static private List<ClientSession> clientSessionList = new List<ClientSession>();
        static public List<ClientSession> ClientSessionList { get { return clientSessionList; } }

        private Form_Main formMain = null;

        public override int OnAccept()
        {
            LogManager.Instance.Write("onAccept : {0}:{1}", this.ToString(), this.GetType());

            formMain = FormsUtility.GetFormMain();

            var listBoxAccept_Disconnect = formMain.getListBoxMNFLog();
            listBoxAccept_Disconnect.InvokeIfNeeded(
                formMain.writeListBoxMNFLog,
                string.Format("Session accepted : {0}", this));

            var listBoxAcceptedSessions = formMain.getListBoxAcceptedSessions();
            listBoxAcceptedSessions.InvokeIfNeeded(
                formMain.addListBoxAcceptedSessions, this);

            ClientSessionList.Add(this);

            return 0;
        }

        public override int OnDisconnect()
        {
            LogManager.Instance.Write("onDisconnect : {0}:{1}", this.ToString(), this.GetType());

            var listBoxAccept_Disconnect = formMain.getListBoxMNFLog();
            listBoxAccept_Disconnect.InvokeIfNeeded(
                formMain.writeListBoxMNFLog,
                string.Format("Session disconnected : {0}", this));

            var listBoxAcceptedSessions = formMain.getListBoxAcceptedSessions();
            listBoxAcceptedSessions.InvokeIfNeeded(
                formMain.removeListBoxAcceptedSessions, this);

            ClientSessionList.Remove(this);

            return 0;
        }
    }
}