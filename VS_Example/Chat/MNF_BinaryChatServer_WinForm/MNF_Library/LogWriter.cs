﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MNF;

namespace MNF_Server_WinForm.MNF_Library
{
    public class FileLogWriter : ILogWriter
    {
        private BinaryFormatter binaryFormatter = new BinaryFormatter();
        private FileStream file = null;
        private Form_Main formMain = null;

        public override bool OnInit()
        {
            formMain = FormsUtility.GetFormMain();
            file = File.Create("MNFLog.log");
            return true;
        }

        public override void OnRelease()
        {
            binaryFormatter = null;
            formMain = null;
            file.Close();
        }

        public override bool OnWrite(ENUM_LOG_TYPE logType, string logString)
        {
            binaryFormatter.Serialize(file, logString);
            Console.WriteLine(logString);

            var listBoxMNFLog = formMain.getListBoxMNFLog();
            listBoxMNFLog.InvokeIfNeeded(
                formMain.writeListBoxMNFLog,
                string.Format("{0} : {1}\n", logType.ToString(), logString));

            return true;
        }
    }
}
