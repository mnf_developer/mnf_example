﻿using System;
using MNF_Common;
using MNF;

namespace MNF_Server_WinForm.MNF_Library
{
    public class BinaryMessageDispatcher : DefaultDispatchHelper<ClientSession, BinaryChatMessageDefine, BinaryChatMessageDefine.ENUM_CS_>
    {
        private Form_Main formMain = null;

        private int onCS_SEND_CHAT_MESSAGE(ClientSession session, object message)
        {
            var listBoxRecvMessage = formMain.getListBoxSendRecvMessage();

            var chatMessage = message as BinaryChatMessageDefine.PACK_CS_SEND_CHAT_MESSAGE;
            if (chatMessage == null)
            {
                listBoxRecvMessage.InvokeIfNeeded(
                    formMain.writeListBoxSendRecvMessage,
                    string.Format("Recv wrong onCS_SEND_CHAT_MESSAGE : {0}, from : {1}", message, session));
                return -1;
            }

            listBoxRecvMessage.InvokeIfNeeded(
                formMain.writeListBoxSendRecvMessage,
                string.Format("Recv : {0}, from : {1}", chatMessage.stringField, session));

            return 0;
        }

        private int onCS_BROADCAST_CHAT_MESSAGE(ClientSession session, object message)
        {
            var listBoxRecvMessage = formMain.getListBoxSendRecvMessage();

            var chatMessage = message as BinaryChatMessageDefine.PACK_CS_BROADCAST_CHAT_MESSAGE;
            if (chatMessage == null)
            {
                listBoxRecvMessage.InvokeIfNeeded(
                    formMain.writeListBoxSendRecvMessage,
                    string.Format("Recv wrong onCS_BROADCAST_CHAT_MESSAGE : {0}, from : {1}", message, session));
                return -1;
            }

            listBoxRecvMessage.InvokeIfNeeded(
                formMain.writeListBoxSendRecvMessage,
                string.Format("Recv : {0}, from : {1}", chatMessage.stringField, session));

            var sendChatMessage = new BinaryChatMessageDefine.PACK_SC_BROADCAST_CHAT_MESSAGE();
            sendChatMessage.stringField = chatMessage.stringField;
            foreach (var acceptedSession in ClientSession.ClientSessionList)
            {
                acceptedSession.AsyncSend((int)BinaryChatMessageDefine.ENUM_SC_.SC_BROADCAST_CHAT_MESSAGE, chatMessage);
            }

            return 0;
        }
    }
}