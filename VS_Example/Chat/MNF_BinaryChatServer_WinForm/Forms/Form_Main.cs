﻿using System;
using System.Windows.Forms;
using MNF_Server_WinForm.MNF_Library;
using MNF_Common;
using MNF;

namespace MNF_Server_WinForm
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();

            textBox_ServerIP.Text = Utility.getLocalAddress().ToString();
            textBox_ServerPort.Text = "20000";
        }

        private bool initMNF()
        {
            try
            {
                LogManager.Instance.SetLogWriter(new FileLogWriter());
                if (LogManager.Instance.Init() == false)
                    return false;
                
                if (TcpHelper.Instance.Start(true) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.Start() failed");
                    return false;
                }

                if (TcpHelper.Instance.StartAccept<ClientSession, BinaryMessageDispatcher>(
                    textBox_ServerIP.Text, textBox_ServerPort.Text, 500) == false)
                {
                    LogManager.Instance.WriteError("TcpHelper.Instance.startAccept<ClientSession, BinaryMessageDispatcher>() failed");
                    return false;
                }

                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return false;
            }
        }

        public ListBox getListBoxMNFLog()
        {
            return listBox_MNF_Log;
        }

        public ListBox getListBoxSendRecvMessage()
        {
            return listBox_Send_RecvMessage;
        }

        public ListBox getListBoxAcceptedSessions()
        {
            return listBox_AcceptedSessions;
        }

        public void writeListBoxMNFLog(string outputLog)
        {
            if (listBox_MNF_Log.Items.Count > 100)
                listBox_MNF_Log.Items.RemoveAt(0);
            listBox_MNF_Log.Items.Add(outputLog);
        }

        public void writeListBoxSendRecvMessage(string outputLog)
        {
            if (listBox_Send_RecvMessage.Items.Count > 100)
                listBox_Send_RecvMessage.Items.RemoveAt(0);
            listBox_Send_RecvMessage.Items.Add(outputLog);
        }

        public void addListBoxAcceptedSessions(MNF.TCPSession session)
        {
            listBox_AcceptedSessions.Items.Add(session);
        }

        public void removeListBoxAcceptedSessions(MNF.TCPSession session)
        {
            listBox_AcceptedSessions.Items.Remove(session);
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            initMNF();
        }

        private void button_Send_Click(object sender, EventArgs e)
        {
            var chatMessage = new BinaryChatMessageDefine.PACK_SC_SEND_CHAT_MESSAGE();
            chatMessage.stringField = textBox_SendChatMessage.Text;

            var listBoxRecvMessage = getListBoxSendRecvMessage();
            listBoxRecvMessage.InvokeIfNeeded(writeListBoxSendRecvMessage, string.Format("Send : {0}", chatMessage.stringField));

            foreach (var clientSession in ClientSession.ClientSessionList)
            {
                clientSession.AsyncSend((int)BinaryChatMessageDefine.ENUM_SC_.SC_SEND_CHAT_MESSAGE, chatMessage);
            }

            textBox_SendChatMessage.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox_Send_RecvMessage.Items.Clear();
            listBox_MNF_Log.Items.Clear();
        }

        private void button_send_individual_Click(object sender, EventArgs e)
        {
            var chatMessage = new BinaryChatMessageDefine.PACK_SC_SEND_CHAT_MESSAGE();
            chatMessage.stringField = textBox_SendChatMessage.Text;

            foreach (var item in listBox_AcceptedSessions.SelectedItems)
            {
                var _session = item as ClientSession;
                _session.AsyncSend((int)BinaryChatMessageDefine.ENUM_SC_.SC_SEND_CHAT_MESSAGE, chatMessage);
            }
        }
    }
}
