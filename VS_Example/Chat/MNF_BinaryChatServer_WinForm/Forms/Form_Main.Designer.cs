﻿namespace MNF_Server_WinForm
{
    partial class Form_Main
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button_Start = new System.Windows.Forms.Button();
            this.textBox_ServerPort = new System.Windows.Forms.TextBox();
            this.textBox_ServerIP = new System.Windows.Forms.TextBox();
            this.Port = new System.Windows.Forms.Label();
            this.IP = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_ClearMessage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_SendChatMessage = new System.Windows.Forms.TextBox();
            this.button_Send = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label_Send_RecvMessage = new System.Windows.Forms.Label();
            this.listBox_Send_RecvMessage = new System.Windows.Forms.ListBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.listBox_MNF_Log = new System.Windows.Forms.ListBox();
            this.label_Log = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_send_individual = new System.Windows.Forms.Button();
            this.listBox_AcceptedSessions = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button_Start);
            this.panel1.Controls.Add(this.textBox_ServerPort);
            this.panel1.Controls.Add(this.textBox_ServerIP);
            this.panel1.Controls.Add(this.Port);
            this.panel1.Controls.Add(this.IP);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(290, 85);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "Start Chat Server";
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(166, 22);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(75, 54);
            this.button_Start.TabIndex = 6;
            this.button_Start.Text = "Start";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // textBox_ServerPort
            // 
            this.textBox_ServerPort.Location = new System.Drawing.Point(60, 55);
            this.textBox_ServerPort.Name = "textBox_ServerPort";
            this.textBox_ServerPort.Size = new System.Drawing.Size(100, 21);
            this.textBox_ServerPort.TabIndex = 3;
            // 
            // textBox_ServerIP
            // 
            this.textBox_ServerIP.Location = new System.Drawing.Point(60, 22);
            this.textBox_ServerIP.Name = "textBox_ServerIP";
            this.textBox_ServerIP.Size = new System.Drawing.Size(100, 21);
            this.textBox_ServerIP.TabIndex = 2;
            // 
            // Port
            // 
            this.Port.AutoSize = true;
            this.Port.Location = new System.Drawing.Point(27, 58);
            this.Port.Name = "Port";
            this.Port.Size = new System.Drawing.Size(27, 12);
            this.Port.TabIndex = 1;
            this.Port.Text = "Port";
            // 
            // IP
            // 
            this.IP.AutoSize = true;
            this.IP.Location = new System.Drawing.Point(27, 25);
            this.IP.Name = "IP";
            this.IP.Size = new System.Drawing.Size(16, 12);
            this.IP.TabIndex = 0;
            this.IP.Text = "IP";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_ClearMessage);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textBox_SendChatMessage);
            this.panel2.Controls.Add(this.button_Send);
            this.panel2.Location = new System.Drawing.Point(12, 103);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(290, 85);
            this.panel2.TabIndex = 1;
            // 
            // button_ClearMessage
            // 
            this.button_ClearMessage.Location = new System.Drawing.Point(206, 24);
            this.button_ClearMessage.Name = "button_ClearMessage";
            this.button_ClearMessage.Size = new System.Drawing.Size(79, 52);
            this.button_ClearMessage.TabIndex = 8;
            this.button_ClearMessage.Text = "Clear Message";
            this.button_ClearMessage.UseVisualStyleBackColor = true;
            this.button_ClearMessage.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Send Chat Message";
            // 
            // textBox_SendChatMessage
            // 
            this.textBox_SendChatMessage.Location = new System.Drawing.Point(3, 24);
            this.textBox_SendChatMessage.Name = "textBox_SendChatMessage";
            this.textBox_SendChatMessage.Size = new System.Drawing.Size(197, 21);
            this.textBox_SendChatMessage.TabIndex = 7;
            // 
            // button_Send
            // 
            this.button_Send.Location = new System.Drawing.Point(3, 51);
            this.button_Send.Name = "button_Send";
            this.button_Send.Size = new System.Drawing.Size(197, 25);
            this.button_Send.TabIndex = 0;
            this.button_Send.Text = "Broadcast Message";
            this.button_Send.UseVisualStyleBackColor = true;
            this.button_Send.Click += new System.EventHandler(this.button_Send_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label_Send_RecvMessage);
            this.panel4.Controls.Add(this.listBox_Send_RecvMessage);
            this.panel4.Location = new System.Drawing.Point(12, 194);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(290, 164);
            this.panel4.TabIndex = 1;
            // 
            // label_Send_RecvMessage
            // 
            this.label_Send_RecvMessage.AutoSize = true;
            this.label_Send_RecvMessage.Location = new System.Drawing.Point(3, 3);
            this.label_Send_RecvMessage.Name = "label_Send_RecvMessage";
            this.label_Send_RecvMessage.Size = new System.Drawing.Size(125, 12);
            this.label_Send_RecvMessage.TabIndex = 3;
            this.label_Send_RecvMessage.Text = "Send/Recv Message";
            // 
            // listBox_Send_RecvMessage
            // 
            this.listBox_Send_RecvMessage.FormattingEnabled = true;
            this.listBox_Send_RecvMessage.HorizontalScrollbar = true;
            this.listBox_Send_RecvMessage.ItemHeight = 12;
            this.listBox_Send_RecvMessage.Location = new System.Drawing.Point(5, 18);
            this.listBox_Send_RecvMessage.Name = "listBox_Send_RecvMessage";
            this.listBox_Send_RecvMessage.ScrollAlwaysVisible = true;
            this.listBox_Send_RecvMessage.Size = new System.Drawing.Size(280, 136);
            this.listBox_Send_RecvMessage.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.listBox_MNF_Log);
            this.panel5.Controls.Add(this.label_Log);
            this.panel5.Location = new System.Drawing.Point(12, 363);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(290, 152);
            this.panel5.TabIndex = 3;
            // 
            // listBox_MNF_Log
            // 
            this.listBox_MNF_Log.FormattingEnabled = true;
            this.listBox_MNF_Log.HorizontalScrollbar = true;
            this.listBox_MNF_Log.ItemHeight = 12;
            this.listBox_MNF_Log.Location = new System.Drawing.Point(3, 18);
            this.listBox_MNF_Log.Name = "listBox_MNF_Log";
            this.listBox_MNF_Log.ScrollAlwaysVisible = true;
            this.listBox_MNF_Log.Size = new System.Drawing.Size(282, 124);
            this.listBox_MNF_Log.TabIndex = 2;
            // 
            // label_Log
            // 
            this.label_Log.AutoSize = true;
            this.label_Log.Location = new System.Drawing.Point(3, 3);
            this.label_Log.Name = "label_Log";
            this.label_Log.Size = new System.Drawing.Size(59, 12);
            this.label_Log.TabIndex = 0;
            this.label_Log.Text = "MNF_Log";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_send_individual);
            this.groupBox1.Controls.Add(this.listBox_AcceptedSessions);
            this.groupBox1.Location = new System.Drawing.Point(308, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(291, 503);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Accepted Sessions";
            // 
            // button_send_individual
            // 
            this.button_send_individual.Location = new System.Drawing.Point(6, 470);
            this.button_send_individual.Name = "button_send_individual";
            this.button_send_individual.Size = new System.Drawing.Size(279, 23);
            this.button_send_individual.TabIndex = 1;
            this.button_send_individual.Text = "Single Message";
            this.button_send_individual.UseVisualStyleBackColor = true;
            this.button_send_individual.Click += new System.EventHandler(this.button_send_individual_Click);
            // 
            // listBox_AcceptedSessions
            // 
            this.listBox_AcceptedSessions.FormattingEnabled = true;
            this.listBox_AcceptedSessions.ItemHeight = 12;
            this.listBox_AcceptedSessions.Location = new System.Drawing.Point(6, 20);
            this.listBox_AcceptedSessions.Name = "listBox_AcceptedSessions";
            this.listBox_AcceptedSessions.Size = new System.Drawing.Size(279, 436);
            this.listBox_AcceptedSessions.TabIndex = 0;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 527);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_Main";
            this.Text = "MNF Binary Chat Server";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Port;
        private System.Windows.Forms.Label IP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBox_ServerPort;
        private System.Windows.Forms.TextBox textBox_ServerIP;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_Send;
        private System.Windows.Forms.Label label_Send_RecvMessage;
        private System.Windows.Forms.ListBox listBox_Send_RecvMessage;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox listBox_MNF_Log;
        private System.Windows.Forms.Label label_Log;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_SendChatMessage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_ClearMessage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox listBox_AcceptedSessions;
        private System.Windows.Forms.Button button_send_individual;
    }
}

