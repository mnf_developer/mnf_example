﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MNF_Server_WinForm
{
    static class FormsUtility
    {
        public static Form_Main GetFormMain()
        {
            var formCollection = Application.OpenForms;
            foreach (var form in formCollection)
            {
                var formMain = form as Form_Main;
                if (formMain != null)
                    return formMain;
            }
            return null;
        }
    }
}
