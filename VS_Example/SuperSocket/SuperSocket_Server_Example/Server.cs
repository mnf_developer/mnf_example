﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.SocketBase.Config;

namespace SuperSocket_Server
{
    public class BinaryServer : AppServer<Session, BinaryRequestInfo>
    {
        public BinaryServer()
            : base(new DefaultReceiveFilterFactory<BinaryReceiveFilter, BinaryRequestInfo>())
        {
            Console.WriteLine("run BinaryServer!");
        }

        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            return base.Setup(rootConfig, config);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
        }

        protected override void OnStopped()
        {
            base.OnStopped();
        }
    }

    public class JsonServer : AppServer<Session, BinaryRequestInfo>
    {
        public JsonServer()
            : base(new DefaultReceiveFilterFactory<JsonReceiveFilter, BinaryRequestInfo>())
        {
            Console.WriteLine("run JsonServer!");
        }

        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            return base.Setup(rootConfig, config);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
        }

        protected override void OnStopped()
        {
            base.OnStopped();
        }
    }
}
