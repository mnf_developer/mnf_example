﻿using System;
using SuperSocket.Common;
using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase.Protocol;
using MNF_Common;
using MNF.Message;
using MNF;

namespace SuperSocket_Server
{
    class BinaryReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    {
        public BinaryReceiveFilter()
            : base(MNF.Message.MessageBuffer<MNF.Message.BinaryMessageHeader>.SerializedHeaderSize)
        {
        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            return ((int)header[offset + 1] * 256 + (int)header[offset + 0]);
        }

        protected override BinaryRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            var messageId = (int)header.Array[3] * 256 + (int)header.Array[2];
            var id = (BinaryMessageDefine.ENUM_CS_)messageId;
            return new BinaryRequestInfo(id.ToString(), bodyBuffer.CloneRange(offset, length));
        }
    }

    class JsonReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    {
        public JsonReceiveFilter()
            : base(MNF.Message.MessageBuffer<MNF.Message.BinaryMessageHeader>.SerializedHeaderSize)
        {
        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            return ((int)header[offset + 1] * 256 + (int)header[offset + 0]);
        }

        protected override BinaryRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            var messageId = (int)header.Array[3] * 256 + (int)header.Array[2];
            var id = (JsonMessageDefine._ENUM_)messageId;
            return new BinaryRequestInfo(id.ToString(), bodyBuffer.CloneRange(offset, length));
        }
    }
}
