﻿using System;
using System.Text;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using MNF_Common;

namespace SuperSocket_Server
{
    public class CS_AES_JSON_ECHO : CommandBase<Session, BinaryRequestInfo>
    {
        private int count = 0;

        public override void ExecuteCommand(Session session, BinaryRequestInfo requestInfo)
        {
            try
            {
                // byte -> string
                var jsonMessage = Encoding.Default.GetString(requestInfo.Body);

				// string -> object
				var message = (aesJsonMessageDefine.PACK_CS_AES_JSON_ECHO)JsonFx.Json.JsonReader.Deserialize(
                    jsonMessage, typeof(aesJsonMessageDefine.PACK_CS_AES_JSON_ECHO));
                
                var jsonEcho = new aesJsonMessageDefine.PACK_SC_AES_JSON_ECHO();
                jsonEcho.sandwiches = message.sandwiches;

                var messageSerializer = new aesJsonMessageSerializer();
                messageSerializer.Serialize((int)aesJsonMessageDefine.ENUM_SC_.SC_AES_JSON_ECHO, jsonEcho);
                session.Send(messageSerializer.GetSerializedBuffer(), 0, messageSerializer.SerializedLength);

                if (++count % 100 == 0)
                    Console.WriteLine("{0}, {1}, {2}", session, GetType(), count);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
