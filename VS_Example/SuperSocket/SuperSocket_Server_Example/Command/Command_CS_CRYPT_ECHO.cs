﻿using System;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using MNF_Common;
using MNF;

namespace SuperSocket_Server
{
    public class CS_CRYPT_ECHO : CommandBase<Session, BinaryRequestInfo>
    {
        private int count = 0;

        public override void ExecuteCommand(Session session, BinaryRequestInfo requestInfo)
        {
            try
            {
                var csMessage = MarshalHelper.RawDeSerialize(
                    requestInfo.Body, typeof(CryptBinaryMessageDefine.PACK_CS_CRYPT_ECHO)) as CryptBinaryMessageDefine.PACK_CS_CRYPT_ECHO;

                var scMessage = new CryptBinaryMessageDefine.PACK_SC_CRYPT_ECHO();
                scMessage.boolField = csMessage.boolField;
                scMessage.int64Field = csMessage.int64Field;
                scMessage.intField = csMessage.intField;
                scMessage.stringField = csMessage.stringField;

                var messageSerializer = new CryptBinaryMessageSerializer();
                messageSerializer.Serialize((int)CryptBinaryMessageDefine.ENUM_SC_.SC_CRYPT_ECHO, scMessage);
                session.Send(messageSerializer.GetSerializedBuffer(), 0, messageSerializer.SerializedLength);

                if (++count % 100 == 0)
                    Console.WriteLine("{0}, {1}, {2}", session, GetType(), count);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
