﻿using System;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using MNF_Common;
using MNF.Message;
using MNF;

namespace SuperSocket_Server
{
    public class CS_ECHO : CommandBase<Session, BinaryRequestInfo>
    {
        private int count = 0;

        public override void ExecuteCommand(Session session, BinaryRequestInfo requestInfo)
        {
            try
            {
                var csMessage = MarshalHelper.RawDeSerialize(
                    requestInfo.Body, typeof(BinaryMessageDefine.PACK_CS_ECHO)) as BinaryMessageDefine.PACK_CS_ECHO;

                var scMessage = new BinaryMessageDefine.PACK_SC_ECHO();
                scMessage.boolField = csMessage.boolField;
                scMessage.int64Field = csMessage.int64Field;
                scMessage.intField = csMessage.intField;
                scMessage.stringField = csMessage.stringField;
                scMessage.structField = csMessage.structField;

                csMessage.intArrayField.CopyTo(scMessage.intArrayField, 0);
                csMessage.structArrayField.CopyTo(scMessage.structArrayField, 0);

                var messageSerializer = new BinaryMessageSerializer();
                messageSerializer.Serialize((int)BinaryMessageDefine.ENUM_SC_.SC_ECHO, scMessage);
                session.Send(messageSerializer.GetSerializedBuffer(), 0, messageSerializer.SerializedLength);

                if (++count % 100 == 0)
                    Console.WriteLine("{0}, {1}, {2}", session, GetType(), count);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
