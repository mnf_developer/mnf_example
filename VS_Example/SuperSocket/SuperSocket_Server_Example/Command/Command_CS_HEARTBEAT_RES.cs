﻿using System;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using MNF_Common;
using MNF;

namespace SuperSocket_Server
{
    public class CS_HEARTBEAT_RES : CommandBase<Session, BinaryRequestInfo>
    {
        public override void ExecuteCommand(Session session, BinaryRequestInfo requestInfo)
        {
            try
            {
                var csMessage = MarshalHelper.RawDeSerialize(
                    requestInfo.Body, typeof(BinaryMessageDefine.PACK_CS_HEARTBEAT_RES)) as BinaryMessageDefine.PACK_CS_HEARTBEAT_RES;

                // update heart beat tick
                session.LastHeartBeatTick = Environment.TickCount;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
