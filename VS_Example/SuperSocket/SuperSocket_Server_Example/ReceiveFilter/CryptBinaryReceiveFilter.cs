﻿using System;
using SuperSocket.Common;
using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase.Protocol;
using MNF_Common;
using MNF.Message;
using MNF.Crypt;
using MNF;

namespace SuperSocket_Server
{
    class CryptBinaryReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    {
        MD5Ref md5Ref = null;

        public CryptBinaryReceiveFilter()
            : base(MessageBuffer<CryptBinaryMessageHeader>.SerializedHeaderSize)
        {
            md5Ref = new MD5Ref();
        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            return ((int)header[offset + 1] * 256 + (int)header[offset + 0]);
        }

        protected override BinaryRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            var messageHeader = MarshalHelper.RawDeSerialize(header.Array, typeof(CryptBinaryMessageHeader)) as CryptBinaryMessageHeader;

            // convert hash to two UInt64
            var encryptedByte = md5Ref.Md5Sum(bodyBuffer, offset, length);
            UInt64 checksum1 = 0, checksum2 = 0;
            md5Ref.convertToInteger(encryptedByte, out checksum1, out checksum2);

            // compare checksum
            if ((checksum1 != messageHeader.checksum1) || (checksum2 != messageHeader.checksum2))
                return new BinaryRequestInfo(null, null);

            var id = (CryptBinaryMessageDefine.ENUM_CS_)messageHeader.messageID;
            return new BinaryRequestInfo(id.ToString(), bodyBuffer.CloneRange(offset, length));
        }
    }
}
