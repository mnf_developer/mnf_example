﻿using System;
using SuperSocket.SocketEngine;
using SuperSocket.SocketBase;
using MNF;

namespace SuperSocket_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting the LookAround!");
            LookAround.Instance.Start("0", true);

            Console.WriteLine("Starting the server!");
            var bootstrap = BootstrapFactory.CreateBootstrap();

            if (!bootstrap.Initialize())
            {
                Console.WriteLine("Failed to initialize!");
                Console.ReadKey();
                return;
            }

            var result = bootstrap.Start();

            Console.WriteLine("Start result: {0}!", result);

            if (result == StartResult.Failed)
            {
                Console.WriteLine("Failed to start!");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Press key 'q' to stop it!");

            while (Console.ReadKey().KeyChar != 'q')
            {
                Console.WriteLine();
                continue;
            }

            Console.WriteLine();

            //Stop the appServer
            bootstrap.Stop();
            LookAround.Instance.Stop();

            Console.WriteLine("The server was stopped!");
        }
    }
}
