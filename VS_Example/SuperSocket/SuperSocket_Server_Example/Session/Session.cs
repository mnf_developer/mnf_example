﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace SuperSocket_Server
{
    public class Session : AppSession<Session, BinaryRequestInfo>
    {
        public int LastHeartBeatTick { get; set; }

		protected override void OnSessionStarted()
        {
        }

        protected override void HandleUnknownRequest(BinaryRequestInfo requestInfo)
        {
        }

        protected override void HandleException(Exception e)
        {
        }

        protected override void OnSessionClosed(CloseReason reason)
        {
            base.OnSessionClosed(reason);
        }
    }
}