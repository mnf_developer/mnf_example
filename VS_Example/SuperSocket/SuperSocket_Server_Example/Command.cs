﻿using System;
using System.Text;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using JsonFx.Json;
using MNF_Common;
using MNF.Message;
using MNF;

namespace SuperSocket_Server
{
    public class CS_ECHO : CommandBase<Session, BinaryRequestInfo>
    {
        private int count = 0;

        public override void ExecuteCommand(Session session, BinaryRequestInfo requestInfo)
        {
            try
            {
                var csMessage = MarshalHelper.rawDeSerialize(
                    requestInfo.Body, typeof(BinaryMessageDefine.PACK_CS_ECHO)) as BinaryMessageDefine.PACK_CS_ECHO;

                var scMessage = new BinaryMessageDefine.PACK_SC_ECHO();
                scMessage.boolField = csMessage.boolField;
                scMessage.int64Field = csMessage.int64Field;
                scMessage.intField = csMessage.intField;
                scMessage.stringField = csMessage.stringField;
                scMessage.structField = csMessage.structField;

                csMessage.intArrayField.CopyTo(scMessage.intArrayField, 0);
                csMessage.structArrayField.CopyTo(scMessage.structArrayField, 0);

                var messageSerializer = new BinaryMessageSerializer();
                messageSerializer.serialize((int)BinaryMessageDefine.ENUM_SC_.SC_ECHO, ref scMessage);
                session.Send(messageSerializer.getSerializedBuffer(), 0, messageSerializer.SerializedLength);

                if (++count % 100 == 0)
                    Console.WriteLine(count);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }

    public class ECHO : CommandBase<Session, BinaryRequestInfo>
    {
        private int count = 0;

        public override void ExecuteCommand(Session session, BinaryRequestInfo requestInfo)
        {
            try
            {
                // byte -> string
                var jsonMessage = Encoding.Default.GetString(requestInfo.Body);

                // string -> object
                var message = JsonReader.Deserialize(jsonMessage, typeof(JsonMessageDefine.PACK_ECHO));

                var messageSerializer = new JsonMessageSerializer();
                messageSerializer.serialize((int)JsonMessageDefine._ENUM_.ECHO, ref message);
                session.Send(messageSerializer.getSerializedBuffer(), 0, messageSerializer.SerializedLength);

                if (++count % 100 == 0)
                    Console.WriteLine(count);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
