﻿using System;
using System.Threading;
using SuperSocket.SocketBase;
using MNF.Message;
using MNF_Common;

namespace SuperSocket_Server
{
    public delegate void TodoSomething();

    public class IntervalCheckThread
	{
		public IntervalCheckThread(int intervalTick, TodoSomething todo)
		{
			this.intervalTick = intervalTick;
			this.todo = todo;
		}

        public void Start()
        {
			workerThread = new Thread(DoWork);
			workerThread.Start();
        }

		public void DoWork()
		{
            while (shouldStop == false)
			{
				Thread.Sleep(intervalTick);
                todo();
			}
		}

		public void RequestStop()
		{
			shouldStop = true;
		}

		private int intervalTick;
        private TodoSomething todo;
		private volatile bool shouldStop;
		private Thread workerThread = null;
	}   
}