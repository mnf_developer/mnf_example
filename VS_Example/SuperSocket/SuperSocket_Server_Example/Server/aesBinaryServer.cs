﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.SocketBase.Config;

namespace SuperSocket_Server
{
    public class aesBinaryServer : AppServer<Session, BinaryRequestInfo>
    {
        public aesBinaryServer()
            : base(new DefaultReceiveFilterFactory<aesBinaryReceiveFilter, BinaryRequestInfo>())
        {
            Console.WriteLine("run {0}!", GetType());
        }

        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            return base.Setup(rootConfig, config);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
        }

        protected override void OnStopped()
        {
            base.OnStopped();
        }
    }
}
