﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.SocketBase.Config;
using MNF.Message;
using MNF_Common;

namespace SuperSocket_Server
{
    public class BinaryServer : AppServer<Session, BinaryRequestInfo>
    {
        public BinaryServer()
            : base(new DefaultReceiveFilterFactory<CustomBinaryReceiveFilter, BinaryRequestInfo>())
        {
            Console.WriteLine("run {0}!", GetType());
		}

        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            return base.Setup(rootConfig, config);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
            intervalCheckThread = new IntervalCheckThread(5000, sendMessage);
            intervalCheckThread.Start();
		}

        protected override void OnStopped()
        {
			intervalCheckThread.RequestStop();
			base.OnStopped();
		}

        private void sendMessage()
        {
			foreach (var session in GetAllSessions())
			{
				int currentTick = Environment.TickCount;
				int elapsedTick = currentTick - session.LastHeartBeatTick;
				if (elapsedTick > 5000)
				{
					Console.WriteLine("{0} is wrong session.", session.SessionID);
				}

				var scMessage = new BinaryMessageDefine.PACK_SC_HEARTBEAT_REQ();
				scMessage.tickCount = Environment.TickCount;

				var messageSerializer = new BinaryMessageSerializer();
				messageSerializer.Serialize((int)BinaryMessageDefine.ENUM_SC_.SC_HEARTBEAT_REQ, scMessage);
				session.Send(messageSerializer.GetSerializedBuffer(), 0, messageSerializer.SerializedLength);
			}
        }

        private IntervalCheckThread intervalCheckThread = null;
    }
}
