﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.SocketBase.Config;
using MNF.Message;
using MNF_Common;

namespace SuperSocket_Server
{
    public class JsonServer : AppServer<Session, BinaryRequestInfo>
    {
        public JsonServer()
            : base(new DefaultReceiveFilterFactory<CustomJsonReceiveFilter, BinaryRequestInfo>())
        {
            Console.WriteLine("run {0}!", GetType());
        }

        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            return base.Setup(rootConfig, config);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
			intervalCheckThread = new IntervalCheckThread(5000, sendMessage);
			intervalCheckThread.Start();
		}

        protected override void OnStopped()
        {
			intervalCheckThread.RequestStop();
			base.OnStopped();
		}

		private void sendMessage()
		{
			foreach (var session in GetAllSessions())
			{
				int currentTick = Environment.TickCount;
				int elapsedTick = currentTick - session.LastHeartBeatTick;
				if (elapsedTick > 5000)
				{
					Console.WriteLine("{0} is wrong session.", session.SessionID);
				}

                var heartbeat = new JsonMessageDefine.PACK_SC_JSON_HEARTBEAT_REQ();
                heartbeat.tickCount = Environment.TickCount;

				var messageSerializer = new JsonMessageSerializer();
                messageSerializer.Serialize((int)JsonMessageDefine.ENUM_SC_.SC_JSON_HEARTBEAT_REQ, heartbeat);
				session.Send(messageSerializer.GetSerializedBuffer(), 0, messageSerializer.SerializedLength);
			}
		}

        private IntervalCheckThread intervalCheckThread = null;
	}
}
