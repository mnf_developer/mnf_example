﻿using MNF_Common;
using MNF;

public class CryptJsonMessageDispatcher : DefaultDispatchHelper<CryptJsonClientSession, CryptJsonMessageDefine, CryptJsonMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_CRYPT_JSON_ECHO(CryptJsonClientSession session, object message)
    {
        var echo = (CryptJsonMessageDefine.PACK_SC_CRYPT_JSON_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var jsonEcho = new CryptJsonMessageDefine.PACK_CS_CRYPT_JSON_ECHO();
        jsonEcho.sandwiches = echo.sandwiches;
        session.AsyncSend((int)CryptJsonMessageDefine.ENUM_CS_.CS_CRYPT_JSON_ECHO, jsonEcho);

        return 0;
    }
}

