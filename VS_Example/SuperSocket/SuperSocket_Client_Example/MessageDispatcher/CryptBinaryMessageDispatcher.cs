﻿using MNF_Common;
using MNF;

public class CryptBinaryMessageDispatcher : DefaultDispatchHelper<CryptBinaryClientSession, CryptBinaryMessageDefine, CryptBinaryMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_CRYPT_ECHO(CryptBinaryClientSession session, object message)
    {
        var echo = (CryptBinaryMessageDefine.PACK_SC_CRYPT_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var ecshoPacket = new CryptBinaryMessageDefine.PACK_CS_CRYPT_ECHO();
        session.AsyncSend((int)CryptBinaryMessageDefine.ENUM_CS_.CS_CRYPT_ECHO, ecshoPacket);

        return 0;
    }
}