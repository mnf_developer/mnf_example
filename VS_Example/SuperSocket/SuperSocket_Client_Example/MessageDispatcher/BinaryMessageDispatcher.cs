﻿using MNF_Common;
using MNF;

public class BinaryMessageDispatcher : DefaultDispatchHelper<CustomBinaryClientSession, BinaryMessageDefine, BinaryMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_ECHO(CustomBinaryClientSession session, object message)
    {
        var echo = (BinaryMessageDefine.PACK_SC_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var ecshoPacket = new BinaryMessageDefine.PACK_CS_ECHO();
        session.AsyncSend((int)BinaryMessageDefine.ENUM_CS_.CS_ECHO, ecshoPacket);

        return 0;
	}

	int onSC_HEARTBEAT_REQ(CustomBinaryClientSession session, object message)
	{
        var heartbeatReq = (BinaryMessageDefine.PACK_SC_HEARTBEAT_REQ)message;
        LogManager.Instance.Write("{0}, {1}, {2}", session, heartbeatReq.GetType(), heartbeatReq.tickCount);

        var heartbeatRes = new BinaryMessageDefine.PACK_CS_HEARTBEAT_RES();
        session.AsyncSend((int)BinaryMessageDefine.ENUM_CS_.CS_HEARTBEAT_RES, heartbeatRes);

		return 0;
	}
}