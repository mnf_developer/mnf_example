﻿using MNF_Common;
using MNF;

public class aesJsonMessageDispatcher : DefaultDispatchHelper<aesJsonClientSession, aesJsonMessageDefine, aesJsonMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_AES_JSON_ECHO(aesJsonClientSession session, object message)
    {
        var echo = (aesJsonMessageDefine.PACK_SC_AES_JSON_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var jsonEcho = new aesJsonMessageDefine.PACK_CS_AES_JSON_ECHO();
        jsonEcho.sandwiches = echo.sandwiches;
        session.AsyncSend((int)aesJsonMessageDefine.ENUM_CS_.CS_AES_JSON_ECHO, jsonEcho);

        return 0;
    }
}

