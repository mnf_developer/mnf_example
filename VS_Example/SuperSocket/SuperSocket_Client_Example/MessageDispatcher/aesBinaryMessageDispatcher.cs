﻿using MNF_Common;
using MNF;

public class aesBinaryMessageDispatcher : DefaultDispatchHelper<aesBinaryClientSession, aesBinaryMessageDefine, aesBinaryMessageDefine.ENUM_SC_>
{
    int count = 0;

    int onSC_AES_ECHO(aesBinaryClientSession session, object message)
    {
        var echo = (aesBinaryMessageDefine.PACK_SC_AES_ECHO)message;
        if (++count % 100 == 0)
            LogManager.Instance.Write("{0}, {1}, {2}", session, echo.GetType(), count);

        var ecshoPacket = new aesBinaryMessageDefine.PACK_CS_AES_ECHO();
        session.AsyncSend((int)aesBinaryMessageDefine.ENUM_CS_.CS_AES_ECHO, ecshoPacket);

        return 0;
    }
}