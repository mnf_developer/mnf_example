﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using JsonFx.Json;
using MNF;
using MNF.Message;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class JsonMessageHeader_
{
    [MarshalAs(UnmanagedType.I2)]
    public short messageSize;

    [MarshalAs(UnmanagedType.U2)]
    public ushort messageID;
}

public class JsonMessageSerializer_ : Serializer<JsonMessageHeader_>
{
    public byte[] convertToByte(JsonMessageHeader_ header)
    {
        int byteArray = 0;
        byteArray = header.messageID;
        byteArray <<= 16;
        byteArray |= (int)header.messageSize;

        return BitConverter.GetBytes(byteArray);
    }

    protected override void _serialize<T>(int messageID, ref T managedData)
    {
        var jsonData = JsonWriter.Serialize(managedData);
        var convertedData = Encoding.UTF8.GetBytes(jsonData);
        SerializedLength = convertedData.Length;

        MessageBuffer.MessageHeader.messageSize = (short)convertedData.Length;
        MessageBuffer.MessageHeader.messageID = (ushort)messageID;

        var convertedHeader = convertToByte(MessageBuffer.MessageHeader);
        SerializedLength += convertedHeader.Length;

        Buffer.BlockCopy(convertedHeader, 0, MessageBuffer.SerializedBuffer, 0, convertedHeader.Length);
        Buffer.BlockCopy(convertedData, 0, MessageBuffer.SerializedBuffer, convertedHeader.Length, convertedData.Length);
    }
}

public class JsonMessageDeserializer_ : Deserializer<JsonMessageHeader_>
{
    protected override void _deserialize(SessionBase session, ref ParsingResult parsingResult)
    {
        var tcpSession = session as TCPSession;

        // check readable header
        if (tcpSession.RecvCircularBuffer.ReadableSize < MessageBuffer<JsonMessageHeader_>.SerializedHeaderSize)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_INCOMPLETE;
            return;
        }

        // read header
        if (tcpSession.RecvCircularBuffer.read(MessageBuffer.SerializedBuffer, MessageBuffer<JsonMessageHeader_>.SerializedHeaderSize) == false)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        int messageBodySize = (int)(MessageBuffer.SerializedBuffer[1] * 256) + (int)MessageBuffer.SerializedBuffer[0];
        int messageSize = messageBodySize + MessageBuffer<JsonMessageHeader_>.SerializedHeaderSize;
        int messageId = (int)(MessageBuffer.SerializedBuffer[3] * 256) + (int)MessageBuffer.SerializedBuffer[2];

        // check header id
        var dispatchInfo = tcpSession.DispatchHelper.tryGetMessageDispatch(messageId);
        if (dispatchInfo == null)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        // check readalbe body
        if (tcpSession.RecvCircularBuffer.ReadableSize < MessageBuffer<JsonMessageHeader_>.SerializedHeaderSize + messageBodySize)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_INCOMPLETE;
            return;
        }

        // read body
        if (tcpSession.RecvCircularBuffer.read(MessageBuffer.SerializedBuffer, MessageBuffer<JsonMessageHeader_>.SerializedHeaderSize, messageBodySize) == false)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        // byte -> string
        var jsonMessage = Encoding.Default.GetString(MessageBuffer.SerializedBuffer);

        // string -> object
        var message = JsonReader.Deserialize(jsonMessage, dispatchInfo.messageType);
        if (message == null)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_COMPLETE;
        parsingResult.dispatcher = dispatchInfo.dispatcher;
        parsingResult.message = message;
        parsingResult.messageSize = messageSize;

        // pop dispatched message size
        tcpSession.RecvCircularBuffer.pop(messageSize);
    }
}