﻿using System;
using System.Runtime.InteropServices;
using MNF;
using MNF.Message;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class BinaryMessageHeader_
{
    [MarshalAs(UnmanagedType.I2)]
    public short messageSize;

    [MarshalAs(UnmanagedType.U2)]
    public ushort messageID;
}

public class BinaryMessageSerializer_ : Serializer
{
    public MessageBuffer<BinaryMessageHeader_> MessageBuffer { get; private set; }

    public BinaryMessageSerializer_()
    {
        MessageBuffer = new MessageBuffer<BinaryMessageHeader_>();
    }

    public override byte[] getSerializedBuffer()
    {
        return MessageBuffer.SerializedBuffer;
    }

    protected override void _serialize<T>(int messageID, ref T managedData)
    {
        int serializedSize = 0;
        MarshalHelper.rawSerialize(
            ref managedData
            , MessageBuffer.SerializedBuffer
            , MessageBuffer<BinaryMessageHeader_>.SerializedHeaderSize
            , ref serializedSize);
        SerializedLength = serializedSize;

        BinaryMessageHeader_ messageHeader = MessageBuffer.MessageHeader;
        messageHeader.messageSize = (short)(serializedSize);
        messageHeader.messageID = (ushort)messageID;
        MarshalHelper.rawSerialize(
            ref messageHeader
            , MessageBuffer.SerializedBuffer
            , 0
            , ref serializedSize);
        SerializedLength += serializedSize;
    }
}

public class BinaryMessageDeserializer_ : Deserializer
{
    private IntPtr marshalAllocatedBuffer;
    private int marshalAllocatedBufferSize = 0;

    public MessageBuffer<BinaryMessageHeader_> MessageBuffer { get; private set; }

    public BinaryMessageDeserializer_()
    {
        MessageBuffer = new MessageBuffer<BinaryMessageHeader_>();
        marshalAllocatedBufferSize = 20480;
        marshalAllocatedBuffer = MarshalHelper.allocGlobalHeap(marshalAllocatedBufferSize);
    }

    ~BinaryMessageDeserializer_()
    {
        MarshalHelper.deAllocGlobalHeap(marshalAllocatedBuffer);
    }

    protected override void _deserialize(SessionBase session, ref ParsingResult parsingResult)
    {
        var tcpSession = session as TCPSession;

        // check readable header
        if (tcpSession.RecvCircularBuffer.ReadableSize < MessageBuffer<BinaryMessageHeader_>.SerializedHeaderSize)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_INCOMPLETE;
            return;
        }

        // read header
        if (tcpSession.RecvCircularBuffer.read(MessageBuffer.SerializedBuffer, MessageBuffer<BinaryMessageHeader_>.SerializedHeaderSize) == false)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        int messageBodySize = (int)(MessageBuffer.SerializedBuffer[1] * 256) + (int)MessageBuffer.SerializedBuffer[0];
        int messageSize = messageBodySize + MessageBuffer<BinaryMessageHeader_>.SerializedHeaderSize;
        int messageId = (int)(MessageBuffer.SerializedBuffer[3] * 256) + (int)MessageBuffer.SerializedBuffer[2];

        // check header id
        var dispatchInfo = tcpSession.DispatchHelper.tryGetMessageDispatch(messageId);
        if (dispatchInfo == null)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        // check readalbe body
        if (tcpSession.RecvCircularBuffer.ReadableSize < MessageBuffer<BinaryMessageHeader_>.SerializedHeaderSize + messageBodySize)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_INCOMPLETE;
            return;
        }

        // read body
        if (tcpSession.RecvCircularBuffer.read(MessageBuffer.SerializedBuffer, MessageBuffer<BinaryMessageHeader_>.SerializedHeaderSize, messageBodySize) == false)
        {
            parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_ERROR;
            return;
        }

        object message = MarshalHelper.rawDeSerialize(
            MessageBuffer.SerializedBuffer
            , dispatchInfo.messageType
            , 0
            , ref marshalAllocatedBuffer
            , ref marshalAllocatedBufferSize);
        if (message == null)
            throw new Exception(string.Format("Dispatcher({0}), Expect packet size({1})",
                dispatchInfo, MarshalHelper.getManagedDataSize(dispatchInfo.messageType)));

        parsingResult.parsingResultEnum = ParsingResult.ParsingResultEnum.PARSING_COMPLETE;
        parsingResult.dispatcher = dispatchInfo.dispatcher;
        parsingResult.message = message;
        parsingResult.messageSize = messageSize;

        // pop dispatched message size
        tcpSession.RecvCircularBuffer.pop(messageSize);
    }
}