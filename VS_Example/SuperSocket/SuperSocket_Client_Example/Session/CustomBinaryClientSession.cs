﻿using MNF;
using MNF_Common;

public class CustomBinaryClientSession : TCPSession<CustomBinaryMessageSerializer, CustomBinaryMessageDeserializer>
{
    public override int OnConnectSuccess()
    {
        LogManager.Instance.Write("OnConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var ecshoPacket = new BinaryMessageDefine.PACK_CS_ECHO();
        AsyncSend((int)BinaryMessageDefine.ENUM_CS_.CS_ECHO, ecshoPacket);

        return 0;
    }

    public override int OnConnectFail()
    {
        LogManager.Instance.Write("OnConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int OnDisconnect()
    {
        LogManager.Instance.Write("OnDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}