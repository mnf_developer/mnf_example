﻿using MNF;
using MNF_Common;

public class aesJsonClientSession : TCPSession<aesJsonMessageSerializer, aesJsonMessageDeserializer>
{
    public override int OnConnectSuccess()
    {
        LogManager.Instance.Write("OnConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var jsonEcho = new aesJsonMessageDefine.PACK_CS_AES_JSON_ECHO();
        jsonEcho.sandwiches = Sandwich.createSandwichList(10);
        AsyncSend((int)aesJsonMessageDefine.ENUM_CS_.CS_AES_JSON_ECHO, jsonEcho);

        return 0;
    }

    public override int OnConnectFail()
    {
        LogManager.Instance.Write("OnConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int OnDisconnect()
    {
        LogManager.Instance.Write("OnDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}
