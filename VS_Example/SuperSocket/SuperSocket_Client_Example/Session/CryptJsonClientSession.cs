﻿using MNF;
using MNF_Common;

public class CryptJsonClientSession : TCPSession<CryptJsonMessageSerializer, CryptJsonMessageDeserializer>
{
    public override int OnConnectSuccess()
    {
        LogManager.Instance.Write("OnConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var jsonEcho = new CryptJsonMessageDefine.PACK_CS_CRYPT_JSON_ECHO();
        jsonEcho.sandwiches = Sandwich.createSandwichList(10);
        AsyncSend((int)CryptJsonMessageDefine.ENUM_CS_.CS_CRYPT_JSON_ECHO, jsonEcho);

        return 0;
    }

    public override int OnConnectFail()
    {
        LogManager.Instance.Write("OnConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int OnDisconnect()
    {
        LogManager.Instance.Write("OnDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}
