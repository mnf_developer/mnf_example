﻿using MNF;
using MNF_Common;

public class aesBinaryClientSession : TCPSession<aesBinaryMessageSerializer, aesBinaryMessageDeserializer>
{
    public override int OnConnectSuccess()
    {
        LogManager.Instance.Write("OnConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var ecshoPacket = new aesBinaryMessageDefine.PACK_CS_AES_ECHO();
        AsyncSend((int)aesBinaryMessageDefine.ENUM_CS_.CS_AES_ECHO, ecshoPacket);

        return 0;
    }

    public override int OnConnectFail()
    {
        LogManager.Instance.Write("OnConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int OnDisconnect()
    {
        LogManager.Instance.Write("OnDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}