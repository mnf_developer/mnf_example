﻿using MNF;
using MNF.Message;
using MNF_Common;

public class JsonClientSession : TCPSession<JsonMessageSerializer_, JsonMessageDeserializer_>
{
    public override int onConnectSuccess()
    {
        LogManager.Instance.Write("onConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var ecshoPacket = JsonMessageDefine.createEcho(10);
        //syncSend((int)JsonMessageDefine._ENUM_.ECHO, ref ecshoPacket);
        asyncSend((int)JsonMessageDefine._ENUM_.ECHO, ref ecshoPacket);

        return 0;
    }

    public override int onConnectFail()
    {
        LogManager.Instance.Write("onConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int onDisconnect()
    {
        LogManager.Instance.Write("onDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}
