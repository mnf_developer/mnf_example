﻿using MNF;
using MNF_Common;

public class CryptBinaryClientSession : TCPSession<CryptBinaryMessageSerializer, CryptBinaryMessageDeserializer>
{
    public override int OnConnectSuccess()
    {
        LogManager.Instance.Write("OnConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var ecshoPacket = new CryptBinaryMessageDefine.PACK_CS_CRYPT_ECHO();
        AsyncSend((int)CryptBinaryMessageDefine.ENUM_CS_.CS_CRYPT_ECHO, ecshoPacket);

        return 0;
    }

    public override int OnConnectFail()
    {
        LogManager.Instance.Write("OnConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int OnDisconnect()
    {
        LogManager.Instance.Write("OnDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}