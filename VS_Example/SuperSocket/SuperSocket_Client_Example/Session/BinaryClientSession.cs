﻿using MNF;
using MNF.Message;
using MNF_Common;

public class BinaryClientSession : TCPSession<BinaryMessageSerializer_, BinaryMessageDeserializer_>
{
    public override int onConnectSuccess()
    {
        LogManager.Instance.Write("onConnectSuccess : {0}:{1}", this.ToString(), this.GetType());

        var ecshoPacket = new BinaryMessageDefine.PACK_CS_ECHO();
        //syncSend((int)BinaryMessageDefine.ENUM_CS_.CS_ECHO, ref ecshoPacket);
        asyncSend((int)BinaryMessageDefine.ENUM_CS_.CS_ECHO, ref ecshoPacket);

        return 0;
    }

    public override int onConnectFail()
    {
        LogManager.Instance.Write("onConnectFail : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }

    public override int onDisconnect()
    {
        LogManager.Instance.Write("onDisconnect : {0}:{1}", this.ToString(), this.GetType());
        return 0;
    }
}