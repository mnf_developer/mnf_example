﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Security.Cryptography;
using MNF_Common;
using MNF;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            LogManager.Instance.SetLogWriter(new ConsoleLogWriter());
            if (LogManager.Instance.Init() == false)
                Console.WriteLine("LogWriter init failed");

            Console.WriteLine("Staring LookAround!");
            LookAround.Instance.Start("0", false);

            while (LookAround.Instance.IsFoundServer == false)
                Utility.Sleep(1000);

            Console.WriteLine("Found Server : {0}", LookAround.Instance.ServerIP);

            try
            {
                string customBinaryServerPort = "10000";
                string customJsonServerPort = "20000";
                string cryptBinaryServerPort = "10001";
                string cryptJsonServerPort = "20001";
                string aesBinaryServerPort = "10002";
                string aesJsonServerPort = "20002";

                if (TcpHelper.Instance.Start(false) == false)
                {
                    Console.WriteLine("TcpHelper.Instance.Start() failed");
                    return;
                }

                // custom
                if (TcpHelper.Instance.AsyncConnect<CustomBinaryClientSession, BinaryMessageDispatcher>(
                    LookAround.Instance.ServerIP, customBinaryServerPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}:{1}) failed", LookAround.Instance.ServerIP, customBinaryServerPort);
                    return;
                }

                if (TcpHelper.Instance.AsyncConnect<CustomJsonClientSession, JsonMessageDispatcher>(
                    LookAround.Instance.ServerIP, customJsonServerPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}:{1}) failed", LookAround.Instance.ServerIP, customJsonServerPort);
                    return;
                }

                // md5
                if (TcpHelper.Instance.AsyncConnect<CryptBinaryClientSession, CryptBinaryMessageDispatcher>(
                    LookAround.Instance.ServerIP, cryptBinaryServerPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}:{1}) failed", LookAround.Instance.ServerIP, cryptBinaryServerPort);
                    return;
                }

                if (TcpHelper.Instance.AsyncConnect<CryptJsonClientSession, CryptJsonMessageDispatcher>(
                    LookAround.Instance.ServerIP, cryptJsonServerPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}:{1}) failed", LookAround.Instance.ServerIP, cryptJsonServerPort);
                    return;
                }

                // aes
                if (TcpHelper.Instance.AsyncConnect<aesBinaryClientSession, aesBinaryMessageDispatcher>(
                    LookAround.Instance.ServerIP, aesBinaryServerPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}:{1}) failed", LookAround.Instance.ServerIP, aesBinaryServerPort);
                    return;
                }

                if (TcpHelper.Instance.AsyncConnect<aesJsonClientSession, aesJsonMessageDispatcher>(
                    LookAround.Instance.ServerIP, aesJsonServerPort) == null)
                {
                    Console.WriteLine("TcpHelper.Instance.AsyncConnect({0}:{1}) failed", LookAround.Instance.ServerIP, aesJsonServerPort);
                    return;
                }
            }
            catch (Exception e)
            {
                LogManager.Instance.WriteException(e, "run failed");
            }

            while (true)
            {
                TcpHelper.Instance.DipatchNetworkInterMessage();
                Thread.Sleep(10);
            }
        }
    }
}
