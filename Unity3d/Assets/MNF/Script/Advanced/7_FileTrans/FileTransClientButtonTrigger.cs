﻿using System;
using UnityEngine;
using UnityEngine.UI;
using MNF;

public class FileTransClientButtonTrigger : MonoBehaviour
{
	public InputField serverIP;
	public InputField serverPort;
    public RawImage rawImage;

	string loadFileName = "DefaultImage";
	TextAsset loadBinaryImage = null;

    public void Awake()
    {
		LogManager.Instance.SetLogWriter(new UnityLogWriter());
        if (LogManager.Instance.Init() == false)
            Debug.Log("LogWriter init failed");
	}

    void Release()
    {
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();        
    }

    void OnDestroy()
    {
        Release();
    }

	void OnApplicationQuit()
	{
        Release();
	}

    void Update()
	{
		TcpHelper.Instance.DipatchNetworkInterMessage();
	}

    public void OnStartClient()
    {
		LookAround.Instance.Start(serverPort.text, false);

		while (LookAround.Instance.IsFoundServer == false)
		{
			Utility.Sleep(1000);
		}

		serverIP.text = LookAround.Instance.ServerIP;
		serverPort.text = LookAround.Instance.ServerPort;

		if (TcpHelper.Instance.Start(false) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
			return;
		}

		if (TcpHelper.Instance.AsyncConnect<FileTransClientSession, FileTransClientMessageDispatcher>(
			serverIP.text, serverPort.text) == null)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.AsyncConnect({0}:{1}) failed",
				serverIP.text, serverPort.text);
			return;
		}

		LogManager.Instance.Write("Start Client Success");
	}

    public void OnLoadImage()
    {
		LogManager.Instance.Write("Start Load Image");

		loadBinaryImage = Resources.Load(loadFileName, typeof(TextAsset)) as TextAsset;

        var texture = new Texture2D(200, 100);
		texture.LoadImage(loadBinaryImage.bytes);
        rawImage.texture = texture;

        var text = rawImage.GetComponentInChildren<Text>();
        text.text = string.Format("w:{0} - h:{1} - s:{2}/kb",
            rawImage.texture.width, rawImage.texture.height, loadBinaryImage.bytes.Length / 1024);

		LogManager.Instance.Write("End Load Image");
	}
}
