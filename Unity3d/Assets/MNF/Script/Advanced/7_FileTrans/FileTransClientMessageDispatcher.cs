﻿using System;
using UnityEngine;
using UnityEngine.UI;
using MNF_Common;
using MNF;

public class FileTransClientMessageDispatcher : DefaultDispatchHelper<FileTransClientSession, FileTransMessageDefine, FileTransMessageDefine.ENUM_SC_>
{
    int onSC_FILE_TRANS_START(FileTransClientSession session, object message)
    {
        var fileTransStart = (FileTransMessageDefine.PACK_SC_FILE_TRANS_START)message;
        session.rawImageBytes = new byte[fileTransStart.fileSize];
        session.recvedSize = 0;

		var rawImage = GameObject.Find("RawImage").GetComponent<RawImage>();
		var text = rawImage.GetComponentInChildren<Text>();
        text.text = string.Format("Start File Trans : {0}", fileTransStart.fileName);
		
        return 0;
    }

    int onSC_FILE_TRANS_SEND(FileTransClientSession session, object message)
    {
		var fileTransSend = (FileTransMessageDefine.PACK_SC_FILE_TRANS_SEND)message;
        Buffer.BlockCopy(fileTransSend.binary, 0, session.rawImageBytes, session.recvedSize, fileTransSend.sendSize);
        session.recvedSize += fileTransSend.sendSize;

		var rawImage = GameObject.Find("RawImage").GetComponent<RawImage>();
		var text = rawImage.GetComponentInChildren<Text>();
        text.text = string.Format("Total Recved Size : {0}/kb", session.recvedSize / 1024);
		return 0;
	}

	int onSC_FILE_TRANS_END(FileTransClientSession session, object message)
	{
		var fileTransEnd = (FileTransMessageDefine.PACK_SC_FILE_TRANS_END)message;

		var rawImage = GameObject.Find("RawImage").GetComponent<RawImage>();
		var texture = new Texture2D(200, 100);
        texture.LoadImage(session.rawImageBytes);
		rawImage.texture = texture;

		var text = rawImage.GetComponentInChildren<Text>();
        text.text = string.Format("file:{0} - w:{1} - h:{2} - s:{3}/kb",
            fileTransEnd.fileName, rawImage.texture.width, rawImage.texture.height, session.rawImageBytes.Length / 1024);
		return 0;
	}
}