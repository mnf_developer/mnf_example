﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using MNF;
using MNF_Common;

public class FileTransServerButtonTrigger : MonoBehaviour
{
	public InputField serverIP;
	public InputField serverPort;
    public RawImage rawImage;
    public Button sendButton;

	string loadFileName = "FileTransImage";
	TextAsset loadBinaryImage = null;

    public void Awake()
    {
		LogManager.Instance.SetLogWriter(new UnityLogWriter());
		if (LogManager.Instance.Init() == false)
			Debug.Log("LogWriter init failed");
        
        sendButton.gameObject.SetActive(false);
	}

	void Release()
	{
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.StopAccept();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();
	}

	void OnDestroy()
	{
		Release();
	}

	void OnApplicationQuit()
	{
		Release();
	}

	void Update()
	{
		TcpHelper.Instance.DipatchNetworkInterMessage();
	}

    public void OnStartServer()
    {
		LookAround.Instance.Start(serverPort.text, true);

		while (LookAround.Instance.IsSetMyInfo == false)
		{
			Utility.Sleep(1000);
		}
		serverIP.text = LookAround.Instance.MyIP;

		if (TcpHelper.Instance.Start(false) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
			return;
		}

        if (TcpHelper.Instance.StartAccept<FileTransServerSession, FileTransServerMessageDispatcher>(
			serverIP.text, serverPort.text, 500) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.StartAccept<FileTransServerSession, FileTransServerMessageDispatcher>() failed");
			return;
		}
		sendButton.gameObject.SetActive(true);
		LogManager.Instance.Write("Start Server Success");
	}

    public void OnLoadImage()
    {
        if (loadBinaryImage != null)
            return;

		LogManager.Instance.Write("Start Load Image");

		loadBinaryImage = Resources.Load(loadFileName, typeof(TextAsset)) as TextAsset;

        var texture = new Texture2D(200, 100);
		texture.LoadImage(loadBinaryImage.bytes);
        rawImage.texture = texture;

        var text = rawImage.GetComponentInChildren<Text>();
        text.text = string.Format("w:{0} - h:{1} - s:{2}/kb",
            rawImage.texture.width, rawImage.texture.height, loadBinaryImage.bytes.Length / 1024);
	
        LogManager.Instance.Write("End Load Image");
	}

	public void OnSendImage()
	{
        StartCoroutine("RunSendImage");
	}

	IEnumerator RunSendImage()
	{
		var clientSessionEnumerator = TcpHelper.Instance.GetClientSessionEnumerator();
		while (clientSessionEnumerator.MoveNext())
		{
			var session = clientSessionEnumerator.Current.Value;

			// start
			var fileTransStart = new FileTransMessageDefine.PACK_SC_FILE_TRANS_START();
			fileTransStart.fileName = loadFileName;
			fileTransStart.fileSize = loadBinaryImage.bytes.Length;
			session.AsyncSend((int)FileTransMessageDefine.ENUM_SC_.SC_FILE_TRANS_START, fileTransStart);
			LogManager.Instance.Write("Start Send Image - file:{0} - size:{1}/kb", fileTransStart.fileName, fileTransStart.fileSize / 1024);

			var text = rawImage.GetComponentInChildren<Text>();

			// send
			int sentSize = 0;
			while (sentSize < loadBinaryImage.bytes.Length)
			{
				var fileTransSend = new FileTransMessageDefine.PACK_SC_FILE_TRANS_SEND();
				fileTransSend.sendSize = fileTransSend.binary.Length;
				if ((sentSize + fileTransSend.sendSize) > loadBinaryImage.bytes.Length)
					fileTransSend.sendSize = loadBinaryImage.bytes.Length - sentSize;

				Buffer.BlockCopy(loadBinaryImage.bytes, sentSize, fileTransSend.binary, 0, fileTransSend.sendSize);
				sentSize += fileTransSend.sendSize;

				session.AsyncSend((int)FileTransMessageDefine.ENUM_SC_.SC_FILE_TRANS_SEND, fileTransSend);

				text.text = string.Format("Sending Image - sentSize:{0}/kb", sentSize / 1024);
				yield return null;

                //Utility.Sleep(10000000);
			}

			// end
			var fileTransEnd = new FileTransMessageDefine.PACK_SC_FILE_TRANS_END();
			fileTransEnd.fileName = loadFileName;
			session.AsyncSend((int)FileTransMessageDefine.ENUM_SC_.SC_FILE_TRANS_END, fileTransEnd);
			LogManager.Instance.Write("End Send Image - file:{0}", fileTransEnd.fileName);
		}
	}
}
