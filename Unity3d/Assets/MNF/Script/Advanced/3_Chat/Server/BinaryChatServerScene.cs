﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using MNF;

public class BinaryChatServerScene : IScene
{
	public InputField serverIP;
	public InputField serverPort;
    public InputField inputChatMessageBox;
    public InputField recvMessageBox;
    public InputField inputNicknameBox;
    public Dropdown dropdownSessins;

    public BinaryChatServerScene()
    {
        SceneName = "BinaryChatServerScene";
    }

    void Awake()
    {
        LogManager.Instance.SetLogWriter(new UnityLogWriter());
        if (LogManager.Instance.Init() == false)
            Debug.Log("LogWriter init failed");

        MNF_ChatServer.Instance.BinaryChatServerScenePoint = this;
		LookAround.Instance.Start(serverPort.text, true);
	}

    void Start()
    {
        inputChatMessageBox.GetComponentInChildren<Text>().text = "";
        recvMessageBox.GetComponentInChildren<Text>().text = "";
    }
}