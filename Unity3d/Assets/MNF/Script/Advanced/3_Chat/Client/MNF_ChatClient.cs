﻿using UnityEngine;
using MNF;

public class MNF_ChatClient : UnitySingleton<MNF_ChatClient>
{
    public bool IsInit { get; private set; }
    public BinaryChatClientScene BinaryChatClientScenePoint { get; set; }

    public bool init()
    {
        if (IsInit == true)
            return true;

		while (LookAround.Instance.IsFoundServer == false)
		{
            Utility.Sleep(1000);
        }
        var responseEndPoints = LookAround.Instance.GetResponseEndPoint();
		foreach (var responseEndPoint in responseEndPoints)
		{
			if (responseEndPoint.isServer == true)
			{
				BinaryChatClientScenePoint.serverIP.text = responseEndPoint.ipEndPoint.Address.ToString();
				BinaryChatClientScenePoint.serverPort.text = responseEndPoint.ipEndPoint.Port.ToString();
				break;
			}
		}
		
		if (TcpHelper.Instance.Start(false) == false)
        {
            LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
            return false;
        }

        if (TcpHelper.Instance.AsyncConnect<ChatClientSession, ChatClientMessageDispatcher>(
            BinaryChatClientScenePoint.serverIP.text, BinaryChatClientScenePoint.serverPort.text) == null)
        {
            LogManager.Instance.WriteError("TcpHelper.Instance.AsyncConnect({0}:{1}) failed",
                BinaryChatClientScenePoint.serverIP.text, BinaryChatClientScenePoint.serverPort.text);
            return false;
        }

        IsInit = true;

        return IsInit;
    }

    void Update()
    {
        if (IsInit == false)
            return;

        TcpHelper.Instance.DipatchNetworkInterMessage();
    }

	void OnApplicationQuit()
	{
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();
	}
}