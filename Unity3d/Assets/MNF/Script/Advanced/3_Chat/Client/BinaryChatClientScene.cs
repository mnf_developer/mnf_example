﻿using UnityEngine;
using UnityEngine.UI;
using MNF;

public class BinaryChatClientScene : IScene
{
	public InputField serverIP;
	public InputField serverPort;
    public InputField inputChatMessageBox;
    public InputField recvMessageBox;
    public InputField inputNicknameBox;

    public BinaryChatClientScene()
    {
        SceneName = "BinaryChatClientScene";
    }

    void Awake()
    {
        LogManager.Instance.SetLogWriter(new UnityLogWriter());
        if (LogManager.Instance.Init() == false)
            Debug.Log("LogWriter init failed");

        MNF_ChatClient.Instance.BinaryChatClientScenePoint = this;
		LookAround.Instance.Start(serverPort.text, false);
	}

    void Start()
    {
        inputChatMessageBox.GetComponentInChildren<Text>().text = "";
        recvMessageBox.GetComponentInChildren<Text>().text = "";
    }
}