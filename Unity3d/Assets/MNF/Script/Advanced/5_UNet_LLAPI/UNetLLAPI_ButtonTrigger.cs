﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using MNF;

public class UdpMessageDefine
{
    public enum MESSAGES
    {
        START,
        SPAWN_OBJECT,
    }

    [Serializable]
    public class PACK_START
    {
    }

    [Serializable]
    public class PACK_SPAWN_OBJECT
    {
        public Vector3 position;
        public Quaternion rotation;
    }
}

public class UNetLLAPI_ButtonTrigger : P2PController
{
    public InputField inputFieldIP;
    public InputField inputFieldPort;

    public Rigidbody rocketInstance;
    public float lastInpuDelta;

    void Awake()
    {
        LogManager.Instance.SetLogWriter(new UnityLogWriter(), (int)ENUM_LOG_TYPE.LOG_TYPE_ALL);
        if (LogManager.Instance.Init() == false)
            Debug.Log("LogWriter init failed");
    }

    public void OnServer()
	{
        StartServer(inputFieldPort.text);
    }

    public void OnClient()
    {
        StartClient(inputFieldPort.text);
    }

    protected override void OnStartComplete(string myIP, int myPort)
    {
        inputFieldIP.text = myIP;
    }

    protected override void Update()
    {
        base.Update();

        lastInpuDelta += Time.deltaTime;

        if ((Input.GetMouseButton(0) == true) || (Input.touchCount > 0))
            BroadcastSpawn();
    }

    void OnGUI()
    {
        if (IsInit == true)
        {
            var connectionEnum = GetConnectList();
            while (connectionEnum.MoveNext())
            {
                var connectionInfo = connectionEnum.Current.Value;
                GUILayout.Label(string.Format("IP:{0} Port:{1} ConId:{2}",
                                              connectionInfo.IP, connectionInfo.Port, connectionInfo.ConnectionId));
            }
        }
    }

    void BroadcastSpawn()
    {
        if (IsInit == false)
            return;
        
        if (lastInpuDelta < 0.5f)
            return;

        lastInpuDelta = 0;

        Vector3 screenPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5);
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
        worldPos.y = Camera.main.transform.position.y;

        var spawnObject = new UdpMessageDefine.PACK_SPAWN_OBJECT();
        spawnObject.position = worldPos;
        spawnObject.rotation = new Quaternion();
        Broadcast(QosType.Reliable, (int)UdpMessageDefine.MESSAGES.SPAWN_OBJECT, spawnObject);
    }

    protected override bool MessageDispatcher(int hostId, int connectionId, int channelId, int messageId, object messageData)
    {
        LogManager.Instance.Write("hostId:{0} connectionId:{1} channelId:{2} messageId:{3}", hostId, connectionId, channelId, messageId);

        switch (messageId)
        {
            case (int)UdpMessageDefine.MESSAGES.START:
                {
                }
                break;

            case (int)UdpMessageDefine.MESSAGES.SPAWN_OBJECT:
                {
                    var spawn = (UdpMessageDefine.PACK_SPAWN_OBJECT)messageData;
                    Instantiate(rocketInstance, spawn.position, spawn.rotation);
                }
                break;
        }
        return true;
    }

    void OnApplicationQuit()
    {
        Debug.Log("Application ending after " + Time.time + " seconds");
        LookAround.Instance.Stop();
        LogManager.Instance.Release();
        Release();
    }
}