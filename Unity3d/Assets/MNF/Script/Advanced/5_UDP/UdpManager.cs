﻿using System;
using UnityEngine;
using MNF;

public class UdpManager : UnitySingleton<UdpManager>
{
    public bool IsInit { get; set; }
    public bool IsServer { get; set; }
    public UdpSession Session { get; set; }
	public UdpScene UdpScenePoint { get; set; }

    public UdpManager()
    {
        LogManager.Instance.SetLogWriter(new UnityLogWriter());
        if (LogManager.Instance.Init() == false)
            Debug.Log("LogManager.Instance.init() success");
        else
            Debug.Log("LogManager.Instance.init() failed");
    }

    void Update()
    {
        if (IsInit == false)
			return;

        UdpHelper.Instance.DipatchMessage();
    }

	public bool init()
    {
        if (IsInit == true)
            return true;

        UdpScenePoint.inputFieldIP.text = LookAround.Instance.MyIP;
        LookAround.Instance.Stop();

        try
        {
            IsInit = UdpHelper.Instance.Run(isRunThread: false);
			Session = UdpHelper.Instance.Create(
				dispatchExporterType:typeof(UdpMessageDispatcher)
				, bindIPString:UdpScenePoint.inputFieldIP.text
				, bindPortString:UdpScenePoint.inputFieldPort.text); 
        }
        catch (Exception e)
        {
            LogManager.Instance.Write(e.ToString());
            IsInit = false;
        }

        LogManager.Instance.Write("init UdpServerManager result: {0}, {1}:{2}", 
		                          IsInit, UdpScenePoint.inputFieldIP.text, UdpScenePoint.inputFieldPort.text);

        return IsInit;
    }

	internal bool exchangePeerInfo()
	{
		return true;
	}

	void OnApplicationQuit()
	{
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		UdpHelper.Instance.Release();
		LogManager.Instance.Release();
	}
}