﻿using UnityEngine;
using UnityEngine.UI;
using MNF;

public class UdpScene : IScene
{
	public InputField inputFieldIP;
	public InputField inputFieldPort;

	public Rigidbody rocketInstance;
    public float lastInpuDelta = 0.0f;
    
    public UdpScene()
    {
        SceneName = "UdpScene";
    }

    void Awake()
    {
        UdpManager.Instance.UdpScenePoint = this;
    }

    void Start()
    {
    }

    void Update()
    {
		if (UdpManager.Instance.IsInit == false)
			return;
		
        lastInpuDelta += Time.deltaTime;

        if (Input.GetMouseButton(0) == true)
        {
			broadcastSpawn();
		}
        else if (Input.touchCount > 0)
        {
			broadcastSpawn();
        }
    }

	void broadcastSpawn()
	{
		if (lastInpuDelta < 0.5f)
			return;

		lastInpuDelta = 0;

		Vector3 screenPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5);
		Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
		worldPos.y = Camera.main.transform.position.y;

		var spawnObject = new UdpMessageDefine.PACK_UDP_SPAWN_OBJECT();
		spawnObject.position = worldPos;
		spawnObject.rotation = new Quaternion();

        var otherDevices = LookAround.Instance.GetResponseEndPoint();
        UdpManager.Instance.Session.reliableSendTo(otherDevices, ref spawnObject, isSendMe:true);

        foreach (var device in otherDevices)
        {
            LogManager.Instance.Write("send to {0}:{1}", device.isServer, device.ipEndPoint);
        }
    }
}