﻿using UnityEngine;
using MNF;

public class UdpButtonTrigger : MonoBehaviour
{
    public void OnSearch()
    {
        LookAround.Instance.Start(UdpManager.Instance.UdpScenePoint.inputFieldPort.text, false);
        if (LookAround.Instance.IsRunning == true)
			LogManager.Instance.Write("LookAround start() success");
        else
			LogManager.Instance.Write("LookAround start() failed");
    }

    public void OnStart()
	{
		if (UdpManager.Instance.init() == true)
			LogManager.Instance.Write("UdpManager init() success");
        else
			LogManager.Instance.Write("UdpManager init() failed");
    }
}