﻿using MNF;
using UnityEngine;

public class UdpMessageDispatcher : DefaultDispatchHelper<UdpSession, UdpMessageDefine, UdpMessageDefine.ENUM_UDP_MESSAGES>
{
    int onUDP_SPAWN_OBJECT(UdpSession session, object message)
    {
        var udpMessageInfo = (UdpMessageInfo)message;
		LogManager.Instance.Write("{0}, {1}, {2}", udpMessageInfo.RemoteIP, udpMessageInfo.UdpMessage, udpMessageInfo.IsSendMe);

        var spawn = (UdpMessageDefine.PACK_UDP_SPAWN_OBJECT)udpMessageInfo.UdpMessage;
        var rocketPrefab = UdpManager.Instance.UdpScenePoint.rocketInstance;
        MonoBehaviour.Instantiate(rocketPrefab, spawn.position, spawn.rotation);

        return 0;
    }

    int onUDP_SHOT(UdpSession session, object message)
    {
        //var udpMessageInfo = (UdpMessageInfo)message;
        //var shot = (UdpMessageDefine.PACK_UDP_SHOT)udpMessageInfo.UdpMessage;
		//LogManager.Instance.Write("{0}, {1}, {2}", udpMessageInfo.RemoteIP, udpMessageInfo.UdpMessage, udpMessageInfo.IsSendMe);
		
        //var rocketPrefab = Udp_Manager.Instance.UdpScenePoint.rocketInstance;
        //var rocketInstance = Instantiate(rocketPrefab, spawn.position, spawn.rotation) as Rigidbody;
        //rocketInstance.AddForce(barrelEnd.forward * 5000);

        return 0;
    }
}
