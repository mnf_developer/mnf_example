﻿using System.Runtime.InteropServices;
using MNF;
using UnityEngine;

public partial class UdpMessageDefine
{
    public enum ENUM_UDP_MESSAGES
    {
		UDP_SPAWN_OBJECT,
        UDP_SHOT,
    }
    
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public class PACK_UDP_SPAWN_OBJECT : UdpMessageHeader
    {
        public Vector3 position;
        public Quaternion rotation;

        public PACK_UDP_SPAWN_OBJECT()
        {
            messageSize = (short)MarshalHelper.GetManagedDataSize(typeof(PACK_UDP_SPAWN_OBJECT));
            messageID = (ushort)ENUM_UDP_MESSAGES.UDP_SPAWN_OBJECT;

            position = new Vector3();
            rotation = new Quaternion();
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public class PACK_UDP_SHOT : UdpMessageHeader
    {
        public Vector3 position;
        public Vector3 rotation;

        public PACK_UDP_SHOT()
        {
            messageSize = (short)MarshalHelper.GetManagedDataSize(typeof(PACK_UDP_SHOT));
            messageID = (ushort)ENUM_UDP_MESSAGES.UDP_SHOT;
        }
    }
}