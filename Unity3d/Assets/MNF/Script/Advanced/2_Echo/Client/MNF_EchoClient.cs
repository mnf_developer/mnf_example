﻿using System;
using UnityEngine;
using MNF;

public class MNF_EchoClient : UnitySingleton<MNF_EchoClient>
{
    public bool IsInit { get; private set; }
    public EchoClientScene EchoClientScenePoint { get; set; }

    public bool init()
    {
        if (IsInit == true)
            return true;

		while (LookAround.Instance.IsFoundServer == false)
		{
            Utility.Sleep(1000);
        }
        var responseEndPoints = LookAround.Instance.GetResponseEndPoint();
		foreach (var responseEndPoint in responseEndPoints)
		{
			if (responseEndPoint.isServer == true)
			{
                EchoClientScenePoint.serverIP.text = responseEndPoint.ipEndPoint.Address.ToString();
				break;
			}
		}

        if (TcpHelper.Instance.Start(false) == false)
        {
            LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
            return false;
        }

        if (TcpHelper.Instance.AsyncConnect<EchoClientBinarySession, EchoClientBinaryMessageDispatcher>(
            EchoClientScenePoint.serverIP.text, EchoClientScenePoint.binaryServerPort.text) == null)
        {
            LogManager.Instance.WriteError("TcpHelper.Instance.AsyncConnect({0}:{1}) failed",
                EchoClientScenePoint.serverIP.text, EchoClientScenePoint.binaryServerPort.text);
            return false;
        }

        if (TcpHelper.Instance.AsyncConnect<EchoClientJsonSession, EchoClientJsonMessageDispatcher>(
            EchoClientScenePoint.serverIP.text, EchoClientScenePoint.jsonServerPort.text) == null)
        {
            LogManager.Instance.WriteError("TcpHelper.Instance.AsyncConnect({0}:{1}) failed",
                EchoClientScenePoint.serverIP.text, EchoClientScenePoint.jsonServerPort.text);
            return false;
        }

        IsInit = true;

        return IsInit;
    }

    void Update()
    {
        if (IsInit == false)
            return;

        TcpHelper.Instance.DipatchNetworkInterMessage();
    }

	void OnApplicationQuit()
	{
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();
	}
}