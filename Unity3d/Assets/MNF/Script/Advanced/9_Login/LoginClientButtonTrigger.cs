﻿using UnityEngine;
using UnityEngine.UI;
using MNF;
using MNF_Common;

public class LoginClientButtonTrigger : MonoBehaviour
{
	public InputField serverIP;
	public InputField serverPort;

	public InputField id;
	public InputField pwd;
	public InputField result;

    public void Awake()
    {
		LogManager.Instance.SetLogWriter(new UnityLogWriter());
		if (LogManager.Instance.Init() == false)
			Debug.Log("LogWriter init failed");
	}

    void Release()
    {
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();
    }

    void OnDestroy()
    {
        Release();
    }

	void OnApplicationQuit()
	{
        Release();
	}

    void Update()
	{
		TcpHelper.Instance.DipatchNetworkInterMessage();
	}

    public void OnStartClient()
    {
		LookAround.Instance.Start(serverPort.text, false);

		while (LookAround.Instance.IsFoundServer == false)
			Utility.Sleep(1000);
		
        LookAround.Instance.Stop();

		serverIP.text = LookAround.Instance.ServerIP;
		serverPort.text = LookAround.Instance.ServerPort;

		if (TcpHelper.Instance.Start(false) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
			return;
		}

		if (TcpHelper.Instance.AsyncConnect<LoginClientSession, LoginClientMessageDispatcher>(
			serverIP.text, serverPort.text) == null)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.AsyncConnect({0}:{1}) failed",
				serverIP.text, serverPort.text);
			return;
		}

		LogManager.Instance.Write("Start Client Success");
	}

    public void OnRegist()
    {
        if (id.text == "" || pwd.text == "")
            return;
        
        var regist = new LoginMessageDefine.PACK_CS_REGIST();
        regist.id = id.text;
        regist.pwd = pwd.text;
        
        var session = TcpHelper.Instance.GetFirstClient<LoginClientSession>();
        session.AsyncSend((int)LoginMessageDefine.ENUM_CS_.CS_REGIST, regist);
	}

    public void OnLogin()
    {
		if (id.text == "" || pwd.text == "")
			return;
        
		var login = new LoginMessageDefine.PACK_CS_LOGIN();
		login.id = id.text;
		login.pwd = pwd.text;

		var session = TcpHelper.Instance.GetFirstClient<LoginClientSession>();
		session.AsyncSend((int)LoginMessageDefine.ENUM_CS_.CS_LOGIN, login);
    }
}
