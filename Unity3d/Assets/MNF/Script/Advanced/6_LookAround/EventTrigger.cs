﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using MNF;
using MNF_Common;

public class EventTrigger : MonoBehaviour
{
    public Dropdown dropdown;
    public Toggle toggle;
    public InputField inputField;
	public Text myInfo;
	public InputField recvMessage;
    bool isRunning = true;

    public void OnLookAroundStart()
    {
		LogManager.Instance.SetLogWriter(new UnityLogWriter());
		if (LogManager.Instance.Init() == false)
			Debug.Log("LogWriter init failed");

        LookAround.Instance.Start(inputField.text, toggle.isOn);
        StartCoroutine("UpdateDeviceList");
	}

	public void OnSendMessage()
	{
        var jsonEcho = new JsonMessageDefine.PACK_CS_JSON_ECHO();
        jsonEcho.sandwiches = Sandwich.CreateSandwichList(1);
        LookAround.Instance.SendMessage(100, jsonEcho);
	}

    IEnumerator UpdateDeviceList()
    {
        while (isRunning == true)
        {
			if (LookAround.Instance.IsSetMyInfo)
			{
                myInfo.text = string.Format("My Info : IP:{0} - Port:{1}", LookAround.Instance.MyIP, LookAround.Instance.MyPort);
			}

            dropdown.options.Clear();

            var responseEndPoints = LookAround.Instance.GetResponseEndPoint(3);
            foreach (var responseEndPoint in responseEndPoints)
            {
                var optionData = new UnityEngine.UI.Dropdown.OptionData(responseEndPoint.ToString());
                dropdown.options.Add(optionData);
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    void Update()
    {
        var reponseMessage = LookAround.Instance.PopReponseMessage();
        if (reponseMessage != null)
        {
			recvMessage.text = string.Format("{0} {1} {2} {3}",
				reponseMessage.ID, reponseMessage.IsMine, reponseMessage.Type, reponseMessage.Message);    
        }
    }

    void OnApplicationQuit()
    {
        isRunning = false;
        Debug.Log("Application ending after " + Time.time + " seconds");
        LookAround.Instance.Stop();
        LogManager.Instance.Release();
    }
}