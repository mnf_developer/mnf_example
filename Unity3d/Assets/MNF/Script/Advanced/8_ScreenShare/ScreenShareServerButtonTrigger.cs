﻿using System;
using UnityEngine;
using UnityEngine.UI;
using MNF;

public class ScreenShareServerButtonTrigger : MonoBehaviour
{
	public InputField serverIP;
	public InputField serverPort;
	public InputField captureFrame;
    public MonoBehaviour screenCapture;

    public void Awake()
    {
		LogManager.Instance.SetLogWriter(new UnityLogWriter());
		if (LogManager.Instance.Init() == false)
			Debug.Log("LogWriter init failed");

        captureFrame.text = Convert.ToString(((ScreenCapture)screenCapture).captureFrame);
	}

	void Release()
	{
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.StopAccept();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();
	}

	void OnDestroy()
	{
		Release();
	}

	void OnApplicationQuit()
	{
		Release();
	}

	void Update()
	{
		TcpHelper.Instance.DipatchNetworkInterMessage();
	}

    public void OnStartServer()
    {
		LookAround.Instance.Start(serverPort.text, true);

		while (LookAround.Instance.IsSetMyInfo == false)
		{
			Utility.Sleep(1000);
		}
		serverIP.text = LookAround.Instance.MyIP;

		if (TcpHelper.Instance.Start(false) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
			return;
		}

        if (TcpHelper.Instance.StartAccept<ScreenShareServerSession, ScreenShareServerMessageDispatcher>(
			serverIP.text, serverPort.text, 500) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.StartAccept<ScreenShareServerSession, ScreenShareServerMessageDispatcher>() failed");
			return;
		}
		LogManager.Instance.Write("Start Server Success");
	}

    public void OnCaptureFrame()
    {
        ((ScreenCapture)screenCapture).captureFrame = Convert.ToInt32(captureFrame.text);
    }
}
