﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MNF;
using MNF_Common;

public class ScreenCapture : MonoBehaviour
{
    public int captureFrame = 15;
	public RawImage rawImage;
	public Text text;
	public int captureWidth = 400;
    public int captureHeight = 400;
    //Texture2D capturedTexture;
	Texture2D outputTexture;
    //Rect readPixels;

	public float elapsedTime;

	void Start()
    {
		//capturedTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		//readPixels = new Rect(0, 0, Screen.width, Screen.height);
		outputTexture = new Texture2D(Screen.width, Screen.height);
    }

    void LateUpdate()
    {
		elapsedTime += Time.deltaTime;
		if (elapsedTime >= (1.0f / captureFrame))
		{
			StartCoroutine(Capture());
			elapsedTime = 0.0f;
		}
    }

	IEnumerator Capture()
	{
		//Wait for graphics to render
		yield return new WaitForEndOfFrame();

		RenderTexture rt = new RenderTexture(Screen.width, Screen.height, 24);
		Texture2D screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

		//Render from all!
		foreach (Camera cam in Camera.allCameras)
		{
			cam.targetTexture = rt;
			cam.Render();
			cam.targetTexture = null;
		}

		RenderTexture.active = rt;
		screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		Camera.main.targetTexture = null;
		RenderTexture.active = null; //Added to avoid errors
		Destroy(rt);

		//Split the process up
		yield return 0;

		// Encode texture into PNG
        byte[] jpgBytes = screenShot.EncodeToJPG();
		byte[] pngBytes = screenShot.EncodeToPNG();
		byte[] rawBytes = screenShot.GetRawTextureData();

		outputTexture.LoadImage(jpgBytes);
		rawImage.texture = outputTexture;

        text.text = string.Format("JPG:{0}, PNG:{1}, Raw:{2}", 
            jpgBytes.Length, pngBytes.Length, rawBytes.Length);

        var screenShare = new ScreenShareMessageDefine.PACK_SC_SCREEN_SHARE();
        Buffer.BlockCopy(jpgBytes, 0, screenShare.binary, 0, jpgBytes.Length);
        screenShare.sendSize = jpgBytes.Length;

		var clientSessionEnumerator = TcpHelper.Instance.GetClientSessionEnumerator();
		while (clientSessionEnumerator.MoveNext())
		{
            clientSessionEnumerator.Current.Value.AsyncSend(
                (int)ScreenShareMessageDefine.ENUM_SC_.SC_SCREEN_SHARE, screenShare);
		}

		//// We should only read the screen after all rendering is complete
		//yield return new WaitForEndOfFrame();

		//// Read screen contents into the texture
		//capturedTexture.ReadPixels(readPixels, 0, 0, false);
		//capturedTexture.Apply();

		//// Encode texture into PNG
		//byte[] jpgBytes = capturedTexture.EncodeToJPG();
		//byte[] pngBytes = capturedTexture.EncodeToPNG();
		//byte[] rawBytes = capturedTexture.GetRawTextureData();

		//outputTexture.LoadImage(jpgBytes);
		//rawImage.texture = outputTexture;

		//text.text = string.Format("JPG:{0}, PNG:{1}, Raw:{2}", 
		//  jpgBytes.Length, pngBytes.Length, rawBytes.Length);

		//  var screenShare = new ScreenShareMessageDefine.PACK_SC_SCREEN_SHARE();
		//  Buffer.BlockCopy(jpgBytes, 0, screenShare.binary, 0, jpgBytes.Length);
        //  screenShare.sendSize = jpgBytes.Length;

		//var clientSessionEnumerator = TcpHelper.Instance.GetClientSessionEnumerator();
		//while (clientSessionEnumerator.MoveNext())
		//{
		//  clientSessionEnumerator.Current.Value.AsyncSend(
		//      (int)ScreenShareMessageDefine.ENUM_SC_.SC_SCREEN_SHARE, screenShare);
		//}
	}
}