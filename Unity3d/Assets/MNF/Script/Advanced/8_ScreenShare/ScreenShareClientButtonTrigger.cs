﻿using UnityEngine;
using UnityEngine.UI;
using MNF;

public class ScreenShareClientButtonTrigger : MonoBehaviour
{
	public InputField serverIP;
	public InputField serverPort;

    public void Awake()
    {
		LogManager.Instance.SetLogWriter(new UnityLogWriter());
		if (LogManager.Instance.Init() == false)
			Debug.Log("LogWriter init failed");
	}

    void Release()
    {
		Debug.Log("Application ending after " + Time.time + " seconds");
		LookAround.Instance.Stop();
		TcpHelper.Instance.Stop();
		LogManager.Instance.Release();        
    }

    void OnDestroy()
    {
        Release();
    }

	void OnApplicationQuit()
	{
        Release();
	}

    void Update()
	{
		TcpHelper.Instance.DipatchNetworkInterMessage();
	}

    public void OnStartClient()
    {
		LookAround.Instance.Start(serverPort.text, false);

		while (LookAround.Instance.IsFoundServer == false)
		{
			Utility.Sleep(1000);
		}

		serverIP.text = LookAround.Instance.ServerIP;
		serverPort.text = LookAround.Instance.ServerPort;

		if (TcpHelper.Instance.Start(false) == false)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.run() failed");
			return;
		}

		if (TcpHelper.Instance.AsyncConnect<ScreenShareClientSession, ScreenShareClientMessageDispatcher>(
			serverIP.text, serverPort.text) == null)
		{
			LogManager.Instance.WriteError("TcpHelper.Instance.AsyncConnect({0}:{1}) failed",
				serverIP.text, serverPort.text);
			return;
		}

		LogManager.Instance.Write("Start Client Success");
	}
}
