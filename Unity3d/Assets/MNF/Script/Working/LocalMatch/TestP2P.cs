﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TestP2P : MonoBehaviour {

	/* 
     *  namespace UnityEngine.Networking
        {
            public enum QosType
            {
                Unreliable,
                UnreliableFragmented,
                UnreliableSequenced,
                Reliable,
                ReliableFragmented,
                ReliableSequenced,
                StateUpdate,
                ReliableStateUpdate,
                AllCostDelivery
            }
        }
     */

	void InitNetworkTransport() {
		// Initializing the Transport Layer with no arguments (default settings)
		NetworkTransport.Init();

		ConnectionConfig config = new ConnectionConfig();
		int myReiliableChannelId = config.AddChannel(QosType.Reliable);
		int myUnreliableChannelId = config.AddChannel(QosType.Unreliable);

        HostTopology topology = new HostTopology(config, 10);

        {
			int hostId = NetworkTransport.AddHost(topology, 8888);
			byte error = 0;
			var connectionId = NetworkTransport.Connect(hostId, "127.0.0.1", 9999, 0, out error);
            Debug.Log(string.Format("8888 hostId:{0} connectionId:{1}", hostId, connectionId));
		}
        {
			int hostId = NetworkTransport.AddHost(topology, 9999);
			byte error = 0;
			var connectionId = NetworkTransport.Connect(hostId, "127.0.0.1", 8888, 0, out error);
			Debug.Log(string.Format("9999 hostId:{0} connectionId:{1}", hostId, connectionId));
		}


        // disconnect
		//NetworkTransport.Disconnect(hostId, connectionId, out error);

        // send
		//NetworkTransport.Send(hostId, connectionId, myReiliableChannelId, buffer, bufferLength, out error);

  //      // recv
		//NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);

        //// recv
        //NetworkTransport.ReceiveFromHost(recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
	}

	// Use this for initialization
	void Start () {
        InitNetworkTransport();
	}
	
	// Update is called once per frame
	void Update () {
		int recHostId;
		int connectionId;
		int channelId;
		byte[] recBuffer = new byte[1024];
		int bufferSize = 1024;
		int dataSize;
		byte error;
		NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
		switch (recData)
		{
			case NetworkEventType.Nothing:
				break;
			case NetworkEventType.ConnectEvent:
                Debug.Log(string.Format("recData:{0} recHostId:{1} connectionId:{2} channelId:{3} dataSize:{4} error:{5}",
                                        recData, recHostId, connectionId, channelId, dataSize, error));
				break;
			case NetworkEventType.DataEvent:
				Debug.Log(string.Format("recData:{0} recHostId:{1} connectionId:{2} channelId:{3} dataSize:{4} error:{5}",
										recData, recHostId, connectionId, channelId, dataSize, error));
				break;
			case NetworkEventType.DisconnectEvent:
				Debug.Log(string.Format("recData:{0} recHostId:{1} connectionId:{2} channelId:{3} dataSize:{4} error:{5}",
										recData, recHostId, connectionId, channelId, dataSize, error));
				break;
		}
	}
}
