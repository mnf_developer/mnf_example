﻿//using System;
//using System.IO;
//using System.Collections;
//using System.Collections.Generic;
//using System.Net;
//using System.Net.Sockets;
//using System.Net.Security;
//using System.Security.Authentication;
//using System.Text;
//using System.Security.Cryptography.X509Certificates;
//using UnityEngine;

//namespace SuperSocket.ClientEngine
//{
//	/// <summary>
//	/// Security options
//	/// </summary>
//	public class SecurityOption
//	{
//		/// <summary>
//		/// The SslProtocols want to be enabled
//		/// </summary>
//		public SslProtocols EnabledSslProtocols { get; set; }

//		/// <summary>
//		/// Client X509 certificates
//		/// </summary>
//		public X509CertificateCollection Certificates { get; set; }

//		/// <summary>
//		/// Whether allow untrusted certificate
//		/// </summary>
//		public bool AllowUnstrustedCertificate { get; set; }

//		/// <summary>
//		/// Whether allow the certificate whose name doesn't match current remote endpoint's host name
//		/// </summary>
//		public bool AllowNameMismatchCertificate { get; set; }

//		public SecurityOption()
//			: this(GetDefaultProtocol(), new X509CertificateCollection())
//		{

//		}

//		public SecurityOption(SslProtocols enabledSslProtocols)
//			: this(enabledSslProtocols, new X509CertificateCollection())
//		{

//		}

//		public SecurityOption(SslProtocols enabledSslProtocols, X509Certificate2 certificate)
//			: this(enabledSslProtocols, new X509CertificateCollection(new X509Certificate2[] { certificate }))
//		{

//		}

//		public SecurityOption(SslProtocols enabledSslProtocols, X509CertificateCollection certificates)
//		{
//			EnabledSslProtocols = enabledSslProtocols;
//			Certificates = certificates;
//		}

//		private static SslProtocols GetDefaultProtocol()
//		{
//#if NETSTANDARD
//            return SslProtocols.Tls11 | SslProtocols.Tls12;
//#else
//			return SslProtocols.Default;
//#endif
//		}
//	}
//}

//public class LoginServer : MonoBehaviour {

//	// Use this for initialization
//	void Start () {
		
//	}
	
//	// Update is called once per frame
//	void Update () {
		
//	}
//}

//public enum HostType
//{
//	HostType_Server,
//	HostType_Client,
//}

//class AsyncSSLIOResult
//{
//    public AsyncSSLIOResult(Socket socket, SslStream sslStream, HostType hostType)
//    {
//        this.socket = socket;
//        this.sslStream = sslStream;
//        this.hostType = hostType;
//    }

//	public Socket socket;
//    public SslStream sslStream;
//    public HostType hostType;
//}

//class SslRef
//{
//	public X509Certificate2Collection Certificates { get; set; }

//    public bool InitServer(string certificateFile, string password, Socket socket)
//    {
//        var certificate = new X509Certificate2(certificateFile, password);
//        Certificates = new X509Certificate2Collection();
//        Certificates.Add(certificate);

//		bool clientAuthenticate = false;
//        bool checkRevocation = false;
//		SslStream ssl = new SslStream(new NetworkStream(socket));
//        ssl.BeginAuthenticateAsServer(
//            certificate,
//            clientAuthenticate,
//            SslProtocols.Default,
//            checkRevocation,
//            new AsyncCallback(SslAuthenticateCallback),
//            new AsyncSSLIOResult(socket, ssl, HostType.HostType_Server));

//		return true;
//    }

//	void SslAuthenticateCallback(IAsyncResult ar)
//	{
//		if (!Disposed)
//		{

//			BaseSocketConnection connection = null;
//			SslStream stream = null;
//			bool completed = false;

//			try
//			{

//				AuthenticateCallbackData callbackData = (AuthenticateCallbackData)ar.AsyncState;

//				connection = callbackData.Connection;
//				stream = callbackData.Stream;

//				if (connection.Active)
//				{

//					if (callbackData.HostType == HostType.htClient)
//					{
//						stream.EndAuthenticateAsClient(ar);
//					}
//					else
//					{
//						stream.EndAuthenticateAsServer(ar);
//					}

//					if ((stream.IsSigned && stream.IsEncrypted))
//					{
//						completed = true;
//					}

//					callbackData = null;
//					connection.Stream = stream;

//					if (completed)
//					{
//						connection.EventProcessing = EventProcessing.epUser;
//						FireOnConnected(connection);
//					}
//					else
//					{
//						FireOnException(connection, new SSLAuthenticationException("Ssl authenticate is not signed or not encrypted."));
//					}

//				}

//			}
//			catch (Exception ex)
//			{
//				FireOnException(connection, ex);
//			}

//		}

//	}

//	public bool InitClient(string certificateFile)
//	{
//		return true;
//	}
//}


//namespace Examples.System.Net
//{
//	public sealed class SslTcpServer
//	{
//		static X509Certificate serverCertificate = null;
//		// The certificate parameter specifies the name of the file 
//		// containing the machine certificate.
//		public static void RunServer(string certificate)
//		{
//			serverCertificate = X509Certificate2.CreateFromCertFile(certificate);
//			// Create a TCP/IP (IPv4) socket and listen for incoming connections.
//			TcpListener listener = new TcpListener(IPAddress.Any, 8080);
//			listener.Start();
//			while (true)
//			{
//				Console.WriteLine("Waiting for a client to connect...");
//				// Application blocks while waiting for an incoming connection.
//				// Type CNTL-C to terminate the server.
//				TcpClient client = listener.AcceptTcpClient();
//				ProcessClient(client);
//			}
//		}
//		static void ProcessClient(TcpClient client)
//		{
//			// A client has connected. Create the 
//			// SslStream using the client's network stream.
//			SslStream sslStream = new SslStream(
//				client.GetStream(), false);
//			// Authenticate the server but don't require the client to authenticate.
//			try
//			{
//				sslStream.AuthenticateAsServer(serverCertificate,
//					false, SslProtocols.Tls, true);
//				// Display the properties and settings for the authenticated stream.
//				DisplaySecurityLevel(sslStream);
//				DisplaySecurityServices(sslStream);
//				DisplayCertificateInformation(sslStream);
//				DisplayStreamProperties(sslStream);

//				// Set timeouts for the read and write to 5 seconds.
//				sslStream.ReadTimeout = 5000;
//				sslStream.WriteTimeout = 5000;
//				// Read a message from the client.   
//				Console.WriteLine("Waiting for client message...");
//				string messageData = ReadMessage(sslStream);
//				Console.WriteLine("Received: {0}", messageData);

//				// Write a message to the client.
//				byte[] message = Encoding.UTF8.GetBytes("Hello from the server.<EOF>");
//				Console.WriteLine("Sending hello message.");
//				sslStream.Write(message);
//			}
//			catch (AuthenticationException e)
//			{
//				Console.WriteLine("Exception: {0}", e.Message);
//				if (e.InnerException != null)
//				{
//					Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
//				}
//				Console.WriteLine("Authentication failed - closing the connection.");
//				sslStream.Close();
//				client.Close();
//				return;
//			}
//			finally
//			{
//				// The client stream will be closed with the sslStream
//				// because we specified this behavior when creating
//				// the sslStream.
//				sslStream.Close();
//				client.Close();
//			}
//		}
//		static string ReadMessage(SslStream sslStream)
//		{
//			// Read the  message sent by the client.
//			// The client signals the end of the message using the
//			// "<EOF>" marker.
//			byte[] buffer = new byte[2048];
//			StringBuilder messageData = new StringBuilder();
//			int bytes = -1;
//			do
//			{
//				// Read the client's test message.
//				bytes = sslStream.Read(buffer, 0, buffer.Length);

//				// Use Decoder class to convert from bytes to UTF8
//				// in case a character spans two buffers.
//				Decoder decoder = Encoding.UTF8.GetDecoder();
//				char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
//				decoder.GetChars(buffer, 0, bytes, chars, 0);
//				messageData.Append(chars);
//				// Check for EOF or an empty message.
//				if (messageData.ToString().IndexOf("<EOF>") != -1)
//				{
//					break;
//				}
//			} while (bytes != 0);

//			return messageData.ToString();
//		}
//		static void DisplaySecurityLevel(SslStream stream)
//		{
//			Console.WriteLine("Cipher: {0} strength {1}", stream.CipherAlgorithm, stream.CipherStrength);
//			Console.WriteLine("Hash: {0} strength {1}", stream.HashAlgorithm, stream.HashStrength);
//			Console.WriteLine("Key exchange: {0} strength {1}", stream.KeyExchangeAlgorithm, stream.KeyExchangeStrength);
//			Console.WriteLine("Protocol: {0}", stream.SslProtocol);
//		}
//		static void DisplaySecurityServices(SslStream stream)
//		{
//			Console.WriteLine("Is authenticated: {0} as server? {1}", stream.IsAuthenticated, stream.IsServer);
//			Console.WriteLine("IsSigned: {0}", stream.IsSigned);
//			Console.WriteLine("Is Encrypted: {0}", stream.IsEncrypted);
//		}
//		static void DisplayStreamProperties(SslStream stream)
//		{
//			Console.WriteLine("Can read: {0}, write {1}", stream.CanRead, stream.CanWrite);
//			Console.WriteLine("Can timeout: {0}", stream.CanTimeout);
//		}
//		static void DisplayCertificateInformation(SslStream stream)
//		{
//			Console.WriteLine("Certificate revocation list checked: {0}", stream.CheckCertRevocationStatus);

//			X509Certificate localCertificate = stream.LocalCertificate;
//			if (stream.LocalCertificate != null)
//			{
//				Console.WriteLine("Local cert was issued to {0} and is valid from {1} until {2}.",
//					localCertificate.Subject,
//					localCertificate.GetEffectiveDateString(),
//					localCertificate.GetExpirationDateString());
//			}
//			else
//			{
//				Console.WriteLine("Local certificate is null.");
//			}
//			// Display the properties of the client's certificate.
//			X509Certificate remoteCertificate = stream.RemoteCertificate;
//			if (stream.RemoteCertificate != null)
//			{
//				Console.WriteLine("Remote cert was issued to {0} and is valid from {1} until {2}.",
//					remoteCertificate.Subject,
//					remoteCertificate.GetEffectiveDateString(),
//					remoteCertificate.GetExpirationDateString());
//			}
//			else
//			{
//				Console.WriteLine("Remote certificate is null.");
//			}
//		}
//		private static void DisplayUsage()
//		{
//			Console.WriteLine("To start the server specify:");
//			Console.WriteLine("serverSync certificateFile.cer");
//			Environment.Exit(1);
//		}
//		public static int Main(string[] args)
//		{
//			string certificate = null;
//			if (args == null || args.Length < 1)
//			{
//				DisplayUsage();
//			}
//			certificate = args[0];
//			SslTcpServer.RunServer(certificate);
//			return 0;
//		}
//	}
//}